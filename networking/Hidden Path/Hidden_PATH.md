# WRITEUP NETWORKING / HIDDEN PATH
## Consignes :

```
Vous avez réussi à avoir accès à un équipement réseau exposé sur internet. Utilisez le pour essayer d'explorer le reste de l'architecture.

Pour se connecter à l'équipement réseau: 
ssh vyos@<ip_portailvms>
Password: vyos

PS : Attention ! Les réseaux de ce challenge ne sont pas accessibles directement depuis votre machine personnelle mais uniquement depuis l'équipement réseau du challenge.
```

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
PortailVMS
```

## Points attribués :

```
25
```

## Hint :

```
Vyos est une distribution Linux, ne vous limitez pas à la CLI
```

## Flag :

```
fl@g{N3tw0rk_1s_th3_k3y}
```


# writeup

Lancer l'instance sur portailVMS

Se connecter en ssh sur l'equipement réseau :

```
ssh vyos@ip_instance

pass: vyos
```

Effectuer une capture de flux sur l'interface eth1 (qui n'a pas d'adresse) avec cette commande:

```
monitor traffic interface eth1
```

On remarque une requete ARP d'un equipement avec l'adresse ip 192.168.3.2 qui cherche à contacter l'adresse ip 192.168.3.1
On configure donc l'interface eth1 avec l'adresse ip 192.168.3.1:

```
configure
set interfaces ethernet eth1 address 192.168.3.1/24
commit
```

Faire un ping pour s'assurer que l'on arrive à joindre l'adresse 192.168.3.2:

```
run ping 192.168.3.2
```

Refaire une capture de flux sur l'interface eth1:

```
run monitor traffic interface eth1
```

On remarque que des paquets de type OSPF arrivent depuis l'adresse 192.168.3.2.
On va donc configurer un router ospf afin de decouvrir de nouvelles routes par ce protocole
```
set protocols ospf area 0 network 192.168.3.0/24
set protocols ospf parameters router-id 192.168.3.1
commit
```

On affiche les routes pour verifier si un nouveau réseau est apparu

```
exit
show ip route
```

On decouvre un nouveau réseau, le 172.6.58.0/30, on peut donc déduire qu'il n'y a que 2 adresses ip dans ce réseau, la .1 et la .2
En scannant les 2 machines avec l'outil nmap présent sur le vyos, on decouvre un serveur web sur la machine .2
Pour utiliser l'outil il faut passer root sur le vyos

```
sudo bash
password: vyos

nmap -sS -v -O 172.6.58.2
```

Grace à l'outil curl présent sur le Vyos, on va pouvoir afficher la page web et ainsi recuperer le flag.

```
curl --url http://172.6.58.2
```


Le flag est : **fl@g{N3tw0rk_1s_th3_k3y}**

<!-- Review Taz : 20230321 -->