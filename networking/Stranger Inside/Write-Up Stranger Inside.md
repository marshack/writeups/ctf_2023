# WRITEUP NETWORKING / STRANGER INSIDE
## Enoncé :

Une remontée d'informations vous annonce qu'une machine a un comportement suspect sur le réseau. Malheureusement c'est une marchine virtuelle. Elle perturbe l'ensemble de l'architecture de l'entreprise, vous devez l'isoler et ne laisser qu'un accès SSH afin d'investiguer pour trouver la source du problème depuis la machine d'administration.
Pour ce faire vous avez un accès SSH au switch de l'architecture depuis la machine d'administration.
IP Switch : 192.168.4.1
IP Machine infectée : 192.168.2.118
Credentials admin/switch/infectée : admin : 12345678

Pour accéder à la machine d'administration : ssh admin@<IP_portailvms>

***PS : Attention ! Les réseaux de ce challenge ne sont pas accessibles directement depuis votre machine personnelle mais uniquement depuis la machine d'administration du challenge.***

## Write-Up

### <u>Reconnaisance archi / Analyse réseau</u>

Nous commençons par nous connecter au switch depuis la machine d'administration.
```bash
$ ssh admin@192.168.4.1
```

Nous pouvons afficher la ***running config*** du switch pour se rendre compte de l'architecture présente 
```HPE
$ display current-config
```

On repère 4 réseaux comprennant celui de la machine infectée.
- 192.168.1.0 (WAN)
- 192.168.2.0 (infectée)
- 192.168.3.0 (entreprise)
- 192.168.4.0 (admin)

Il va falloir créer des ACLs pour isoler la machine infectée du reste du réseau tout en nous laissant une possibilité de faire une connexion SSH depuis la machine admin vers la machine infectée.

### <u>Isolement de la machine suspecte</u>

La création des ACLs se fait en ***system view***

```HPE
$ system-view

# access-list advanced name *name*
# rule 1 permit tcp source 192.168.2.118 0 source-port eq 22 destination 192.168.4.26 0
# rule 10 deny ip source 192.168.2.118 0 destination any

# interface Gi2/0
# packet-filter name *name* inbound

# exit
# exit
$ exit
```

Une fois que la machine a arrêté de contacter les différents réseaux auquels elle avait accès, le service SSH est activé.

### Connexion en SSH

Si on a bien fait notre travail avec les ACLs, le SSH est activé sur la machine infectée. On va donc accéder à cette machine.

```bash
$ ssh admin@192.168.2.118
```

Ca y est, nous avons réussi à accéder à la machine, il faut maintenant trouver le flag.

### Recherche du fichier

Du fait que la machine est infectée, on va faire une recherche dans les processus présents sur la machine.
```bash
$ ps
```

Nous voyons un processus ***stranger*** ce qui nous conduit à aller regarder dans le ***/bin***.

En parcourant les fichiers, on retrouve bien un fichier ***stranger***.

Quand on exécute le binaire, une phrase apparait : 
```bash
$ ./stranger
"Ok... Tu m'as trouvé... Heureusement, le challenge est bien ficelé..."
```

Un petit indice qui nous indique de faire un ***strings*** sur le fichier.
```bash
$ strings /bin/stranger
```
En parcourant le code, on trouve le flag suivant :

**fl@g{AutomobileClubLuxembourg}**

<!-- Review Taz : 20230322 -->