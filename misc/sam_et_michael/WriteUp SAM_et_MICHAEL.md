# WRITEUP MISC / SAM ET MICKAEL
## Consigne :
```
Vous disposez de l'export d'une base SAM d'un poste windows 10.

Retrouver le mot de passe de l'utilisateur *michael.jordan*.

`e34cb183a7fb36a913cebdd04d54aed90a2a5f59`  [SAM.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/misc/sam_michael/SAM.zip)

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
SAM.zip
```

## Serveur :

```
download
```

## Points attribués :

```
15
```

## Hint : 

```
Néant
```

## Flag :
```
fl@g{turtle}
```

## Solution : 

Importer les fichiers SAM et SYSTEM sur sa machine kali linux.
Installer impacket via la commande 
> `sudo apt install python3-impacket`.

Récupérer les hash des mots de passe via la commande
> `impacket-secretsdump -system /home/kali/Desktop/sam/SYSTEM -sam /home/kali/Desktop/sam/SAM LOCAL > /home/kali/Desktop/hash.txt`

où `/home/kali/Desktop/sam/` represente le chemin où se situe vos deux fichiers.

Utiliser `john` pour déchiffrer les mots de passe.

> `john hash.txt --format=NT --wordlist /usr/share/wordlists/rockyou.txt`

Le mot de passe du compte michael.jordan apparait, il s'agit de "turtle".

<!-- Review Taz : 20230321 -->