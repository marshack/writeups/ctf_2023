# Firejail profile for bastion
quiet

# Persistent global definitions
include /etc/firejail/globals.local

caps.drop chown,dac_override,dac_read_search,fowner,fsetid,kill,setpcap,linux_immutable,net_bind_service,net_broadcast,net_admin,net_raw,ipc_lock,ipc_owner,sys_module,sys_rawio,sys_chroot,sys_ptrace,sys_pacct,sys_admin,sys_boot,sys_nice,sys_resource,sys_time,sys_tty_config,mknod,lease,setfcap,mac_override,mac_admin,syslog,wake_alarm,block_suspend

novideo
nosound
allusers
private
private-dev
private-tmp
private-bin pwd,ls,ssh,jail,env,echo,printf,bash,sh,cp,mv,tail,ps,chmod,readlink,sudo,dirname,date,wget,chgrp,basename,mkdir,touch,cat,more,less,sed,grep,groups,hostname,id,who,vi,awk,which,head,find,sha256sum,sleep,scriptreplay

