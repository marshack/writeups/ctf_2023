#!/bin/bash
#
# Bastion : the most secure in the world
# Copyright : The6bi3re (C) 
# License   : GPLv5

# abort if using an undefined variable
set -o nounset

current_script="$(/usr/bin/readlink -f "$0")"
script_path=$(/usr/bin/dirname ${current_script})

source ${script_path}/common.sh

#### Properties (private)

# default jail
#jail="/bin/bash -r"
firejail_profile="/opt/bastion/profiles/bastion.profile"
jail="/usr/bin/firejail --profile=${firejail_profile} /bin/bash"
jail_fallback="/bin/bash -r"
username="${USER}"
members_group="bmembers"
target_dir="/opt/bastion/log/${username}"
timestamp="$($C_DATE +%s)"
hash_file="${target_dir}/sha256sum.txt"
log_file="${target_dir}/traces.log"

#### Methods

logErrorAndQuit () {
    addLogEntry "[ERROR] $1"
    errorAndQuit "$1"
}

protectFile () {
    $C_CHMOD u=r,g=r,o= "$1" || logErrorAndQuit "Impossible change file mode (file $1)"
    $C_CHGRP "${username}" "$1" || logErrorAndQuit "Impossible change group ownership (file $1)"
    $C_SHA256SUM "$1" >> ${hash_file} || logErrorAndQuit "Impossible to make hash (file $1)"
}

createAndProtectMetaFile () {
    if [ ! -f  "$1" ]; then
        $C_TOUCH "$1" || logErrorAndQuit "Impossible to create file : $1"
        $C_CHGRP "${username}" "$1" || logErrorAndQuit "Impossible change group ownership (file $1)"
        $C_CHMOD u=rw,g=r,o= "$1" || logErrorAndQuit "Impossible change file mode (file $1)"
    fi
}

addLogEntry () {
    $C_ECHO "$($C_DATE --utc +%c) UTC - ${username} - ${timestamp} - $1" 2>/dev/null >> ${log_file}
}

#### Main

usage="Usage: $(/usr/bin/basename $0)
make typescript of terminal session

Options:
  -h : display this help and exit
  -v : output version information and exit
"

while getopts "vh" opt; do
  case $opt in
    h)
      $C_ECHO "$usage"
      exit 0
      ;;
    v)
      $C_ECHO -e "\nVersion $version\n"
      exit 0
      ;;
    \?)
      $C_ECHO "$usage"
      exit 1
      ;;
  esac
done

# fix default umask
umask 027

whoami="$($C_ID -u -n 2>/dev/null)"

# check user
if ! $C_ID "${username}" > /dev/null 2>&1 ; then
    errorAndQuit "User account '${username}' does not exist"
fi
if [[ "${username}" == "root" ]]; then
    errorAndQuit "go away !"
fi


# from ssh ?
if [ -z "$SSH_TTY" ] || [ "$($C_PS -o comm= -p $($C_PS -o ppid= -p $PPID))" != "sshd" ] ; then
    errorAndQuit "Only use over SSH"
fi

# check members
if [ -z "$($C_GROUPS "${username}" | $C_GREP -E "(^|\s+)${members_group}(\s+|$)" 2>/dev/null)" ] ; then
    errorAndQuit "This user '${username}' is not a member of the group '${members_group}'"
fi

if [ ! -d "${target_dir}" ]; then
    $C_MKDIR -p "${target_dir}" || logErrorAndQuit "Impossible to create directory ${target_dir}"
    $C_CHGRP "${username}" "${target_dir}" || logErrorAndQuit "Impossible change group ownership (path ${target_dir})"
    $C_CHMOD u=rwx,g=rx,o= "${target_dir}" || logErrorAndQuit "Impossible change file mode (path ${target_dir})"
fi

createAndProtectMetaFile "${hash_file}"
createAndProtectMetaFile "${log_file}"

addLogEntry "[INFO] Connected"

timing="${target_dir}/${timestamp}-timing.txt"
target="${target_dir}/${timestamp}-type.txt"

if [[ "$($C_CAT /proc/1/comm)" != "firejail" ]]; then
    $C_SCRIPT -q --force --timing="${timing}" "${target}" -c "$C_SUDO -u ${username} ${jail}"
  else
    # already in sandbox, start a restricted shell
    $C_SUDO -u ${username} ${jail_fallback}
fi

protectFile "${timing}"
protectFile "${target}"

addLogEntry "[INFO] Connection closed"
