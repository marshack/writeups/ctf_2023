#!/bin/bash

#### Properties (private)
version="0.6"

# output color
BLUE='\033[0;34m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
CYAN="\033[36m"
NC='\033[0m' # No Color

C_ECHO=/usr/bin/echo
C_SED=/usr/bin/sed
C_CAT=/usr/bin/cat
C_PRINTF=/usr/bin/printf
C_TOUCH=/usr/bin/touch
C_GREP=/usr/bin/grep
C_AWK=/usr/bin/awk
C_HEAD=/usr/bin/head
C_ID=/usr/bin/id
C_GROUPS=/usr/bin/groups
C_MKDIR=/usr/bin/mkdir
C_CHMOD=/usr/bin/chmod
C_CHGRP=/usr/bin/chgrp
C_DATE=/usr/bin/date
C_SLEEP=/usr/bin/sleep
C_SUDO=/usr/bin/sudo
C_SCRIPT=/usr/bin/script
C_SCRIPTREPLAY=/usr/bin/scriptreplay
C_SHA256SUM=/usr/bin/sha256sum
C_PS=/bin/ps
C_WHICH=/usr/bin/which

#### Methods

echapChars () {
    $C_ECHO -n $* | $C_SED s/%/%%/g
}

information () {
    CH1="$(echapChars $1)"
    $C_PRINTF "${CYAN}$CH1 ${NC}\n"
}

warning () {
    CH1="$(echapChars $1)"
    $C_PRINTF "\n${RED}$CH1 ${NC}\n"
}

errorAndQuit () {
	warning "Error : $1\n"
	exit 1
}

separator () {
    $C_PRINTF "${BLUE}%90s${NC}\n" | $C_SED 's/ /─/g'
}

function isNumeric {
    case $1 in
        *[!0-9]*) return 1  ;;
        *) return 0 ;;
    esac
}

