#!/bin/bash
#
# Bastion : the most secure in the world
# Copyright : The6bi3re (C) 
# License   : GPLv5

# abort if using an undefined variable
set -o nounset

current_script="$(/usr/bin/readlink -f "$0")"
script_path=$(/usr/bin/dirname ${current_script})

source ${script_path}/common.sh

#### Properties (private)
count_opt=0
speed_opt=0
list_opt=0
speed=1
count=1
_re_sed_timing='s/.*\/([0-9]+)-timing.txt/\1/'
LIMIT_CMD="$C_CAT"

#### Methods

h1 () {
    separator
    information "${1}"
    separator
}

checkIntegrity () {
  $C_SHA256SUM --ignore-missing -c $1 > /dev/null && return 0
  return 1
}

printTimeStampAndDate () {
    timestamp="$1"
    _date="$($C_DATE -d @$timestamp)" && $C_ECHO "$current_ts ($_date)"
}

#### Main

usage="Usage: $(/usr/bin/basename $0) [USERNAME] [TIMESTAMP]
play back typescripts, using timing information

Options:
  -l : list the record timestamps
  -a <num> : speed up execution with time multiplier
  -c <num> : replay N records from this timestamp
  -h : display this help and exit
  -v : output version information and exit

Examples:
  $(/usr/bin/basename $0) -l user1
  $(/usr/bin/basename $0) user1 1643477601
  $(/usr/bin/basename $0) -a 5 -c 25 user1
"

while getopts "vhla:c:" opt; do
  case $opt in
    l)
      list_opt=1
      ;;
    a)
      speed_opt=1
      speed=${OPTARG}
      ;;
    c)
      count_opt=1
      count=${OPTARG}
      LIMIT_CMD="$C_HEAD -n ${count}"
      ;;
    h)
      $C_ECHO "$usage"
      exit 0
      ;;
    v)
      $C_ECHO -e "\nVersion $version\n"
      exit 0
      ;;
    \?)
      $C_ECHO "$usage"
      exit 1
      ;;
  esac
done


if ! isNumeric $speed; then
    errorAndQuit "'speed' must be a number (option -a)"
fi

if ! isNumeric $count; then
    errorAndQuit "'count' must be a number (option -c)"
fi

if (( $count < 1 )); then
    errorAndQuit "'count' must be > 0 (option -c)"
fi

shift $((OPTIND-1))

if [[ $list_opt -eq 1 ]]; then
  if [[ $speed_opt -eq 1 ]]; then errorAndQuit "cant use -l with -a" ; fi
  if [[ $count_opt -eq 1 ]]; then errorAndQuit "cant use -l with -c" ; fi
fi

if [[ ( "$#" < 1 ) ]]; then errorAndQuit "Username is required"; fi

username="$1"
if [[ ( "$#" < 2 ) ]]; then
  timestamp=".*"
else
  timestamp="$2"
fi

target_dir="/opt/bastion/log/${username}"
hash_file="${target_dir}/sha256sum.txt"

if [[ ! -d "${target_dir}" ]]; then
  errorAndQuit "No trace for this user : ${username}"
fi

if [[ $list_opt -eq 1 ]]; then
  for current_ts in  $($C_CAT ${hash_file} | $C_GREP "timing" | $C_SED -r "${_re_sed_timing}"); do
      printTimeStampAndDate "${current_ts}"
  done
  exit $?
fi


for current_ts in $($C_CAT ${hash_file} | $C_GREP "timing" | $C_SED -n "/${timestamp}/,\$p" | $LIMIT_CMD | $C_SED -r "${_re_sed_timing}"); do
  timing_file="${target_dir}/${current_ts}-timing.txt"
  trace_file="${target_dir}/${current_ts}-type.txt"
  if [[ -f "${timing_file}" && -f "${trace_file}" ]]; then
      checkIntegrity "${hash_file}" || errorAndQuit "Integrity validator returned an error"
      h1 "[*] START REPLAY - USER : ${username} - TIME : $(printTimeStampAndDate ${current_ts})"
      $C_SLEEP 2
      $C_SCRIPTREPLAY --divisor $speed --timing="${timing_file}" "${trace_file}"
      h1 "[*] REPLAY FINISHED"
  else
      errorAndQuit "No trace found for this user and this timestamp"
  fi
done

