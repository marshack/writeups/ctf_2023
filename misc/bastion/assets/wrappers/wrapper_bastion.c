#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

extern char **environ;
extern int errno;

/* 
gcc -o wrapper_bastion wrapper_bastion.c
*/
int main (int argc, char **argv) {
  uid_t uid, euid;
  uid=getuid();
  euid=geteuid();
  setreuid (euid, euid);
  char *args[] = { "/opt/bastion/shell.sh", NULL };
  execve(args[0], &args[0], environ);
  return errno;
}

