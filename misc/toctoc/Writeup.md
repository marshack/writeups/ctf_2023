# WRITEUP MISC / TOCTOC
## Consigne:
```
Le propriétaire aime la politesse.
Sa porte est fermée (http: 1337), il va falloir être poli(e)

```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

Attention : Besoin de NET_ADMIN et NET_RAW
docker run -i -t --cap-add NET_RAW --cap-add NET_ADMIN toctoc 

```
```

## Points attribués :

```
20
```

## Hint : 

```
néant
```

## Flag :

```
fl@g{be_polite_knock_on_the_door!!}
```

## Solution : 
On visite la page, on nous dit "Toc toc", l'image fait de même.
On nous précise qu'on a 2 secondes pour flagger.

Si on rafraichit la page, une séquence de trois nombres change.

On s'agit donc d'une séquence de trois ports aléatoires sur lesquels toquer.

Il nous faut le paquet knockd
```bash
sudo apt install -y knockd 
```

On crée un petit script
```bash
cat > /tmp/go <<EOF
curl -s http://172.17.0.2 -o toctoc.txt
sed -i -E "s/^(.*)\{'0/'0/;s/}(.*)$//" toctoc.txt
sed -i -E "s/'.'://g" toctoc.txt
sed -i -E "s/,//g" toctoc.txt
sed -i -E "s/^/knock -u 172.17.0.2 /g" toctoc.txt
chmod +x ./toctoc.txt
./toctoc.txt
curl -s http://172.17.0.2:1337
EOF
chmod +x /tmp/go
/tmp/go

<html> <head><title> Toctoc ! </title> </head> <body><center><h1>Bien jou&eacute; !</h1><br/><br/>Le flag est : fl@g{be_polite_knock_on_the_door!!}</center></body></html>

```

Le flag est fl@g{be_polite_knock_on_the_door!!}

<!-- Review Taz : 20230321 -->