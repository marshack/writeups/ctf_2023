# WRITEUP MISC / AMORCAGE
## Consignes
```
Votre collègue vous sollicite car il est incapable de retrouver les accès à un ancien serveur sur lequel se trouve des données importantes. 

Trouvez un moyen pour lui venir en aide. 

Démarrer un environnement virtuel. La durée de vie de l'environnement est limitée.

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/12','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce jointe :
```
neant
```

## Serveur :
```
PortailVMS
```

## Points attribués :
```
15
```

## Flag :
```
fl@g{b3tt3r_s3cur3_GRUB}
```

## Architecture :
```
Néant
```

## Solutions :
GRUB est un programme d'amorçage qui nous permet de sélectionner sur quel système d'exploitation nous allons booter. 

Lancer l'ova à l'aide de VM Ware ou de VirtualBox. 

Nous n'avons pas les credentials, nous allons donc essayer de trouver un autre moyen de récupérer les informations.

D'après le titre du challenge, on va donc se tourner vers le GRUB, programme d'amorçage, et voir s'il est sécurisé. 

Lors du boot de la machine, en arrivant sur le GRUB, il suffit d'appuyer sur la touche *e* pour le modifier. 

Les paramètres qui nous intéressent sont ceux encadrés comme ci-dessous : 

![Partie 1](grub1.PNG)

Nous modifions le paramètre `ro` par `rw` afin d'avoir les permissions d'écriture et nous ajoutons le paramètre `init=/bin/bash` afin de booter sur un shell. 

![Partie 2](grub2.PNG) 

On appuie sur F10 pour valider. 

Sur notre shell, on lance la commande `id` pour s'assurer que nous sommes bien root. 

![Partie 3](id.PNG)

Sous le répertoire root, on trouve le fichier .passwd qui contient le flag. 

<!-- Review Taz : 20230321 -->