# WRITEUP WARGAME / IL EN SAVAIT TROP
## Consigne:

```
Bob est un ancien agent du FBI. 
Vieillard et parano, il vit à l’écart du monde dans la campagne auvergnate depuis de nombreuses années. 
Il en sait beaucoup et pense que le FBI veut l’éliminer. Il y a deux semaines, Bob a été retrouvé pendu dans un hangar désaffecté de Clermont-Ferrand.

Vous faites partie d’une équipe de hackers. La fille de Bob vous a contacté pour rétablir la vérité dans cette affaire car elle ne croit pas au suicide de son père.

Votre équipe a commencé à étudier le sujet et a pu récolter des éléments. La fille de Bob vous a informé que son père stockait des documents sur un serveur web et avait souvent recours à des diverses techniques pour cacher ses données du FBI.

De plus, le vieil homme utilisait de nombreuses caméras, afin de sécuriser ses différents lieux de vie. Dernièrement Bob avait confié à sa fille qu’il avait remarqué la présence d’un gros 4x4 de couleur grise qui semblait le suivre.

Dépêchez vous, le FBI a déjà commencé à supprimer des preuves. Prouvez au plus vite que Bob ne s’est pas suicidé.

* Nota : Bob est anglophone.

Le flag est au format underscore case et est composé de deux parties :  
* un message
* le nom de la ville où Bob a été enlevé  
* le tout en minuscule

Ce qui donne : `fl@g{message_ville_de_l_enlevement}`
<p></p>

`8fd13131451693ac6f15533b18e4ca3453908b70`  [Il_en_savait_trop.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/wargame/il_en_savait_trop/Il_en_savait_trop.zip)

> Format du flag : fl@g{...}
```

## Pièce jointe :

```
https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/wargame/il_en_savait_trop/Il_en_savait_trop.zip
HASH : 8fd13131451693ac6f15533b18e4ca3453908b70
```

## Serveur :

```
Serveur de Download
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{i_will_never_kill_myself_blanzat}
```

## Solution : 

Deux éléments nous sont fournis : une capture réseau et un fichier sans extension.

Commençons par la capture réseau (on peut également commencer le challenge par l’autre fichier, peu importe). 
En l’ouvrant avec Wireshark et en parcourant les divers échanges, on remarque des trames HTTP, ce qui colle avec le contexte “La fille de Bob vous a informé que son père stockait des documents sur un serveur web”.

![image01](images/image01.jpeg)

En mettant le filtre “http”, on constate que la majorité des échanges se font entre les IP 192.168.95.129 et 192.168.95.1.

![image02](images/image02.jpeg)

Il faut à présent analyser les échanges afin de comprendre ce qu’il se passe ici. En cliquant sur une des trames envoyées par l’IP 192.168.95.1 puis en déroulant le menu “Hypertext Transfer Protocol” puis “Autorization Basic”, on remarque qu’il s’agit d’une tentative de connexion en HTTP Basic de la part de l’IP 192.168.95.1 sur le serveur web 192.168.95.129.

![image03](images/image03.jpeg)

Un autre élément pouvant nous frapper est la présence du User-agent “python-requests”. Cela nous indique que cette connexion fait probablement l’objet d’une action automatisée. Regardons à présent une trame de réponse, il s’agit d’une erreur 401 UNAUTHORIZED, ce qui signifie que l’authentification a échoué.

On remarque également que toutes ces actions se sont déroulées dans un laps de temps extrêmement court. Ce qui nous permet, encore une fois, de pencher pour une action automatisée.

![image04](images/image04.jpeg)

En observant le détail des autres échanges, on remarque que le schéma est le même partout : l’IP 192.168.95.1 tente de se connecter avec un identifiant et mot de passe différent à de multiples reprises au serveur web 192.168.95.129. 

Il devient alors évident, à la vue des autres éléments que nous possédons, d’une tentative de bruteforce sur ce serveur web, à l'aide d'un script python.

Voyons maintenant si cette tentative a abouti ou non : pour cela, il faut chercher un code de retour 200 avec le serveur web en IP source et l’IP 192.168.95.1 en destination. On met donc le filtre “http.response.code==200 and ip.src==192.168.95.129 and ip.dst==192.168.95.1”.

Bingo ! Un échange ressort, ce qui signifie que la tentative de bruteforce a bien abouti. En déployant les menus “Hypertext Transfer Protocol”>”Line-based text data”, on peut visualiser le code HTML de la page retournée par le serveur, qui contient un lien précédé de la mention **Very secret file secure location**, nous sommes donc sur la bonne voie.

![image05](images/image05.jpeg)

Ce lien pointe vers un fichier “truth.wav” stocké sur Mega. Téléchargeons ce fichier audio afin de voir ce qu’il contient.

Après l’avoir écouté, vous avez les oreilles qui saignent. Serait-ce un son extra terrestre, du morse ? Si vous avez travaillé un peu vos talents en sténographie, vous tenterez d’observer le spectre audio de ce fichier, il pourrait contenir un message caché.

J’utilise personnellement Sonic Vizualiser pour cela, ajoutez le fichier audio, cliquez sur Layer > add spectrogram > truth.wav : all channels mixed.

![image06](images/image06.jpeg)

Il y a bien un message caché dans le spectre audio. Mais celui-ci est pour le moment peu visible.

![image07](images/image07.jpeg)

On va alors optimiser l’affichage du spectrogramme en réglant les paramètres suivants sur la droite de notre écran:

![image08](images/image08.jpeg)

Et là, le message est tout de suite plus clair : “If I suddenly die, truth is here :
https://pastebin.com/68iHKKhN

![image09](images/image09.jpeg)

Je commence à avoir l’impression que nous approchons de la vérité (1ère partie du flag, cf contexte).
En se rendant au lien pastebin, on obtient une série de caractères qui semble à première vue ne rien vouloir dire.

![image10](images/image10.jpeg)

Soit on connaît déjà ce langage, soit on peut le coller le contenu du pastebin dans un moteur de recherche pour se rendre compte facilement qu’il s’agit de
Brainfuck, un langage de programmation bien exotique.

![image11](images/image11.jpeg)

On peut facilement reverse du brainfuck via, par exemple, le site dcode.fr :

![image12](images/image12.jpeg)

On obtient alors le message “i_will_never_kill_myself” écrit en underscore case. Tiens tiens tiens, ne serait-ce pas la vérité ? Nous avons la première
partie du flag :  **i_will_never_kill_myself**

Regardons à présent le second fichier : il n’a pas d’extension mais nous pouvons dans un premier temps identifier son type grâce à la commande
“file”:

![image13](images/image13.jpeg)

Il s’agit donc ici d’un disque. 

Pour voir la table de partitions de ce disque, on utilise la commande `mmls` :

![image14](images/image14.jpeg)

Il y a beaucoup de choses sur ce disque, mais restons concentrés sur ce qui nous intéresse : trouver la ville dans laquelle Bob a été enlevé. Il y a de fortes
chances qu’une partition de type basic data partition nous aide, puisque c’est la plus à même de contenir des documents/photos/vidéos...

Cette partition commence à l’offset **31744**, on utilise la commande `fls -o 31744 -r mHOqsd` afin de visualiser les fichiers présents dans cette partition.

![image15](images/image15.jpeg)


Et le moins que l’on puisse dire, c’est que là aussi, on a beaucoup de choses. Beaucoup de fichiers PDF, de fichiers ayant été téléchargés directement sur ce
disque (cf .crdownload), de photos... (la liste des fichiers ci-dessus est non exhaustive).

On peut exporter ces fichiers pour les examiner avec la commande `icat -o 31744 -r mHOqsd <numéro_inode_fichier> > <fichier_de_sortie.extension>`

A force de chercher, on tombe sur un dossier “Somfy autocatch system” qui contient un fichier nommé "readme.txt'' et qui n’a pas été supprimé.

![image16](images/image16.jpeg)

On l’exporte alors et on l’ouvre :

![image17](images/image17.jpeg)

Intéressant, un système de caméras ! Comme nous l'avait dit sa fille (cf contexte). Visiblement le système prend des photos lorsque du mouvement
est détecté et nous avons un fichier "camera_04.jpg" qui a été supprimé de ce dossier. Exportons ce fichier et ouvrons-le afin de voir ce qu’il contient :

![image18](images/image18.jpeg)

![image19](images/image19.jpeg)

On voit distinctement un homme menaçant un vieil homme avec une arme à côté d’un gros 4x4 gris (cf contexte), ne serait-ce pas notre enlèvement ? Il
faut qu’on sache où cette photo a été prise pour avoir la seconde partie du flag.

On utilise alors la commande `exif camera_04.jpg` afin de visualiser les métadonnées de l'image

![image20](images/image20.jpeg)


Les métadonnées contiennent une géolocalisation ! On a plus qu'à traduire les coordonnées pour déterminer le lieu ou a été prise la photo.
On utilise alors l’un des nombreux sites permettant de traduire des coordonnées en point géographique sur une map (ici j’ai utilisé
https://www.gps-coordinates.net/) :


![image21](images/image21.jpeg)

La photo a été prise à côté de Clermont-Ferrand (cf contexte), dans la ville de Blanzat.
Nous avons notre flag au complet :

**fl@g{i_will_never_kill_myself_blanzat}**

<!-- Review Taz : 20230322 -->