# WRITEUP FORENSIC / COMPROMISSION / EXFILTRATION ICMP
## Consignes
```
#### Exfiltration ICMP

Notre SOC local a fait remonter un pic d'activité réseau anormal pour 2 machines en particulier.

Avec la capture réseau fournie, pouvez-vous nous aider à y voir plus clair ?

`781c2da112a47b52f1925a6e0e070db98c41a8e3`  [Capture.pcapng](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Exfiltration_ICMP/Capture.pcapng)

> Format du flag : Fl@g{.....................}
```

## Pièce jointe :
```
- Capture.pcapng (capture réseau de l'échange)
```

## Serveur :
```
Néant
```

## Points attribués :
```
50
```

## Hint :
```
Language utilisé pour l'exfiltration de données : Python3
Recherche google : decoding xor data with few key bytes
```

## Flag : 
```
Fl@g{ICMP_3xtr@cti0n_Is_Fun_W@y}
```

## Solution :

### Description
Ce challenge porte sur une exfiltration de fichiers via le protocole ICMP.

Le client et le serveur, codés en python, utilisent la librairie "Scapy" pour forger les requêtes ICMP. 
Le contenu de ces requêtes est obfusqué via un algorithme XOR (voir "Ressource").

### Ressources
Recherche Google amenant à l'algorithme XOR : "decoding xor data with few key bytes"

- site : https://www.rsreese.com/decoding-xor-payload-using-first-few-bytes-as-key/
- site : https://github.com/VirusFriendly/gomi-scripts/blob/master/xor-pcap.py
	
Fichier challenge : Capture.pcapng

Code source :
- Serveur : ICMP_Exfil_Server.py
- Client : ICMP_Exfil_Client.py
- Flag : flag.txt
- Divers : tree.jpg
- Solveur : Decode.py

### Explications
#### Etude de la capture réseau
- Ouvrir la capture réseau avec Wireshark. Après plusieurs recherches, notamment via le type de protocole (dns, dhcp, icmp, etc.), on tombe sur des requêtes **ICMP suspectes**.
- Beaucoup de requêtes "request" sont envoyées à la suite, sans aucune réponse en retour, et via une adresse IP hors de la plage d'adressage locale.
- En regardant le contenu, on peut voir des données du type **BEG_Encrypted_Data_Xor_Challenge_BEG**, ce qui est anormal pour ce type de requête/protocole.

#### Etude des requêtes ICMP
 - On part donc sur une exfiltration de données via les requêtes ICMP. 
 - En regardant le contenu des trames, on comprend que les données exfiltrées sont obfusquées via un XOR.
 - Après quelques réflexions et analyses des 3 premiers échanges de trames ICMP, cela ressemble à un "challenge" client / serveur. Le client envoie une donnée xorée au serveur et celui-ci calcule la même donnée xorée puis renvoie "SUCCESS" ou "FAILED".

```mermaid
sequenceDiagram
Client ->> Serveur: BEG_Encrypted_Data_Xor_Challenge_BEG
Client ->> Serveur : OBFUSCATED_DATA
Client ->> Serveur : END_Xor_Challenge_END
Serveur ->> Client : SUCCES or FAILED
```

#### Etude de la clé XOR
- Pour trouver la clé XOR, il faut utiliser la valeur en clair **BEG_Encrypted_Data_Xor_Challenge_BEG** de la première trame et la valeur **xorée** de la deuxième trame.
- La clé XOR est longue de 20 caractères car il y a une répétition de la clé sur les autres caractères. 

> Note : Chaque échange est unique et a donc sa propre clé XOR.

#### Déchiffrement des trames ICMP
- Maintenant que l'on a la logique pour trouver la clé XOR, il ne reste plus qu'à récupérer les paquets de la capture, dé-xorer le tout et le fichier flag.txt apparaît.
- On utilise la librairie *scapy* sous python3. La logique est la suivante :

```mermaid
sequenceDiagram
Decode.py ->> Capture.pcapng : Lecture des 3 premières trames ICMP (IP : 10.1.10.10)
Decode.py ->> Decode.py : Calcul clé XOR
Decode.py ->> Capture.pcapng : Lecture des trames ICMP contenant le fichier (IP : 10.10.10.10)
Decode.py ->> Decode.py : Dé-obfuscation
Decode.py ->> Decode.py : Ecriture du fichier en clair
```

### Détails

#### Code source du solveur python
```
#!/usr/bin/env python3
from scapy.all import *

def exfiltrate_data():
	global start
	global end
	global start_key
	global end_key
	global key
	
	for paquet in pcap_paquet:
        # Traitement des paquets de données
		if paquet.haslayer(ICMP) and paquet[IP].src == ip_src_data:
			query = paquet.getlayer(Raw).load
			# Debut de l'envoie
			if query == debut:
				tmp_data = b""
				xored_data = b""
				start = 1
				end = 0

			# Fin de l'envoie
			elif query == fin:
				with open(file_name, "wb") as output_file:
					output_file.write(xored_data)
				start = 0
				end = 0

			# Récupération du nom du fichier
			elif start == 1 and end == 0:
				file_name = query.decode("utf-8")
				print(" Filename : " + str(file_name))
				end = 1

			# Traitement de la donnée			
			else:
				tmp = XOR(query)
				xored_data = xored_data + tmp

		# Traitement des paquets de control de clé XOR
		elif paquet.haslayer(ICMP) and paquet[IP].src == ip_src_key:
                        # Debut de l'envoie
			query = paquet.getlayer(Raw).load
			if query == debut_key:
				tmp_xored_key = b""
				start_key = 1
				end = 0

			# Fin de l'envoie
			elif query == fin_key:
				key = tmp_data_xored[0:20]
				print("\n KEY FINDED : " + str(key))

			# Calcul de la clé XOR
			elif start_key == 1 and end_key == 0:
				tmp_xored_key = bytearray(query)
				tmp_data_xored = bytearray(debut_key)
				test_len = len(tmp_xored_key)
				
				for i, bufbyte in enumerate(tmp_data_xored):
					tmp_data_xored[i] = bufbyte ^ tmp_xored_key[i % test_len]
				
				end = 1	

		
def XOR(data):
	global key
	
	tmp_data = bytearray(data)
	tmp_key = bytearray(key)
	key_len = len(key)
	
	for i, bufbyte in enumerate(tmp_data):
		tmp_data[i] = bufbyte ^ key[i % key_len]
	
	return bytes(tmp_data)	

				
##### MAIN #####
global key
global start
global end
global start_key
global end_key

# Capture
pcap_paquet = rdpcap("Capture.pcapng")

# Adresse IP concernées
ip_src_data = "10.10.10.10"
ip_src_key = "10.1.10.10"

# TAG de début et de fin (clé et donnée)
debut = b"BEGIN"
fin = b"END_TRAME"
debut_key = b"BEG_Encrypted_Data_Xor_Challenge_BEG"
fin_key = b"END_Xor_Challenge_END"

# Nécessaire au fonctionnement des boucles
start = 0
end = 0
start_key = 0
end_key = 0
tmp_data = b""
xored_data = b""
file_name = ""
key = b""

# Lancement de la dé-obfuscation
exfiltrate_data()
```

<!-- Review Taz : 20230321 -->