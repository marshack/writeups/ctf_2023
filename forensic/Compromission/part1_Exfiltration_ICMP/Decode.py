#!/usr/bin/env python3
from scapy.all import *

def exfiltrate_data():
	global start
	global end
	global start_key
	global end_key
	global key
	
	for paquet in pcap_paquet:
                # Traitement des paquets de données
		if paquet.haslayer(ICMP) and paquet[IP].src == ip_src_data:
			query = paquet.getlayer(Raw).load
			# Debut de l'envoie
			if query == debut:
				tmp_data = b""
				xored_data = b""
				start = 1
				end = 0

			# Fin de l'envoie
			elif query == fin:
				with open(file_name, "wb") as output_file:
					output_file.write(xored_data)
				start = 0
				end = 0

			# Récupération du nom du fichier
			elif start == 1 and end == 0:
				file_name = query.decode("utf-8")
				print(" Filename : " + str(file_name))
				end = 1

			# Traitement de la donnée			
			else:
				tmp = XOR(query)
				xored_data = xored_data + tmp

		# Traitement des paquets de control de clé XOR
		elif paquet.haslayer(ICMP) and paquet[IP].src == ip_src_key:
                        # Debut de l'envoie
			query = paquet.getlayer(Raw).load
			if query == debut_key:
				tmp_xored_key = b""
				start_key = 1
				end = 0

			# Fin de l'envoie
			elif query == fin_key:
				key = tmp_data_xored[0:20]
				print("\n KEY FINDED : " + str(key))

			# Calcul de la clé XOR
			elif start_key == 1 and end_key == 0:
				tmp_xored_key = bytearray(query)
				tmp_data_xored = bytearray(debut_key)
				test_len = len(tmp_xored_key)
				
				for i, bufbyte in enumerate(tmp_data_xored):
					tmp_data_xored[i] = bufbyte ^ tmp_xored_key[i % test_len]
				
				end = 1	

		
def XOR(data):
	global key
	
	tmp_data = bytearray(data)
	tmp_key = bytearray(key)
	key_len = len(key)
	
	for i, bufbyte in enumerate(tmp_data):
		tmp_data[i] = bufbyte ^ key[i % key_len]
	
	return bytes(tmp_data)	

				
##### MAIN #####
global key
global start
global end
global start_key
global end_key

# Capture
pcap_paquet = rdpcap("Capture.pcapng")

# Adresse IP concernées
ip_src_data = "10.10.10.10"
ip_src_key = "10.1.10.10"

# TAG de début et de fin (clé et donnée)
debut = b"BEGIN"
fin = b"END_TRAME"
debut_key = b"BEG_Encrypted_Data_Xor_Challenge_BEG"
fin_key = b"END_Xor_Challenge_END"

# Nécessaire au fonctionnement des boucles
start = 0
end = 0
start_key = 0
end_key = 0
tmp_data = b""
xored_data = b""
file_name = ""
key = b""

# Lancement de la dé-obfuscation
exfiltrate_data()
