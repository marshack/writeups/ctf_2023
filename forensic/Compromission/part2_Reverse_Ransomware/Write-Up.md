# WRITEUP FORENSIC / COMPROMISSION / REVERSE RANSOMWARE
## Consignes :
```
#### Reverse ransomware

Avec votre aide sur la première partie, nous avons pu remonter jusqu'à la source, merci !

J'ai encore besoin de vous pour un dernier travail. 

Il semblerait que l'exfiltration de données provienne d'une des machines de notre parc. Malgré la mise à jour récente de la machine (Windows 10 22H2 - 19045), elle est sous le joug d'un Ransomware (voir Desktop.jpg). 

La seule chose que nous ayons pu faire est une capture mémoire, pouvez-vous nous aider ?

**Capture mémoire :** `36f82ab9d949896edf6193c4a318280753330014`  [DESKTOP.dmp.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Reverse_Ransomware/DESKTOP.dmp.zip)

**Capture d'écran :** [Desktop.jpg](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Reverse_Ransomware/Desktop.jpg)

> Format du flag : Fl@g{.....................}
```

## Pièce jointe :
```
 - DESKTOP.dmp (dump mémoire)
 - Desktop.jpg (capture d'écran du bureau)
```

## Serveur :
```
Néant
```

## Points attribués :
```
50
```

## Hint :
```
Language utilisé pour le Ransomware : Python3
Outil utile : pyinstxtractor
```

## Flag : 
```
Fl@g{Th@nks_F0r_Pl@y1ng_My_G@me}
```

## Solutions

### Description
Ce challenge porte sur l'étude d'un Ransomware, codé en python puis converti en exécutable.
Le virus chiffre, via AES, le fichier contenant le flag puis enregistre la clef de chiffrement et l'IV dans le registre.

### Ressource
Fichier challenge : 
- Desktop.jpg
- DESKTOP.dmp (Taille : 1.99Go (2 146 467 840) / md5 : 7DDACFFF92D58AD924A975C6E385FDD6)

Code source :
- Py Extractor : pyinstxtractor.py
- Solveur : Decode.py

### Explications

#### Analyse du dump mémoire
- En prenant en compte le Windows 10 annoncé dans le contexte, on va utiliser volatility3 pour travailler sur le dump mémoire.
- On commence par lister tous les processus, les connexions réseaux, les commandes lancées pour les processus et un scan des fichiers sur le disque.
- Un processus particulier ressort : **scvhost.exe (PID : 4712)** qui lance à son tour **scvhost.exe (PID : 4948)**
- Le nom ressemble beaucoup au processus légitime nommé **svchost.exe**

#### Extraction des données du dump mémoire
- En faisant une recherche via le résultat de **filescan**, on retrouve 4 occurrences du fichier **scvhost.exe**, nous allons utiliser le premier résultat et extraire le fichier (**offset : 0x820668811280**).
> `python3 /opt/volatility3/vol.py -f DESKTOP.dmp windows.dumpfiles --virtaddr 0x820668811280`
- **Résultat** : récupération réussie, taille du fichier 13.5 Mo (14 204 928 octets)

#### Analyse du fichier extrait
- Un string sur le fichier nous révèle la présence de **python** au sein de cet exécutable (python38.dll, bibliothèque tkinter, PIL, Crypto, etc...)
- Il existe une façon de transformer un script python en exécutable, l'outil **Pyinstaller**
- Il existe aussi une façon de faire le processus inverse, via 2 outils, **pyinstxtractor et uncompyle6**

#### Reverse python, EXE vers PY
- On commence par télécharger **pyinstxtractor** via Github puis on exécute le script avec le fichier en argument.
> `python3 pyinstxtractor-master\pyinstxtractor.py scvhost.exe`
- Cela nous ressort beaucoup de fichiers dont un intéressant, **scvhost.pyc**
- On installe ensuite la librairie **uncompyle6** pour python et on rentre le fichier ".pyc" en argument.
> `pip3 install uncompyle6`
> `python3 uncompyle6.py scvhost.pyc > scvhost_reverse.py`
- Nous voici en présence du code source du Ransomware !

#### Création du script de déchiffrement
- Voici les éléments utiles composants le Ransomware :
	- Utilisation de la librairie pycryptodome.
	- Chiffrement du type **AES-CBC avec IV**.
	- **Clé aléatoire de 32 bits** à chaque lancement.
	- Enregistrement de la clef et de l'IV dans 2 clefs de registre :
		- **HKCU\Control Panel\Mouse\key**.
		- **HKCU\Control Panel\Keyboard\iv**.
		- Extraction des clefs de registre via volatility :
			- Recherche de la ruche "HKCU" :
				- >`windows.registry.hivelist`
				- >**0xe18fabc4e000	\??\C:\Users\root\ntuser.dat**
			- Impression des clefs :
				- > `windows.registry.printkey --offset 0xe18fabc4e000 --key "Control Panel\Mouse"`
				- > `key : jycu2GsuCU6fKHZ8//59dha2TmpDq4q98r7WZNk0EPU=`
				- > `windows.registry.printkey --offset 0xe18fabc4e000 --key "Control Panel\Keyboard"`
				- > `iv : TCq4pQ4GD1EKzDS+WVatbw==`
	- Chiffrement des fichiers du dossier **C:\Users\root\Desktop\Private** :
		- Extension : **y0uhav3b33np0wn3d**.
		- Recherche via le filescan de volatility, 2 fichiers ressortent :
			- **private.txt.y0uhav3b33np0wn3d** (offset : 0x820668b483e0).
			- **flag.txt.y0uhav3b33np0wn3d** (offset : 0x820668b49e70).
		- Extraction des 2 fichiers via volatility :
			- > `windows.dumpfiles --virtaddr 0x820668b483e0`
			- > `windows.dumpfiles --virtaddr 0x820668b49e70`
		- Chaine en base64 présentes dans les 2 fichiers :
			- > `flag.txt : 3O+kPLs6iQBq3Lq9B+n/h4lFzB73CKv8Giy7GqhK+Qo=`
			- > `private.txt : jObam87ibQ+1W5L5CL5RUEgUdhHnMYl7rRK89hroo1X6+VnTtLbrBt0S5bV2qTxp`
- Nous avons maintenant tous les éléments nécessaires à la création du script de déchiffrement (**voir détails**).
- Le flag se situe dans **private.txt**, le fichier flag.txt est un leurre.

#### Code source du solveur python
```
#!/usr/bin/env python3
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from base64 import b64decode

# Clef et IV trouvés dans les clefs de registre
key = "jycu2GsuCU6fKHZ8//59dha2TmpDq4q98r7WZNk0EPU=="
iv = "TCq4pQ4GD1EKzDS+WVatbw=="

# Texte chiffré
txt = "jObam87ibQ+1W5L5CL5RUEgUdhHnMYl7rRK89hroo1X6+VnTtLbrBt0S5bV2qTxp=="

# Decodage Base64
key_decoded = b64decode(key)
iv_decoded = b64decode(iv)
content_decoded = b64decode(txt)

# Utilisation de l'AES-CBC avec IV pour le déchiffrement
cipher = AES.new(key_decoded, AES.MODE_CBC, iv_decoded)
result = unpad(cipher.decrypt(content_decoded), AES.block_size)

# Affichage du résultat
print(result)
```

<!-- Review Taz : 20230321 -->