#!/usr/bin/env python3
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from base64 import b64decode

# Clef et IV trouvé dans les clefs de registre
key = "jycu2GsuCU6fKHZ8//59dha2TmpDq4q98r7WZNk0EPU=="
iv = "TCq4pQ4GD1EKzDS+WVatbw=="

# Texte chiffré
txt = "jObam87ibQ+1W5L5CL5RUEgUdhHnMYl7rRK89hroo1X6+VnTtLbrBt0S5bV2qTxp=="

# Decodage Base64
key_decoded = b64decode(key)
iv_decoded = b64decode(iv)
content_decoded = b64decode(txt)

# Utilisation de l'AES-CBC avec IV pour le déchiffrement
cipher = AES.new(key_decoded, AES.MODE_CBC, iv_decoded)
result = unpad(cipher.decrypt(content_decoded), AES.block_size)

# Affichage du résultat
print(result)
