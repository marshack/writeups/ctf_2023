# WRITEUP FORENSIC / CLE USB
## Consigne :
```
Une clé USB a été volée par un groupe d'étudiants du professeur Xavier, le contenu de cette clé a été copié grâce à un `dd`.
Les étudiants ont pu remettre la clé à sa place sans éveiller de soupçon.

Ils savent que cette clé contient le mot de passe pour accéder au Cérébro.
Aidez les à retrouver ce mot de passe.

`b7631d923f0e5cf35914e6149b6b9ead71f4a99d`  [Cle.raw](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/cle/Cle.raw)

> Format du flag : fl@g{.....................}
```

## Pièce(s)  jointe(s) :
```
Cle.raw
```

## Serveur :
```
Néant
```

## Points attribués :
```
25
```

## Hint :
```
Intéressez-vous au fichier Charles-Xavier.docx.
```

## Flag :
```
fl@g:{KatherineAnnePryde}
```

## Solution :
> `fls Cle.raw`

Trouver le fichier "Raven-Darkholme.docx:Psylocke.docx"

> `icat Cle.raw 53-128-3 > /tmp/challenge.docx`


Ouvrir le fichier /tmp/challenge.docx et copier la chaine de caractères s'y trouvant.

Utiliser Cyberchef (version : https://gchq.github.io/CyberChef) pour décoder la clé.

Le chiffrement utilisé est ROT8000, obtenir le flag !
```

<!-- Review Taz : 20230321 -->