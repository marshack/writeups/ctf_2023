# WRITEUP FORENSIC / FORT BOYARD
## Consigne: 
```
Bienvenue dans les épreuves cyber de Fort Boyard.  
Votre travail est de retrouver les indices relatifs à la compromission d’un poste de travail au travers des épreuves suivantes :

* Un fichier pas si bien caché que ça ...
* Une sacrée mémoire  
* Beaucoup de bruit, mais ça reste très clair

C’est très simple pour la suite, 1 épreuve = 1 artefact = 1 indice à trouver, 3 indices = 1 mot clé à deviner.  
Le flag devra être inséré en minuscule dans fl@g{}. 

- `ea2d9e24e754e676d53f566243e52a115253898f`  [local.pcapng](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Fort_Boyard/local.pcapng)
- `55ba1772781d4a3f0aad92e8eca470325d54e148`  [XPPro_02-2eacc1cb.7z](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Fort_Boyard/XPPro_02-2eacc1cb.7z)
- `b2ad9f42f1669764fcae68da00272ae2b4f0cf36`  [XPPro_02.7z](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/Fort_Boyard/XPPro_02.7z)

> Format du flag : fl@g{...}
```

## Pièce jointe :
```
XPPro_02.7z
XPPro_02-2eacc1cb.7z
local.pcapng
```

## Serveur :
```
CTFD
```

## Points attribués :
```
40
```

## Hint : 
```
Néant
```

## Flag :
```
fl@g{avion}
```

## Solution : 
Présentation de la méthodologie de résolution du challenge :

### Indice 1 – Un fichier pas si bien caché que ça…
Artefact : fichier vmdk zippé « XPPro_02.7z » 843 mo

Outil à utiliser : Autopsy

- Extraction du fichier vmdk de XPPro_02.7z

le fichier extrait est de type vmdk

- Création d’un nouveau *case* sur Autopsy, y intégrer le VMDK comme source de données

![image-20220128174509020](../Fort_boyard/image_writeup/image-20220128174509020.png)

- Attendre quelques minutes (45% d’analyse minimum est nécessaire)

![image-20220128174704927](../Fort_boyard/image_writeup/image-20220128174704927.png)

- Ouvrir le menu contenant les shell BagMRU

![image-20220128174743832](../Fort_boyard/image_writeup/image-20220128174743832.png)

- Constater la présence d’un dossier Guardian dans « ApplicationData »

- Clic droit > View source file in directory

![image-20220128174844513](../Fort_boyard/image_writeup/image-20220128174844513.png)

- Cliquer sur le dossier Application Data > Guardian puis sur « *Indice1.txt.txt:^SummaryInformation* » pour afficher ses propriétés 

![image-20220128174942108](../Fort_boyard/image_writeup/image-20220128174942108.png)

- Dans l’onglet Text, cliquer sur « Strings », vous disposez alors d’un hash en base64

![image-20220128175025650](../Fort_boyard/image_writeup/image-20220128175025650.png)

- Décoder le à l’aide de CyberChef vous disposez désormais d’une chaine en hexadecimal

![image-20220128175101082](../Fort_boyard/image_writeup/image-20220128175101082.png)

- Réaliser la même opération, et vous obtenez le premier indice « Pilote »

![image-20220128175207460](../Fort_boyard/image_writeup/image-20220128175207460.png)

## Indice 2 – Une sacrée mémoire
Artefact : fichier vmem zippé XPPro_02-2eacc1cb.7z

Outil à utiliser : Volatility 2.4

- Extraction du fichier vmem de XPPro_02-2eacc1cb.7z

- Exécuter la commande « **python vol.py -f ../XPPro_02-2eacc1cb.vmem cmdscan** » puis récupérer le code morse

![image-20220128175456029](../Fort_boyard/image_writeup/image-20220128175456029.png)

- Décoder le à l’aide de Cyberchef et vous obtenez le second indice « CHASSE »

![image-20220128175533948](../Fort_boyard/image_writeup/image-20220128175533948.png)

## Indice 3 – Beaucoup de bruit mais tout reste très clair…
Artefact: capture wireshark local.pcapng

Outil à utiliser : Wireshark

- Ouvrir l’artefact avec Wireshark
- Filtrer sur le protocol http

![image-20220128175743775](../Fort_boyard/image_writeup/image-20220128175743775.png)

- Naviguer à la fin puis récupérer le nom de l’exécutable

![image-20220128175813753](../Fort_boyard/image_writeup/image-20220128175813753.png)

- Décoder le à l’aide de Cyberchef (Magic ou From Base64) et vous obtenez le dernier indice « AILE »

![image-20220128175838444](../Fort_boyard/image_writeup/image-20220128175838444.png)

https://www.fan-fortboyard.fr/pages/fanzone/mots-codes-fort-boyard/2020-2024.html

![image-20220128175940633](../Fort_boyard/image_writeup/image-20220128175940633.png)


```
fl@g{avion}
```
<!-- Review Taz : 20230321 -->