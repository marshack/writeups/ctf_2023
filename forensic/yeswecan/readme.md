# WRITE  FORENSIC / YESWECAN
## Consigne: 
```
Au bureau nous partageons un fichier texte commun (dans une partition chiffré) : **$memo$.txt** afin de stocker nos mots de passe.

Nous avons perdu ce blocnote, mais l'administrateur nous a fourni un fichier qui, selon lui, nous permettrait de le récupérer.

`68d96d3c15108cef572444c5cea872dc6449f860`  [YesWeCan.E01](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/forensics/yeswecan/YesWeCan.E01)
`3136150cce6cbddc3309970ae05bfe4da73c3557` [rockyou.zip](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/rockyou.zip)

> Format du flag : fl@g{...}
```

## Pièce jointe :
```
rockyou.zip
YesWeCan.E01
```

## Serveur :
```
Néant
```

## Points attribués :
```
30
```

## Hint : 
```
Néant
```

## Flag :
```
fl@g{B!tL0cK3r!sNot$@f3}
```

## Solution : 
Nous avons un fichier sans extension connu : `YesWeCan.E01`

Avec Autopsy (par exemple), au vue de l'arborescence, nous constatons qu'il s’agit d'un disque virtuel "Windows".
(Disk image ou vm file)

En fouillant dans l'arborescence nous apercevons un fichier "Confidentiel.vhdx" (dans Documents). Nous allons l'extraire en local pour essayer de l'ouvrir.

En double cliquant sur notre fichier "Confidentiel.vhdx" Windows essaye de monter le disque mais nous avons besoin d'un mot de passe pour le déchiffrement (BITLOCKER)

Nous allons demander à l'outil *johnTheRipper* de bruteforcer le fichier avec le dictionnaire "rockyou.txt" (modifié et fourni)

```
john --format=bitlocker --wordlist=rockyou.txt hash
```

Nous utiliserons aussi *bitlocker2john* pour créer les hachages et les charger dans john. (https://hashes.com/en/johntheripper/bitlocker2john)

John trouve le mot de passe : `ihateyou`

Nous pouvons désormais ouvrir le lecteur chiffré avec le mot de passe.

Dans ce lecteur un fichier texte "mdp.txt" qu'évidement nous allons ouvrir mais visiblement tout a été supprimé !
Démarrons *Autopsy* (ou logiciel de votre choix) afin de voir ce qui a été supprimé (Local disk)

Le flag est dedans ;)
fl@g{B!tL0cK3r!sNot$@f3}

<!-- Review Taz : 20230321 -->