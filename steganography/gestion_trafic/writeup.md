# WRITEUP STEGANOGRAPHY / GESTION TRAFFIC
## Consigne :

```
Un ami cheminot a créé un site pour gérer le trafic de son poste d'aiguillage.
Il pense que son site est bien sécurisé. Vous le connaissez bien et vous savez qu'il a sûrement caché une façon de s'y connecter.

N'oubliez pas la catégorie du challenge.

> le flag est de la forme `fl@g{QuelqueChose}`

http://game1.marshack.fr:42085
```

## Pièce jointe :

```
Néant
```

## Points attribués :

```
25
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{LeTrainEstAnnoncéAvecUnRetardDe3000Ans} 
```

## Solution : 

On accède au site.
Normalement il est assez sécurisé et il faut trouver le bon login/mdp pour y accéder.

En fouillant un peu sur la page, la seule chose qui peut être utile est l'image du logo qu'on télécharge.
Elle est anormalement grosse (env 3.5Mo).
Il s'agit d'un append binaire de deux fichiers : l'image qu'on voit et une archive zip.

En l'ouvrant avec un logiciel d'archive zip, 7z ou autre ou en modifiant l'extension en 7z, on accède a deux fichiers :
- *Plus belle ville de france.png* : Un leurre.
- *qr.png*

En flashant le code QR, on voit que c'est un leurre.
Il y a un message en LSB caché dans l'image *qr.jpg*. On peut la retrouver avec le programme **steghide** :
- Pour installer steghide si vous ne l'avez pas : *sudo apt install steghide*
- Puis extraire le texte caché en LSB : *steghide extract -sf qr.jpg*
- Puis lire le text.txt : *more text.txt*
- Ce qui donnera :
> login=garcin.lazare
> password=MonMotDePasseEstSuperLong!

On teste ce couple sur le site et on obtient le flag : fl@g{LeTrainEstAnnoncéAvecUnRetardDe3000Ans} 

<!-- Review Taz : 20230322 -->