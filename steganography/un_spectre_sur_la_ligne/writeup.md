# WRITEUP STEGANOGRAPHY / UN SPECTRE SUR LA LIGNE
## Consigne :

```
Un ami a capturé un étrange échange sur le réseau de son entreprise mais ne sait pas quoi en faire. Il pense qu'un des employés essaye d'envoyer des informations à quelqu'un de l'extérieur.
Mais ses compétences s'arrêtent là et c'est pourquoi il vous a demandé de l'aider à ce sujet.

<p></p>

`cc82300791145b7db9bacd3b33f1e94d704be3de`  [un_spectre_sur_la_ligne.pcap](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/stegano/un_spectre_sur_la_ligne/un_spectre_sur_la_ligne.pcap)

> le flag est de la forme `fl@g{QuelqueChose}`
```

## Pièce jointe :

```
Le fichier pcap de la capture réseau : Un spectre sur la ligne.pcap
```



## Points attribués :

```
20
```

## Hint : 

```

```

## Flag :

```
fl@g{LaSpectroCBi1}
```

## Solution : 


On télécharge le pcap et on l'ouvre avec Wireshark.
On remarque les protocoles SIP (Signaling VoIP) puis OPUS (communication VoIP). C'est donc une capture d'une communication VoIP.

Dans Wireshark : Telephonie > Appels VoIP.

On remarque qu'une ligne correspond à l'appel VoIP en question.

Dans Wireshark : On clique sur l'appel puis "Lire les flux".

On a deux flux : Celui de A vers B et B vers A. On remarque que seul le second a parlé. On veut donc extraire ce son.

Dans Wireshark : On clique sur la ligne L (source : 10.0.2.4) puis Exporter > Fichier audio synchronisé > vers un wav. Puis fermer Wireshark.

On a donc extrait une conversation VoIP de la capture qu'on peut maintenant écouter. On entend quelqu'un qui parle et un fond sonore très dérangeant, comme des interférences.
Il s'agit, outre de la voix jouant le rôle de leurre, d'un spectrogramme. (Une image transformée en son)

Il faut utiliser un site du style : `https://nsspot.herokuapp.com/imagetoaudio/` ou `https://academo.org/demos/spectrum-analyzer/` ou tout autre lecteur de spectrogramme.
De là, le lecteur nous fournira une image plus ou moins lisible sur laquelle on pourra lire le flag : `fl@g{LaSpectroCBi1}`

<!-- Review Taz : 20230322 -->