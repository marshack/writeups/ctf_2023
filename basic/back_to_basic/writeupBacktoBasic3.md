# WRITEUP BASIC / BACK TO BASIC 3
## Consigne :
```
Vous disposez d'une machine windows 10.

Deux comptes locaux sont présents mais vous ne possedez pas les mots de passe.
Parfois il ne faut pas chercher trop loin.

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 5 challenges
 - La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/17','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
Néant
```

## Serveur :
```
portailvms
```

## Points attribués :
```
5
```

## Hint : 
```
Néant
```

## Flag :
```
fl@g{mot2passetrivial}
```

## Solution : 
1ere étape se connecter sur la machine :

* 2 comptes présents sur la machine.
* Sur le compte cesar.shift saisir un mdp quelconque et valider.
* Valider le message d'erreur en cliquant sur OK
* Une indication de mot de passe s'affiche (votre mot de passe est incorrect) avec le mdp de l'utilisateur (incorrect)

2eme étape trouver le fichier contenant le flag :

* Sur le bureau de l'utilisateur, on peut voir un fichier zippé flag3.zip
* Récupérer le fichier zip et le coller sur sa machine kali linux.
* Créer un hash du dossier zippé à l'aide de la commande suivante: 
> `zip2john flag3.zip > /home/kali/Desktop/hash.txt`
* Découvrez le password grace à la commande suivante: 
> `john hash.txt`

On obtient le résultat suivant:
```
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 2 OpenMP threads
Proceeding with single, rules:Single
Press 'q' or Ctrl-C to abort, almost any other key for status
Almost done: Processing the remaining buffered candidate passwords, if any.
Proceeding with wordlist:/usr/share/john/password.lst
password         (flag3.zip/flag3.txt)     
1g 0:00:00:00 DONE 2/3 (2023-02-24 03:17) 14.28g/s 386142p/s 386142c/s 386142C/s 123456..Peter
Use the "--show" option to display all of the cracked passwords reliably
Session completed. 
```

Ouvrir le dossier zippé avec le mot de passe (`password`)
On découvre un fichier texte contenant le flag en base 64 ( `ZmxAZ3ttb3QycGFzc2V0cml2aWFsfQ==`)

3eme etape déchiffrer le mot de passe

* Se rendre sur le site https://www.dcode.fr/fr
* Dans le menu de droite cliquer sur Comment déchiffrer un message automatiquement?
* Puis cliquer sur tester la reconnaissance de code.
* Saisir la chaine de caractère trouvée puis cliquer sur analyser
* Dans les résultats à gauche, cliquer sur code base64, puis cliquer sur déchiffrer automatiquement.

<!-- Review Taz : 20230321 -->