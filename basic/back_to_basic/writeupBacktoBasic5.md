# WRITEUP BASIC / BACK TO BASIC 5
## Consigne :
```
Vous disposez d'une machine windows 10.

Deux comptes locaux sont présents mais vous ne possedez pas les mots de passe.
Parfois il ne faut pas chercher trop loin.

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 5 challenges
 - La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/17','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
Néant
```

## Serveur :
```
portailvms
```

## Points attribués :
```
5
```

## Hint : 
```
néant
```

## Flag :
```
fl@g{@lternateDataStream}
```

## Solution : 
1ere étape se connecter sur la machine :

* 2 comptes présents sur la machine.
* Sur le compte cesar.shift saisir un mdp quelconque et valider.
* Valider le message d'erreur en cliquant sur OK
* Une indication de mot de passe s'affiche (votre mot de passe est incorrect) avec le mdp de l'utilisateur (incorrect)

2eme étape trouver le fichier contenant le flag :

* Sur le bureau de l'utilisateur, un fichier `secret.txt` existe. Ce fichier contient des données additionnelles (**alternate data stream**).
* pour les visualiser :
    * ouvrir une invite de commande dos
    * se déplacer sur le bureau
    * lister les éléments du bureau à l'aide de la commande `dir /R`

```
C:\Users\cesar.shift\Desktop>dir /R
 Le volume dans le lecteur C n’a pas de nom.
 Le numéro de série du volume est C229-DF3C

 Répertoire de C:\Users\cesar.shift\Desktop

24/02/2023  13:02    <DIR>          .
24/02/2023  13:02    <DIR>          ..
24/02/2023  10:04               332 flag3.zip
22/02/2023  13:43               793 fond d ecran.jpg
                                287 fond d ecran.jpg:Zone.Identifier:$DATA
24/02/2023  13:03                44 secret.txt
                                 28 secret.txt:ads.txt:$DATA
24/02/2023  10:20    <DIR>          
               3 fichier(s)          794 033 octets
               3 Rép(s)   4 116 885 504 octets libres
```

On observe les données additionnelles du fichier sur la ligne `secret.txt:ads.txt:$DATA`

Visualisez ces données a l'aide de la commande `notepad secret.txt:ads.txt`

On obtient le flag: `fl@g{@lternateDataStream}`

<!-- Review Taz : 20230321 -->