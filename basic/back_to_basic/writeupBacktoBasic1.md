# WRITEUP BASIC / BACK TO BASIC 1
## Consigne :
```
Vous disposez d'une machine windows 10.

Deux comptes locaux sont présents mais vous ne possedez pas les mots de passe.
Parfois il ne faut pas chercher trop loin.

Démarrez un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 5 challenges
 - La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/17','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
Néant
```

## Serveur :
```
portailvms
```

## Points attribués :
```
5
```

## Hint : 
```
Néant
```

## Flag :
```
fl@g{decalagecesar}
```

## Solution : 

1ere étape se connecter sur la machine :

* 2 comptes présents sur la machine.
* Sur le compte cesar.shift saisir un mdp quelconque et valider.
* Valider le message d'erreur en cliquant sur OK
* Une indication de mot de passe s'affiche (votre mot de passe est incorrect) avec le mdp de l'utilisateur (incorrect)

2eme étape trouver le fichier contenant le flag :

* Sur le bureau de l'utilisateur, en obs 4*ervant la corbeille, on remarque qu'elle n'est pas vide.
* Double cliquer sur la corbeille, elle contient un fichier texte "motdepasse.txt".
* Restaurer ce fichier, il arrive sur le bureau.
* Ouvrir ce fichier, on obtient la chaine de caractere suivante:
> tm ntio mab lmkitiomkmaiz uiqa v wcjtqmh xia lm tm umbbzm awca tm jwv nwzuib

3eme étape décoder le flag :

* Se rendre sur le site https://www.dcode.fr/fr
* Dans le menu de droite, cliquer sur Comment déchiffrer un message automatiquement?
* Puis cliquer sur tester la reconnaissance de code.
* Saisir la chaine de caractère trouvée puis cliquer sur analyser
* Dans les résultats à gauche, cliquer sur *code cesar*, puis cliquer sur déchiffrer automatiquement.

On obtient le résultat :

* le flag est `decalagecesar` mais n oubliez pas de le mettre sous le bon format : `fl@g{decalagecesar}`

<!-- Review Taz : 20230321 -->