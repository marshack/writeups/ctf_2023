# WRITEUP BASIC / BACK TO BASIC 2
## Consigne :
```
Vous disposez d'une machine windows 10.

Deux comptes locaux sont présents mais vous ne possedez pas les mots de passe.
Parfois il ne faut pas chercher trop loin.

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 5 challenges
 - La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/17','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
Néant
```

## Serveur :
```
portailvms
```

## Points attribués :
```
5
```

## Hint : 
```
Néant
```

## Flag :
```
fl@g{metadonneesEXIF}
```

## Solution : 
1ere étape se connecter sur la machine.

* 2 comptes présents sur la machine.
* Sur le compte cesar.shift saisir un mdp quelconque et valider.
* Valider le message d'erreur en cliquant sur OK
* Une indication de mot de passe s'affiche (votre mot de passe est incorrect) avec le mdp de l'utilisateur (incorrect)

2eme étape trouver le fichier contenant le flag.

* Sur le bureau de l'utilisateur, on peut voir le fichier jpg qui sert de fond d'ecran.
* Sur ce fichier, faire un clic droit puis propriétés.
* Ensuite aller dans l'onglet Détails puis faire défiler l'ascenseur jusqu'aux informations concernant l'appareil photo.
* En face de Marque appareil photo on apercoit un flag : `fl@g{metadonneesEXIF}`

<!-- Review Taz : 20230321 -->