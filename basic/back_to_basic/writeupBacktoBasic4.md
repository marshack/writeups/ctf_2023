# WRITEUP BASIC / BACK TO BASIC 4
## Consigne :
```
Vous disposez d'une machine windows 10.

Deux comptes locaux sont présents mais vous ne possedez pas les mots de passe.
Parfois il ne faut pas chercher trop loin.

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 5 challenges
 - La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/17','_blank')" >Accéder au portail</button>
</div>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :
```
Néant
```

## Serveur :
```
portailvms
```

## Points attribués :
```
5
```

## Hint : 
```
néant
```

## Flag :
```
fl@g{iconetransparent}
```

## Solution : 

1ere étape se connecter sur la machine :

* 2 comptes présents sur la machine.
* Sur le compte cesar.shift saisir un mdp quelconque et valider.
* Valider le message d'erreur en cliquant sur OK
* Une indication de mot de passe s'affiche (votre mot de passe est incorrect) avec le mdp de l'utilisateur (incorrect)

2eme étape trouver le fichier contenant le flag :

* Sur le bureau de l'utilisateur, un dossier est caché (icone vide, texte vide).
* Pour le voir, ouvrir un explorateur Windows et se positionner sur le bureau.
* tout en haut de la liste on peut voir une ligne vide (![Alt text](/path/to/img.jpg "Optional title") image 1 à insérer dans le md).
* En double cliquant on ouvre le dossier et on trouve un fichier flag4.txt
* En ouvrant ce fichier on decouvre la chaine de caractere suivante 
> `102 108 64 103 123 105 99 111 110 101 116 114 97 110 115 112 97 114 101 110 116 125`

3eme etape dechiffrer le flag :

* Se rendre sur le site https://www.dcode.fr/fr
* Dans le menu de droite cliquer sur Comment déchiffrer un message automatiquement?
* Puis cliquer sur tester la reconnaissance de code.
* Saisir la chaine de caractere trouvée puis cliquer sur analyser
* Dans les résultats à gauche, cliquer sur code ASCII, puis cliquer sur déchiffrer automatiquement.

On obtient le flag fl@g{iconetransparent}

<!-- Review Taz : 20230321 -->