# WRITEUP IOT / FIRMWARE BACKDOOR
## Consignes :
```
NV1:

Depuis plusieurs semaines, votre caméra IP, téléopérée et administrée à distance, a un comportement étrange. 

Elle s'allume même lorsque vous ne la sollicitez pas. 

Vous êtes bien sûr conscient des risques de piratage et mettez son firmware à jour tous les mois.

> Format du flag : Chaine de caractères dont le masque est : `(.*)F(.*)L(.*)A(.*)G(.*)`

<p></p>

`e31a802e9191f5555835e090f68b06d8db9528b4`  [AXOOS_M1137_Mk_II_11_3_57.bin](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/IoT/Firmware_backdoor/AXOOS_M1137_Mk_II_11_3_57.bin)


NV2: 

Ce challenge est la continuité  du challenge firmware_backdoor part 1

Trouver la commande permettant à l'attaquant de passer root sur le système.

> Format du flag : Chemin complet du programme (`/../.../..`)

```

## Pièce jointe :
```
- la mise à jour du firmware: [AXOOS_M1137_Mk_II_11_3_57.bin](assets/AXOOS_M1137_Mk_II_11_3_57.bin)
```

## Serveur:
```
Néant
```

## Points attribués :
```
- part1: 20
- part2: 10
```

## Hint :
```
- La caméra est accessible sur les ports 443 et 22.
```

## Flag : 
```
SSH-F__L_A____G-SSH-EASY613
/usr/bin/vi
```

## Solutions

### Description
Le propriétaire de la caméra a téléchargé une mauvaise mise à jour du firmware incluant une clé SSH publique dissimulée dans le répertoire de l'utilisateur **gnats**. De plus **/usr/bin/vi** possède le bit SUID qui permet à l'attaquant de lancer des commandes en tant que **root**.

Le joueur devra donc explorer le firmware à la recherche de comptes cachés.

### Ressource
- la mise à jour du firmware: [AXOOS_M1137_Mk_II_11_3_57.bin](assets/AXOOS_M1137_Mk_II_11_3_57.bin)

### Explications 

#### Analyse du firmware
A l'aide de la commande file ou de 7z, nous constatons que le firmware se présente sous la forme d'une archive **tar.gz**. 

Après décompression avec la commande "*tar -xzvf*", nous découvrons, à l'aide de *file* les fichiers suivants:
```
- boot.img:   data
- dtb.img:    data
- fwinfo.bin: data
- info.json:  ASCII text
- kernel.img: Big-endian UTF-16 Unicode text, with no line terminators
- rootfs.img: Squashfs filesystem
- secmon.img: data
```

Quelques fichiers sont intéressants, tels que *boot.img* qui contient l'image de boot et le noyau ainsi que *rootfs* qui est l'image du système de fichiers Unix.

#### Montage du système de fichiers
L'image du système de fichiers est au format *squashfs*, facilement montable sur un Linux:

```bash
mount -t squashfs rootfs.img /media/
```

#### Analyse des comptes utilisateurs

La première chose à vérifier est la liste des utilisateurs présents sur le système à la recherche d'une anomalie. Pour commencer, recherchons les comptes avec un shell actif:

```bash
grep -v /usr/sbin/nologin /media/etc/passwd | grep -v /bin/false

root:x:0:0:root:/root:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/mologin
```

Le compte est *root* est normal, il sert à la maintenance et est tenu par le propriétaire de la caméra. Le suivant est un compte par défaut installé avec *rsync*. Par contre le troisième est anormal. Le nom du compte de service est courant sur un Linux par contre le shell **/usr/sbin/mologin** est erroné. Il ressemble à *nologin*.

De plus, son édition prouve qu'il ouvre un véritable shell:

```sh
#!/bin/sh

/bin/sh
```

#### Analyse des dossiers et fichiers de l'utilisateur gnats

Recherche des fichiers et répertoires appartenant à l'utilisateur **gnats**:

```bash
find /media -user 41

/media/var/lib/gnats
/media/var/lib/gnats/.ssh
/media/var/lib/gnats/.ssh/authorized_keys
```

Le répertoire de l'utilisateur **gnats** contient une clé SSH permettant à un attaquant de se connecter en SSH. Le contenu de cette clé donne le flag pour valider ce challenge:

```bash
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0Dm3jpzN2BLJwLHSZ8vhZTWKoJMLL3OEUREI2jNk2v3HYXbtx3551gOjszMsEJG5To2xkpkmroU16Fn0RymMudOrkpzSopLV7LWrYx91FrSJnfOUl7Oz26yYb7YmUiP7MuFVpRon7LHDThZgYfbSUImGaEcGob5brAdMDplQwoGBznraSFqPonyiv7KmvNeHRfzJfjEM3Wp1hFDQZtoB0WlvupQEeYLGqaIeW14enyL5ywKnx33xNgyDpi/Gk7AAYQoVnRQ7o9zIhOHAPlgGK8rV5wJ0nJ9qadirBnXw/JcSMDSv6KnZxEH9a99z8gST3W9EGfy+g0r1jsK8uyq0J SSH-F__L_A____G-SSH-EASY613
```

Cela est confirmé par la présente de l'utilisateur **gnats** dans le groupe **ssh-users**.

```bash
grep gnats /media/etc/group

gnats:x:41:
ssh-users:x:118:gnats
```

#### Recherche de fichiers avec SUID et GUID

L'attaquant n'a pas accès en **root** à la caméra, il faut donc chercher le moyen utilisé pour éléver ses privilèges. Sous Linux, une des méthodes la plus simple consiste à utiliser un programme avec le bit SUID:

```bash
find /media -perm /6000

/media/usr/bin/busybox.suid
/media/usr/bin/klog
/media/usr/bin/mount.util-linux
/media/usr/bin/newgrp.shadow
/media/usr/bin/su.shadow
/media/usr/bin/umount.util-linux
/media/usr/bin/vi
/media/usr/libexec/gstreamer-1.0/gst-ptp-helper
/media/usr/sbin/pwauth
/media/usr/sbin/suexec
```

Un programme n'a rien à faire dans cette liste. Il s'agit de **/usr/bin/vi**.

Pour information, en lançant **vi**, l'attaquant pourra exécuter des commandes en tant que **root** en tapant `":! <cmd>"`

<!-- Review Taz : 20230321 -->