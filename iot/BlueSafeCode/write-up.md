

# Blue safe code / Ludwig von unix

## Consignes :
```
Ba'al a installé deux nouveaux coffres forts connectés dans son vaisseau possédant une double authentifications:

- une clé secrète de 512 bits envoyé via Bluetooth;
- un code pin tapé physiquement sur le coffre, servant à chiffrer la première.

Notre équipe a réussi à ouvrir le premier coffre au C4 mais le deuxième nous résiste, ses parois sont renforcées au Naqahdah. Pas le choix, pour récupérer les plans de la futur attaque, vous devez retrouver la clé secrète ainsi que le code pin (en supposant qu'il utilise la même authentification partout).

Par chance, nous avons pu télécharger les données des cristaux du premier coffre et capturer une émission Bluetooth émis par sa télécommande goa'uld lors d'une ouverture.

Vous devez donc dans l'ordre:

- retrouver la clé secrète depuis la capture (flag1, hash md5 de la clé);
- retrouver la clé chiffrée de *baal* enregistrée dans le coffre (flag2);
- retrouver le code pin (flag 3);
- ouvrir le coffre avec votre propre équipement et lire le lieu de la future bataille (flag4).

> **Important: le dernier flag n'est accessible qu'aux joueurs en présentiel.**
```

## Pièces jointes :
- Etape 1:
  - **Fichier de capture Bluetooth**: [ble-simplifié.pcapng](assets/ble-simplifié.pcapng)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)
  - Bluetooth spécification: [ble_core_v5.0.pdf](assets/ble_core_v5.0.pdf)  
  - Bluetooth GATT introduction: https://www.oreilly.com/library/view/getting-started-with/9781491900550/ch04.html
- Etape 2:
  - **Dump du microcontrôleur**: [flash.img](assets/flash.img)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)

- Etape 2:
  - **Micro code en format ELF**: [firmware.elf](assets/firmware.elf)
  - Datasheet ESP32: [esp32-wroom-32_datasheet_en.pdf](assets/esp32-wroom-32_datasheet_en.pdf)
  - Schéma électronique simplifié: [electronique.png](assets/electronique.png)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)
- Etape 4: néant.

## Serveur:

Néant

## Points attribués :
Beaucoup

Total 100/100 voir plus

Répartition possible sur les 4 flags:

- flag 1: 20
- flag 2: 15
- flag 3: 70-80 à voir
- flag 4: 15

## Hint :

- Etape 1: néant;
- Etape 2: néant;
- Etape 3:  
  - Payant, *USE THE FORCE LUC*;
  - Gratuit, ouvrir l'ELF avec **ghidra** 9.2.4 équipé du plugin **[ghidra-xtensa](https://github.com/Ebiroll/ghidra-xtensa)** sur java 11.
  - Gratuit, https://www.highgo.ca/2019/08/08/the-difference-in-five-modes-in-the-aes-encryption-algorithm/ 
  - Gratuit, https://github.com/espressif/esp-idf/blob/master/components/mbedtls/port/include/aes/esp_aes.h
- Etape 4:
  -  Gratuit, pour l'ouverture réelle du coffre, boucler sur la connexion, en effet la connexion au BLE est un peu capricieuse.



## Flag : 
- **Flag 1**: 113ba7bb61475d4b690d7331055895ff
- **Flag 2**: 9aaa2949da8828d8aa5204120243ba5c
- **Flag 3**: 19972007
- **Flag 4**: Praclarush_Taonas

Write-Up Challenge : FTFD
====================================================================

Description
-----------

Le challenge consiste à ouvrir un coffre réel commandé par Bluetooth, protégé par une clé de 512 bits secrète et un code PIN. Comme indiqué dans la documentation, la clé secrète est chiffrée par le code PIN en AES 128bits puis comparée aux clés enregistrées dans le micro-contrôleur.

Les étapes de résolution sont les suivantes:

1. Le clé secrète est transmise en clair au coffre, elle donc récupérable dans le fichier PCAP;
2. La clé enregistrée est stockée sous forme de clé valeur dans un des segments de la mémoire du micro-contrôleur. A l'aide d'un outil, il est possible de l'extraire;
3. Lors de l'étude du code assembleur, on se rend compte que le chiffrement AES est mal implémenté et que la clé (ici le code PIN) est cassable via un brute force;
4. Une fois la clé secrète et le code PIN obtenus, il faut se connecter au coffre via un outil tel que RFXXX ou un script python et lancer la commande d'ouverture.

Ressource
---------
- Etape 1:
  - **Fichier de capture Bluetooth**: [ble-simplifié.pcapng](assets/ble-simplifié.pcapng)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)
  - Bluetooth spécification: [ble_core_v5.0.pdf](assets/ble_core_v5.0.pdf)  
  - Bluetooth GATT introduction: https://www.oreilly.com/library/view/getting-started-with/9781491900550/ch04.html
- Etape 2:
  - **Dump du microcontrôleur**: [flash.img](assets/flash.img)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)
- Etape 2:
  - **Micro code en format ELF**: [firmware.elf](assets/firmware.elf)
  - Datasheet ESP32: [esp32-wroom-32_datasheet_en.pdf](assets/esp32-wroom-32_datasheet_en.pdf)
  - Schéma électronique simplifié: [electronique.png](assets/electronique.png)
  - Manuel utilisateur du coffre: [manuel_utilisateur.pdf](assets/manuel_utilisateur.pdf)
- Etape 4: néant.

Solution
--------
### Analyse des trames BLE

#### Phase d'initialisation:

![](img/ble-init.png)

Lors de la phase d'initialisation, le coffre se déclare aux potentiels clients (trame n°7). Il donne des informations telles que son nom, son adresse MAC, dans notre cas, respectivement "**SafeCode32**" et **40:22:d8:78:c9:ba**.

Une fois l'adresse connue, le client se connecte au périphérique et scan les **Characteristics** disponibles de 0x0001 à 0xffff jusqu'à obtenir une erreur (trame 23 à 37). Ce qui donne:

| UUID                               | Fonction                   | handle | type       | properties                    |
| ---------------------------------- | -------------------------- | ------ | ---------- | ----------------------------- |
| 0x2a05                             | Service Changed            | 0x0002 | properties | indicate                      |
|                                    |                            | 0x0003 | value      |                               |
| 0x2a00                             | Device Name                | 0x0015 | properties | read                          |
|                                    |                            | 0x0016 | value      |                               |
| 0x2803                             | Apperance                  | 0x0017 | properties | read                          |
|                                    |                            | 0x0018 | value      |                               |
| 0x2aa6                             | Central address resolution | 0x0019 | properties | read                          |
|                                    |                            | 0x001a | value      |                               |
| 0x065de41b79fb479db59247caf39bfbbb | *non standard*             | 0x0029 | properties | indicate, notify, write, read |
|                                    |                            | 0x002a | value      |                               |
| 0x065de41b79fb479db59247caf39bfccc | *non standard*             | 0x002c | properties | indicate, notify, write, read |
|                                    |                            | 0x002d | value      |                               |
| 0x065de41b79fb479db59247caf39bfddd | *non standard*             | 0x002f | properties | write, read                   |
|                                    |                            | 0x0030 | value      |                               |

Les quatre premiers UUID font parties du standard BLE, ils servent à obtenir des information sur le périphérique telles que le nom de l'appareil. Aucune de ces **Characteristics** n'est accessible en écriture (write), nous pouvons en déduire que les ordres ne seront pas envoyés via ces derniers.

Les trois suivants , ne sont pas standard, ils sont dît  *propriétaires* et permettent l'envoi d'ordre, ils seront donc à surveiller.

> **Note**: les **Characteristics** sont des registres du périphérique BLE modifiables (commandes) ou non (ex: capteur de température), permettant d'échanger avec le client.

#### Premiers échanges:

![](img/ble-notify.png)

Les handles 0x002e et 0x002b n'ont pas été déclarés lors de la phase initialisation. Il s'agit en fait du **Client Characteristic Configuration Descriptor** (CCCD), dont le handle est celui de la **Characteristic**+2 (ble core p. 2239).

<img src="img/cccd.png" style="zoom:80%;" />

Cette commande permet d'indiquer au coffre que le client veut recevoir des notifications provenant des UUID 0x065de41b79fb479db59247caf39bfbbb et 0x065de41b79fb479db59247caf39bfccc.

#### Echanges des commandes:

Les échanges suivants utilisent *prepare to write* à la place de *write*. Cette commande est utilisée lorsque la quantité de données est supérieure au MTU. L'information est donc segmentée. Le périphérique, lui, acquitte chaque segment en renvoyant la donnée reçue puis attend la commande *execute write* pour mettre à jour le registre.

En prenant un peu de recul, l'ensemble des échanges peut se résumer ainsi:

| Trames  | handles | commandes | données (désgementées)                                       |
| ------- | ------- | --------- | ------------------------------------------------------------ |
| 44 à 58 | 0x002a  | write ->  | 0x6261616c00000000000033890457aafeffa122bcab6bff901a23415a6a7371646c6b6a71464748737a7233008957aafecca122bcab6bff901a23685a6a7371646c6b6b6b6b6b6b6b7a72 |
|         |         |           | baal\0x00\0x00\0x00\0x00\0x00\0x00\0x33\0x89.................. |
| 59      | 0x002a  | notify <- | 0x10494e464f203a2070696e20636f6465206e6565                   |
|         |         |           | \0x10INFO : pin code nee                                     |
| 61      | 0x002a  | notify <- | 0x11494e464f203a20646f6f72206f70656e65642e                   |
|         |         |           | \0x11INFO : door opened.                                     |
| 62 à 76 | 0x002d  | write ->  | 0x6261616c00000000000045050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763 |
|         |         |           | baal\0x00\0x00\0x00\0x00\0x00\0x00\0x45\0x05.................. |
| 77      | 0x002d  | notify <- | 0x10494e464f203a2070696e20636f6465206e6565                   |
|         |         |           | \0x10INFO : pin code nee                                     |
| 79      | 0x002d  | notify <- | 0x13494e464f203a206b657920757064617465642e                   |
|         |         |           | \0x13INFO : key updated.                                     |
| 80 à 81 | 0x002a  | write ->  | 0x434c4f5345                                                 |
|         |         |           | CLOSE                                                        |
| 83      | 0x002a  | notify <- | 0x12494e464f203a20646f6f7220636c6f7365642e                   |
|         |         |           | \0x12INFO : door closed.                                     |

De façon plus synthétique, nous avons:

1. Envoi de 74 octets de données contenant la châine "baal" sur le handle 0x002a;
2. Réception d'une notification "pin code nee[ded]";
3. Réception d'une notification " door opened", 10 secondes plus tard;
4. Envoi de 74 octets de données contenant la châine "baal" sur le handle 0x002d;
5. Réception d'une notification "pin code nee[ed]";
6. Réception d'une notification "key updated";
7. Envoi de la châine "CLOSE" sur le handle 0x002a;
8. Réception d'une notification "door closed";

Nous pouvons en déduire que *baal* a ouvert le coffre avec sa clé secrète et son code pin, puis, a ensuite mis à jour ses données d'authentification, et enfin, a refermé le coffre.

#### Détermination de la clé secrète:

Dans les derniers échanges, *baal* a mis à jour sa clé. Cette dernière doit être présente dans les trames 62 à 76 dont la longueur des donneés est de 74 octets.

Hors, la documentation du coffre nous indique que la clé est à une taille de 512 bits soit 64 octets et que la taille du nom associé à cette clé doit être inférieur ou égale à 10. Cela correspond exactement aux informations échangées. En supposant qu'il y ait un bourrage de 0 à la suite du nom pour attendre les 10 caractères, nous avons:

- nom de la clé: **baal** + bourrage
- clé secrète: **0x45050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763**

Calcul du hash md5 afin d'obtenir le flag n°1:

```python
import hashlib

CLE = bytes.fromhex("45050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763")

hashlib.md5(CLE).hexdigest()

'113ba7bb61475d4b690d7331055895ff'
```



### Extraction des données du coffre depuis l'image du firmware

Une seule image flash d'un ESP32’s peut contenir de multiples applications ainsi que de nombreuses sortes de données (données de calibrage, systèmes de fichiers, stockage de paramètres, etc...). L'image est donc divisée en partitions, une au moins pour chaque artéfact.

> **Note**: le dump du firmware s'effectue avec la commande suivante:
>
> ```bash
> esptool.exe -b 115200 --port COM5 read_flash 0x00000 0x400000 flash.bin
> ```

Pour trouver où et comment sont stockées les clés chiffrées, il faut donc commencer par afficher la table des partitions à l'aide de l'outil [esp32 image parser](https://github.com/tenable/esp32_image_parser).

```bash
$ python3 esp32_image_parser.py show_partitions flash.bin

reading partition table...
entry 0:
  label      : nvs
  offset     : 0x9000
  length     : 20480
  type       : 1 [DATA]
  sub type   : 2 [WIFI]

entry 1:
  label      : otadata
  offset     : 0xe000
  length     : 8192
  type       : 1 [DATA]
  sub type   : 0 [OTA]

entry 2:
  label      : app0
  offset     : 0x10000
  length     : 1310720
  type       : 0 [APP]
  sub type   : 16 [ota_0]

entry 3:
  label      : app1
  offset     : 0x150000
  length     : 1310720
  type       : 0 [APP]
  sub type   : 17 [ota_1]

entry 4:
  label      : spiffs
  offset     : 0x290000
  length     : 1441792
  type       : 1 [DATA]
  sub type   : 130 [unknown]

entry 5:
  label      : coredump
  offset     : 0x3f0000
  length     : 65536
  type       : 1 [DATA]
  sub type   : 3 [unknown]
```

D'après [partition-tables.html](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/partition-tables.htm), le dump du firmware présente 5 partitions:

- **nvs**: stockage d'informations sous la forme de clé/valeur;
- otadata: données utilisées par le bootloader pour charger l'application ou lors de mises à jour;
- app1/app2, de type ota qui est le code du firmware;
- **spiffs**: système de fichiers *spiffs* [doc](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/spiffs.html);
- coredump: données de débuggage en cas de crash.

Deux partitions peuvent stocker de la donnée: le NVS ou le SPIFFS. On commence par dumper le SPIFFS avec l'outil **esp32_image_parser**:

```bash
$ python3 esp32_image_parser.py dump_partition flash.bin  -partition spiffs
Dumping partition 'spiffs' to spiffs_out.bin
```

Un hexdump montre que celui-ci est vide:

```bash
$ hexdump spiffs_out.bin
0000000 ffff ffff ffff ffff ffff ffff ffff ffff
*
0160000
```

Passons maintenant à l'extraction des données présentes dans le **NVS**:

```bash
$ python3 esp32_image_parser.py dump_nvs flash.bin -partition nvs -nvs_output_type json | tail -n -1 | jq . > nvs.json
```

En filtrant les *entry_state* avec l'état *written*, nous trouvons une clé nommée *baal* dans l'espace de nom *safecode* de type *BLOB_DATA*.

````json
      {
        "entry_state": "Written",
        "entry_ns_index": 3,
        "entry_ns": "safecode",
        "entry_type": "BLOB_DATA",
        "entry_span": 3,
        "entry_chunk_index": 128,
        "entry_key": "baal",
        "entry_data_type": "BLOB_DATA",
        "entry_data_size": 64,
        "entry_data": "aWvpu7KRgI2tA72Fi4Y3h+dEm2hLybCRkk7RpYBU36OpC0oNCYCHfUebDhxcaSSttKg6oXEWnuYQQdoEbzoQYQ=="
      },
````

Ce bloc est encodé en base 64, pour le décoder, un simple code python suffit:

```python
#!/usr/bin/python3

base64.b64decode("aWvpu7KRgI2tA72Fi4Y3h+dEm2hLybCRkk7RpYBU36OpC0oNCYCHfUebDhxcaSSttKg6oXEWnuYQQdoEbzoQYQ==")

b'ik\xe9\xbb\xb2\x91\x80\x8d\xad\x03\xbd\x85\x8b\x867\x87\xe7D\x9bhK\xc9\xb0\x91\x92N\xd1\xa5\x80T\xdf\xa3\xa9\x0bJ\r\t\x80\x87}G\x9b\x0e\x1c\\i$\xad\xb4\xa8:\xa1q\x16\x9e\xe6\x10A\xda\x04o:\x10a'

```

Pour avoir son hash MD5 afin d'obtenir le flag:

```python
import hashlib
import base64

CLE = base64.b64decode("aWvpu7KRgI2tA72Fi4Y3h+dEm2hLybCRkk7RpYBU36OpC0oNCYCHfUebDhxcaSSttKg6oXEWnuYQQdoEbzoQYQ==")

hashlib.md5(CLE).hexdigest()

'9aaa2949da8828d8aa5204120243ba5c'
```



### Craquage de la clé de chiffrement (code pin)

D'après la documentation du coffre, la clé secrète de l'utilisateur est chiffrée en AES 128 bits à partir du code PIN. **Normalement, une clé AES n'est pas cassable dans un temps raisonnable mais ici, le développeur a pris quelques raccourcis avec l'état de l'art**.

> **Note**: pour que le challenge soit résolvable dans un temps restreint, nous avons fournis aux joueurs le firmware au format ELF mais sans symboles.

#### Ouverture du fichier ELF

Ouvrir l'ELF avec **ghidra** 9.2.4 équipé du plugin **[ghidra-xtensa](https://github.com/Ebiroll/ghidra-xtensa)** sur java 11 (indiqué dans l'énoncé ou l'aide).

#### Recherche de chaînes connues

Rechercher la chaîne *"door opened"* reçue en notification Bluetooth indiquant que la porte a été ouverte. Deux chaînes sont présentes, la deuxième à l'adresse 0x3f4004c1 est référencée par une fonction à l'adresse 0x400d339c.

Dans cette fonction, la chaîne *"code pin needed"* apparait au début,  *"door opened"* au milieu et *"invalid key"* à la fin. On a donc bien la fonction vérifiant la clé, le code pin et ouvrant la porte.

De plus, une seule fonction utilise ces chaînes en paramètres, on peut supposer qu'il s'agisse de la notification.

![](img/fonction.png)

D'après le pseudo code, la porte est ouverte si et seulement si *iVar2* est égale à zéro (ligne 23). Il faut donc étudier toutes les fonctions de la ligne 17 à 22, soit:

- FUN_400d312c
- FUN_400d3960
- FUN_400d3258
- FUN_400901a4

#### Etude de la fonction FUN_400d312c

![](img/fonction_pin.png)

De la ligne 10 à 14, la zone mémoire de taille 0x10 (16 octets) à l'adresse du pointeur *param_1* (de type char) est initialisée à la valeur **x0ff**.

Lignes 15 et 16, en faisant un parallèle avec la documentation, il est possible que ces deux fonctions soit le lancement des bips (2 x (250ms + 250ms)) et l'éclairage du coffre.

De la ligne 18 à 29, nous avons une boucle qui alimente la zone mémoire à l'adresse *param_1* et se ne termine qu'à trois conditions:

- au bout de la 0x10ème boucle (16ème boucle);
- lorsque FUN_400d3f68 renvoie **"#"**;
- lorsque FUN_400d3f68 renvoie **"*"**;

Ces éléments correspondent exactement à la documentation du coffre, qui dit que le code PIN ne peut dépasser 16 chiffres, qu'il est validé par un **#** ou annulé par un *****.

Cette fonction est donc la fonction de lecture du code PIN, stocké à l'adresse du paramètre et renvoit soit la taille du code (ligne 31), soit *-1* si le code est annulé.

On peut mettre à jour les noms des variables et fonctions dans la fonction principale:

![](img/fonction2.png)

#### Etude de la fonction FUN_400d3960

![](img/fonction_nvs.png)

Cette fonction contient des éléments de débogage (*user_nvs.cpp*, *s_esp_err_t_nvs_get_key*) qui nous indique que l'on récupère ici le contenu d'une clé NVS à partir de son nom.

Lors de l'export du NVS, nous avons trouvé une clé nommée *baal* de type blob. En cherchant dans la documentation la fonction pour écrire dans un blob, on trouve: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/nvs_flash.html

<img src="img/nvs.png" style="zoom:80%;" />

Ce qui correspond à la signature de la fonction ligne 10. On en déduit que *param_1* est le nom de la clé, ici *baal*, *param_2* le buffer qui va recevoir la clé chiffrée contenu dans le NVS. 

Le dernier paramètre **0x40** confirme la taille de la clé qui est de 512 bits (64*8).

La fonction d'ouverture de la porte devient donc:

![](img/fonction3.png)

Si la clé est trouvée, l'algorithme passe à l'étape suivante (lignes 19 et 20) .

#### Etude de la fonction FUN_400d3258

![](img/fonction_aes.png)

Cette fonction prend en entrée le code PIN, l'adresse de la chaine contenant le nom de la clé +10 (clé secrète ???) et un buffer non identifié pour le moment (initialisé à 0, lignes 10 à14). De plus, elle contient des informations de débogage mettant sur la piste d'un chiffrement AES avec la libraire *mbetls*.

D'après https://www.highgo.ca/2019/08/08/the-difference-in-five-modes-in-the-aes-encryption-algorithm/ , en AES les données sont chiffrées par bloc de 128 bits soit 16 octets ou 0x10. Hors, nous avons effectivement une boucle (lignes 18 à 33) qui traite les données en entrée par bloc de 16 octets (ligne 21).

Dans https://github.com/espressif/esp-idf/blob/master/components/mbedtls/port/include/aes/esp_aes.h, nous avons 7 fonctions de chiffrement:

- esp_aes_crypt_ecb
- esp_aes_crypt_cbc
- esp_aes_crypt_cfb128
- esp_aes_crypt_cfb8
- esp_aes_crypt_ctr
- esp_aes_crypt_ofb
- esp_internal_aes_encrypt

Seule la fonction **esp_aes_crypt_ecb(unsigned char *ctx, int mode, const unsigned char *input, unsigned char *output)**, correspond au nombre et types de paramètres (4), les autres nécessitent un vecteur d'initialisation (IV).

La fonction d'ouverture de la porte devient:

![](img/fonction4.png)

#### Etude de la fonction FUN_400901a4

![](img/fonction_memcmp.png)

Cette fonction est lancée avec deux pointeurs de type char* et *0x40* en troisième paramètre.

Si *param_3* est supérieur ou égale à 4 et que les adresses des buffers sont des multiples de 4, les données sont comparées 4 à 4 octets (32 bits), lignes 13, 14 et 15. Sinon, et pour les données restantes (*param_3* modulo 4), elles sont comparées octet par octet (ligne 25, 26 et 28).

Nous pouvons en déduire que cette fonction est semblable à **memcmp()**.

#### Analyse finale de la fonction de la demande d'ouverture

![](img/fonction5.png)

Après identification des fonctions, nous en déduisons que le coffre s'ouvre si la clé chiffrée interne est égale à la clé secrète Bluetooth chiffrée en **AES ECB** avec le code PIN comblée par des **0xff** pour obtenir une clé de 128 bits. 

#### Cassage de la clé en brute force

Avec cette mauvaise implémentation, le nombre de combinaison passe de 256^16 à 10^(longueur code pin) possibilités. L'exemple suivant, est un code python permettant de casser cette clé séquentiellement ou de façon distribuée avec la commande **parallel**.

Temps de cassage (Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz 6 cœurs):

- monotâche: **15 min** environ;
- distribué 12 cœurs (hyperthreading): **1 min**.

``` python
#!/usr/bin/python3
#
# Usage:
# ./bruteforce.py gen | parallel --pipe -N 10000 ./bruteforce.py para
# ./bruteforce.py
#
import itertools
import sys
import os
from Crypto.Cipher import AES
import base64

#key = b'19972007XXXXXXXX'
#cipher = AES.new(key, AES.MODE_ECB)
#c = cipher.encrypt(b'AAAAAAAAAAAAAAAA')
c = base64.b64decode("aWvpu7KRgI2tA72Fi4Y3h+dEm2hLybCRkk7RpYBU36OpC0oNCYCHfUebDhxcaSSttKg6oXEWnuYQQdoEbzoQYQ==")
BLE_KEY = bytes.fromhex("45050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763")

def gen(code_size_max=16, padding="X"):
    for depth in range(1,code_size_max+1):
        for i in itertools.product("0123456789", repeat=depth):
            yield bytes("".join(i), encoding="UTF-8") + b'\xff'*(code_size_max-depth)

def crypt(key):
    cipher = AES.new(key, AES.MODE_ECB)
    if c == cipher.encrypt(BLE_KEY):
        print(key)
        os.system("date")
        sys.exit(0)


if len(sys.argv) < 2:
    for key in gen():
        print(key)
        crypt(key)
   
if sys.argv[1] == "para":
    while True:
        k = sys.stdin.buffer.read(16)
        sys.stdin.buffer.read(1) # \n pour parallel
        if len(k) == 0:
            break
        #print("k", k)
        crypt(k)

    sys.exit(1)

if sys.argv[1] == "gen":
    for k in gen():
        sys.stdout.buffer.write(k)
        sys.stdout.buffer.write(b'\n')
    sys.exit(0)
```

**Sortie du script: 19972007**

### Ouverture du coffre

Nous possédons maintenant toutes les informations nécessaires pour ouvrir le code:

- le nom du coffre: **SafeCode32**;
- UUID Characteristic: **065de41b79fb479db59247caf39bfbbb**;
- le nom de la clé: **baal**;
- la clé secrète: **0x45050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763**;
- le code pin: **19972007**.

Il est possible d'ouvrir le coffre soit en utilisant une application telle que **nRF Connect** sur smartphone, soit en développant un script python sur PC équipé d'une carte ou dongle Bluetooth.

> **INFO**: le coffre ne peut être ouvert avec l'utilitaire **gattool** en ligne de commande, car il ne permet pas d'envoyer des données binaires.

#### Solution python

```python
#!/usr/bin/python3

import pygatt
import time

DEVNAME = "SafeCode32"
DOOR_CHAR = "065de41b-79fb-479d-b592-47caf39bfbbb"

CLE = bytes.fromhex("45050456bbfeffa166bbaa6b003f1a22415a4552545955494f5051534446474840050956cbfefca299bbaa6b003f1a22415a4552545975757575757575464763")
DATA = b'baal'+bytes([0, 0, 0, 0, 0, 0]) + CLE


def handler_notify(handler, data):
    print("NOTIFY: ", data)


adapter = pygatt.GATTToolBackend()
adapter.start()

print("Recherche du coffre ...")
devices = adapter.scan()
address = None
for d in devices:
    if d['name'] == DEVNAME:
        address = d['address']
        break
if address == None:
    raise Exception("Coffre introuvable.")

print("Connexion ...")
connected = False
device = None
while not connected:
    try:
        device = adapter.connect(address, timeout=1)
        print("Connected.")
        connected = True
    except pygatt.exceptions.NotConnectedError:
        pass

# Activer les notification
device.subscribe(DOOR_CHAR, handler_notify)
time.sleep(.5)

# Ouverture du coffre
device.char_write(DOOR_CHAR, DATA)

# Attendre pour voir les notifications
input("end ...")
device.disconnect()
adapter.stop()
```

