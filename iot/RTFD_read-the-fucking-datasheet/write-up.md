# WRITEUP IOT / RTFD Read The Fucking Datasheet
## Consignes :
```
Retrouver la température à partir de la capture effectuée sur un bus i2C et de la documentation technique fournies en pièce jointe.

Le flag est au format **XX.XX** où X est un chiffre compris entre 0 et 9.

<p></p>

`c24d9fad2d5c46c186a80db7d993c70fec07b3a9`  [capture.sal](https://download.marshack.fr/4e9ac9a3fa185a08d21662cde2a07bd1a8803728/IoT/ReadTheFuckingDatasheet/capture.sal)

> Format du flag : fl@g{XX.XX}
```
## Pièces jointes :
- Fichier de capture: [capture.sal](assets/capture.sal)
- Datasheet: [BME280.PDF](assets/BME280.PDF)
- Documentation sur le Bus I2C: [Bus_I2C.pdf](assets/Bus_I2C.pdf)

## Serveur:

Néant

## Points attribués :
40

## Hint :
- gratuit: Le fichier de capture est au format **Saleae Logic** (téléchargeable gratuitement, Linux et Windows).
- payant: 
  - **dig_T1**: 0x6FC0, soit 28608;
  - **dig_T2**: 0x6792 soit 26514;
  - **dig_T3**: 0x0032, soit 50.


## Flag : 
20.71

## Explications

### Description
Lors de ce challenge, les joueurs devront retrouver la température mesurée par un capteur BME280 dont la documentation est fournie en pièce jointes. Le décodage de la partie "signal" bas niveau du protocole I2C est assuré par l'analyseur logique **Logic**. Le but ici est donc d'effectuer une analyse des trames décodées par le logiciel, de comprendre les échanges entre l'hôte et le capteur et de retrouver les données et paramétres afin de calculer la température.

### Ressource
**Fichier challenge** : 

- Fichier de capture: [capture.sal](assets/capture.sal)
- Datasheet: [BME280.pdf](assets/BME280.pdf)
- Documentation sur le Bus I2C: [Bus_I2C.pdf](assets/Bus_I2C.pdf)

**Code source** :

- Example de décodeur en python pour Raspberry PI: [bme280.py](src/bme280.py)

### Première lecture de la capture

![logic](img/logic.png)

Après ouverture de la capture avec le logiciel **Logic**, nous constatons que le signal I2C est déjà décodé. En accord avec le datasheet p33:

- Une écriture dans un registre s'effectue en envoyant l'adresse du capteur (ici 0x77), du bit d'écriture, suivie de l'adresse du registre puis de la donnée;
- Une lecture dans un registre s'effectue en envoyant l'adresse du capteur (ici 0x77), du bit d'écriture, suivie de l'adresse du registre puis de nouveau l'adresse du capteur (ici 0x77), du bit de lecture (trame ci-dessus: lecture des registres 0xF7, 0xF8, 0xF9 etc ...);

![](img/trames.png)

En dézoomant, nous observons trois trames distinctes qu'il faut analyser.

### Première trame

![](img/trame1.png)

Sur cette trame, l'hôte lit deux octets au niveau du registre 0xD0. D'après la documentation, cela correspond à l'ID du capteur soit: 0x60:

![](img/id.png)

> **Cette action n'a pas d'impact sur le calcul de la température.**

### Deuxième trame

![](img/trame2.png)

Sur cette trame, l'hôte écrit **0x49** dans le registre **0xF4**. D'après la documentation p29, ce registre "*ctrl_meas*" sert à configurer la mesure de la température et du taux d'humidité.

Il est composé de trois sous registres:

- osrs_t: 010 -> suréchantillonnage×2 du capteur de température (réduction de bruit);
- osrs_p: 010 -> suréchantillonnage×2 du capteur de pression  (réduction de bruit);
- mode:  01 -> force le lancement d'une mesure.

> **Cette configuration n'a pas d'impact sur le calcul de la température, elle configure et lance les mesures sur les capteurs.**

### Troisième trame: constantes de compensation

![](img/trame3.png)

La troisième trame est une lecture des registres de **0x88** à **0x9F**. Ces registres contiennent les constantes de *trim* pour effectuer les calculs de compensation (datasheet page 24). 
> **Ces données sont essentielles au calcul de la température**.

![](img/trim.png)

Seules les constantes utiles au calcul de la température nous intéressent, soient les 6 premiers octets: C06F92673200. Ce qui donne:

- **dig_T1**: 0x6FC0, soit 28608;
- **dig_T2**: 0x6792 soit 26514;
- **dig_T3**: 0x0032, soit 50.

### Quatrième trame: lecture de la température brute

![](img/trame4.png)

Sur la quatrième trame, l'hôte lit 8 octets à partir du registre **0xF7** correspondant aux valeurs mesurées brutes par les capteurs de température, d'humidité et de pression. Pour retrouver la température brute mesurée, il faut s'aider du tableau page 31 suivant:

![](img/f7_register.png)

A partir de **0xF7** , les trois premiers octets correspondent à la pression, les trois suivants **0xFA** à **0xFC** à la température. La lecture de cette dernière est décrite dans le tableau n°30 page 31:

![](img/fa_register.png)

Soit:

**temp_raw = 0x7F << 12 + 0xBE << 4 + 0x00 >> 4 = 0x07FBE0 = 523232**

### Calcul de la température

Un exemple de fonction en C pour calculer la température compensée est donné dans la documentation technique:

![](img/C_code.png)

Il suffit donc soit de copier ce code tel quel en C, soit de le réécrire en python et de lancer la fonction suivante:

```python
#!/usb/bin/python3

def get_temperature(temp_raw, dig_T1, dig_T2, dig_T3):
    var1 = (((temp_raw>>3) - (dig_T1<<1)) * dig_T2) >> 11
    var2 = (((((temp_raw>>4) - dig_T1) * ((temp_raw>>4) - dig_T1))) >> 12) * dig_T3 >> 14
    t_fine = var1 + var2
    T = (t_fine * 5 + 128) >> 8
    return T/100

print(get_temperature(523232, 28608, 26514, 50))

```

Ce qui donne: **20.71**

fl@g{20.71}

<!-- Review Taz : 20230321 -->