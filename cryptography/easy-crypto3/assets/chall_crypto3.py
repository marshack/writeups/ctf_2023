#!/usr/bin/python3 -u

import sys
from ssl import RAND_bytes
import server

def getRandom(length):
    return RAND_bytes(length)

def encrypt(key, data):
    salt=getRandom(6)
    output=[]
    last=0
    for i in range(len(data)):
        last=data[i] ^ key[i % len(key)] ^ salt[i % len(salt)] ^ last
        output.append(last)
    return salt+b"".join([bytes([c]) for c in output])

def readKeyFromFile(filename):
    with open(filename, mode="rb") as f:
        return f.read()

def callbackRead(data):
    key=readKeyFromFile("/home/crypt03/secret")
    return encrypt(key, data)

if __name__ == "__main__":
    server.init("0.0.0.0", 40004)
    server.setCallbackReadData(callbackRead)
    server.start()
