# WRITEUP CRYPTOGRAPHY / EASY-CRYPTO 3
## Consigne:

```
Nous avons intercepté un message chiffré (fichier à télécharger *cipher3.dat*), merci de le déchiffrer.

Vous avez accès au code source du programme qui chiffre (*chall_crypto3.py*), ainsi qu'à l'instance qui a chiffré ce message.

`nc game1.marshack.fr 42003`

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
cipher3.dat
chall_crypto3.py
```

## Serveur :

```
game1.marshack.fr:42003
```

## Points attribués :

```
25
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{EnfinUn5ecretPr0tege}
```

## Solution : 


On constate dans le code source que le message à chiffrer est "xOré" avec :

 - la clé
 - un sel (salt)
 - avec le caractère chiffré n-1
 
Le message chiffré est ensuite concaténé au sel en sortie. Il est également possible de retrouver la clé en codant notre propre fonction de déchiffrement.

Je n'envoie que des '0' (64 fois de façon arbitraire) :
```bash
python3 -c 'import sys;sys.stdout.buffer.write(b"\x00"*64)' | nc game1.marshack.fr 42003 | python3 -c 'import sys;print(sys.stdin.buffer.read())'
b'4\xdb\xa7,\xb9\x902\xe6\xd3Vm\x9e\x07\x94\xa1!\xa9\xd6\xb7\xacD\xf7H\xd7q\x03&i}\xa5\x03t\xe2!\xcd\x9d\xe6\xa2\x03 \x0b2\x84<6RyE@t\x86j\x9c\x93\xa1u@\xc5\xfe\r\x94\x072\xb2:E$?\xd7d'
```

On récupère le sel (6 octets) et le message chiffré, on retrouve donc la clé  :
```python
salt_and_cipher=b'4\xdb\xa7,\xb9\x902\xe6\xd3Vm\x9e\x07\x94\xa1!\xa9\xd6\xb7\xacD\xf7H\xd7q\x03&i}\xa5\x03t\xe2!\xcd\x9d\xe6\xa2\x03 \x0b2\x84<6RyE@t\x86j\x9c\x93\xa1u@\xc5\xfe\r\x94\x072\xb2:E$?\xd7d'
salt=salt_and_cipher[0:6]
cipher=salt_and_cipher[6:]
key_list=[]
last=0
for i in range(len(cipher)):
    key_list.append(cipher[i] ^ salt[i % len(salt)] ^ last)
    last=cipher[i]

key=b"".join([bytes([c]) for c in key_list])
print("KEY:", key)
```

Ce qui affiche :
```
KEY: b'\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f'
```

On remarque un motif répétitif : `\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f`. 

Il s'agit de la clé et est de longueur 16 octets (128 bits).

On a maintenant la clé, on peut déchiffrer le message :
```python
with open("cipher3.dat", mode="rb") as f:
    salt_and_cipher=f.read()

salt=salt_and_cipher[0:6]
cipher=salt_and_cipher[6:]

key=b'\x06\x0f\x92\xa9\x82c\xadH\x92\xac1\xefU\xc0O\x9f'
output=[]
last=0
for i in range(len(cipher)):
    output.append(cipher[i] ^ key[i % len(key)] ^ salt[i % len(salt)] ^ last)
    last=cipher[i]
print(b"".join([bytes([c]) for c in output]).decode())
```

Ce qui affiche :

```
fl@g{EnfinUn5ecretPr0tege}
```

<!-- Review Taz : 20230321 -->