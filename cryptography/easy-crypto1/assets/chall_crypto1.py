#!/usr/bin/python3 -u

import sys
import server

def encrypt(key, data):
    output=[]
    for i in range(len(data)):
        output.append(data[i] ^ key[i % len(key)])
    return b"".join([bytes([c]) for c in output])

def readKeyFromFile(filename):
    with open(filename, mode="rb") as f:
        return f.read()

def callbackRead(data):
    key=readKeyFromFile("/home/crypt01/secret")
    return encrypt(key, data)

if __name__ == "__main__":
    server.init("0.0.0.0", 40002)
    server.setCallbackReadData(callbackRead)
    server.start()
