# WRITEUP CRYPTOGRAPHY / EASY-CRYPTO 1
## Consigne:

```
Nous avons intercepté un message chiffré (fichier à télécharger *cipher1.dat*). merci de le déchiffrer.

Vous avez accès au code source du programme qui chiffre (*chall_crypto1.py*), ainsi qu'à l'instance qui a chiffré ce message.

`nc game1.marshack.fr 42001`

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
cipher1.dat
chall_crypto1.py
```

## Serveur :

```
game1.marshack.fr:42001
```

## Points attribués :

```
10
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{MonSuperSecretTresSecure}
```

## Solution : 

On constate dans le code source que la clé est juste "xOré" avec le message à chiffrer.

Si on envoie le message chiffré, il sera automatiquement déchiffré (K ^ M ^ K = M) :-D 

```bash
cat cipher1.dat | nc game1.marshack.fr 42001
Bravo, vous pouvez valider ! fl@g{MonSuperSecretTresSecure}
```


<!-- Review Taz : 20230321 -->