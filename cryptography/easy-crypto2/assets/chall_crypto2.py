#!/usr/bin/python3 -u

import sys
from ssl import RAND_bytes
import server

def getRandom(length):
    return RAND_bytes(length)

def encrypt(key, data):
    salt=getRandom(8)
    output=[]
    for i in range(len(data)):
        output.append(data[i] ^ key[i % len(key)] ^ salt[i % len(salt)])
    return salt+b"".join([bytes([c]) for c in output])

def readKeyFromFile(filename):
    with open(filename, mode="rb") as f:
        return f.read()

def callbackRead(data):
    key=readKeyFromFile("/home/crypt02/secret")
    return encrypt(key, data)

if __name__ == "__main__":
    server.init("0.0.0.0", 40003)
    server.setCallbackReadData(callbackRead)
    server.start()

