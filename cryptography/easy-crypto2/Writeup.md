# WRITEUP CRYPTOGRAPHY / EASY-CRYPTO 2
## Consigne:

```
Nous avons intercepté un message chiffré (fichier à télécharger *cipher2.dat*), merci de le déchiffrer.

Vous avez accès au code source du programme qui chiffre (*chall_crypto2.py*), ainsi qu'à l'instance qui a chiffré ce message.

`nc game1.marshack.fr 42002`

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
cipher2.dat
chall_crypto2.py
```

## Serveur :

```
game1.marshack.fr:42002
```

## Points attribués :

```
20
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{MonAutreSecretBis}
```

## Solution : 


On constate dans le code source que la clé est "xOré" avec le message à chiffrer et également avec un sel (salt). Le sel permet d'avoir un message chiffré différent pour un même clair.

Le message chiffré est concaténé au sel en sortie. 

Pour retrouver la clé, on connait le sel et on peut envoyer un message en clair, on peut donc retrouver la clé.

Je n'envoie que des '0' (64 fois de façon arbitraire) :
```bash
python3 -c 'import sys;sys.stdout.buffer.write(b"\x00"*64)' | nc game1.marshack.fr 42002 | python3 -c 'import sys;print(sys.stdin.buffer.read())'
b'\x14\xbe\xd7\xc8\xa1c\xb5\xa5HF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0e'
```

On récupère le sel (8 octets) et le message chiffré, et on retrouve la clé (0x0 ^ K = K) :
```python
salt_and_cipher=b'\x14\xbe\xd7\xc8\xa1c\xb5\xa5HF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0eHF\x9f\x04\xb3\x99\\fc\xab\xdf>\x07\xca\x85\x0e'
salt=salt_and_cipher[0:8]
cipher=salt_and_cipher[8:]
key_list=[]
for i in range(len(cipher)):
    key_list.append(cipher[i] ^ salt[i % len(salt)])
key=b"".join([bytes([c]) for c in key_list])
print("KEY:", key)
```

Ce qui affiche :
```
KEY: b'\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab'
```

On remarque un motif répétitif : `\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab`. 

Il s'agit de la clé, elle est de longueur 16 octets (128 bits).

On a maintenant la clé, on peut déchiffrer le message :

```python
with open("cipher2.dat", mode="rb") as f:
    salt_and_cipher=f.read()

salt=salt_and_cipher[0:8]
cipher=salt_and_cipher[8:]

key=b'\\\xf8H\xcc\x12\xfa\xe9\xc3w\x15\x08\xf6\xa6\xa90\xab'
output=[]
for i in range(len(cipher)):
    output.append(cipher[i] ^ key[i % len(key)] ^ salt[i % len(salt)])
print(b"".join([bytes([c]) for c in output]).decode())
```

Ce qui affiche :
```
Bravo, vous pouvez valider ! fl@g{MonAutreSecretBis}
```

<!-- Review Taz : 20230321 -->